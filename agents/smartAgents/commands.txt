DEPLOYER:

node deployer.js --cli='/home/stefan/Work/cyberlife-js/agents/smartAgents/deployercli/bin/run ' --privateKey='80113ebd0983a0aebbe9d7e16276bc7f8def180723d78f15c6b392dd191e7466' --pubKey='6259ac218eed8caf47e26246d7e13c1df70165f2' --address='/ip4/0.0.0.0/tcp/0' --id=1


RESERVE SIMPLE AGENT NAME:

agent newAgent -n=Simple_Agent -s=https://gitlab.com/universenetwork/simple-agent.git -a=0x62e655f081ec03e0115fb5b59da9f3c3f22de1c90ee21554b28f71cde06c89fc


RESERVE LENDING AGENT NAME:

agent newAgent -n=Lending -s=https://gitlab.com/cyberlife/lending-smart-agent.git -a=0xbd655f081ec03e0115fb5b59da9f3c3f22de1c90ee21554b28f71cde06c89fc



DEPLOY SIMPLE AGENT:

agent deploy -n=Simple_Agent -t=6000 -r=1

DEPLOY LENDING

agent deploy -n=Lending -t=6000 -r=1


WITHDRAW SIMPLE_AGENT:

agent withdraw -n=Simple_Agent -o=1

WITHDRAW LENDING:

agent withdraw -n=Lending -o=1

-----------------------------------------------------------------------------------------------------------------

HOSTS:

node host.js --storage='/home/stefan/host1Bots/' --port=38045 --ip='127.0.0.1' --cli='/home/stefan/Work/cyberlife-js/agents/smartAgents/hostcli/bin/run ' --priv='a34c5157521ac1e4eae5be83f95d78da64765a0cabba34f7b917505352de8ee7' --pbk='cecc7c956a1707b22b81179ae67018f7f8ff57f7' --id=1

node host.js --storage='/home/stefan/host2Bots/' --port=38046 --ip='127.0.0.2' --cli='/home/stefan/Work/cyberlife-js/agents/smartAgents/hostcli/bin/run ' --priv='ca6c909c0bead1906a103b29f545c9c6b1cd157ce524d5aae2de1803a99e874d' --pbk='249a4672c70efc221f17e635307bc9f0d1c0e30f' --id=2

node host.js --storage='/home/stefan/host3Bots/' --port=38047 --ip='127.0.0.1' --cli='/home/stefan/Work/cyberlife-js/agents/smartAgents/hostcli/bin/run ' --priv='9a83993bd1c34e6a920acd533a493c49fd84f74f4d6ac2a24d6f26eba27659e9' --pbk='3162a7c0977f854deb70305469d7d2dc33842efc' --id=3

BROADCAST ACTIVITY HOSTS:

host broadcastActivity

STOP ACTIVITY HOSTS

host stopActivity
