const {Command, flags} = require('@oclif/command')
var ethUtils = require('ethereumjs-util')

class changeRent extends Command {

  async run() {

    const {flags} = this.parse(changeRent)

    if (flags.price == undefined || flags.price <= 0) this.log("You need to provide a positive number for the rent price")

    else this.log(createJSON("changeRent", flags.price));

  }

}

changeRent.description = `Change the price you ask in ETH per second for hosting an agent`

changeRent.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  price: flags.string({char: 'p', description: 'How much you ask per second for hosting, in ETH denominations'})
}

function createJSON(type, price) {

  return '{"type":' + '"' + type.toString()
         + '", "price":' + '"' + price.toString()
         + '"}';

}

module.exports = changeRent
