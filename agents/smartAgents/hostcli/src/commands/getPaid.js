const {Command, flags} = require('@oclif/command')
var ethUtils = require('ethereumjs-util')

class getPaid extends Command {

  async run() {

    const {flags} = this.parse(getPaid);

    if (flags.name == undefined) this.log("Please provide a name for the agent")

    else this.log(createJSON("getPaid", flags.name));

  }

}

getPaid.description = `Get paid for hosting a particular agent`

getPaid.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: "Name of the agent"})
}

function createJSON(type, name) {

  return '{"type":' + '"' + type.toString()
         + '", "name":' + '"' + name.toString()
         + '"}';

}

module.exports = getPaid
