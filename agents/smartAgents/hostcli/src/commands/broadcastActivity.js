const {Command, flags} = require('@oclif/command')
var ethUtils = require('ethereumjs-util')

class broadcastActivity extends Command {

  async run() {

    const {flags} = this.parse(broadcastActivity)

    this.log(createJSON("broadcastActivity"));

  }

}

broadcastActivity.description = `Announce that you can host agents and execute them`

broadcastActivity.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'})
}

function createJSON(type) {

  return '{"type":' + '"' + type.toString() + '"}';

}

module.exports = broadcastActivity
