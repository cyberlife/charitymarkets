const {Command, flags} = require('@oclif/command')
var ethUtils = require('ethereumjs-util')

class manageStakeWithdrawal extends Command {

  async run() {

    const {flags} = this.parse(manageStakeWithdrawal);

    if (flags.type == undefined || flags.type.lenght == 0) this.log("You need to provide a tx type")

    else this.log(createJSON("manageStakeWithdrawal", flags.type));

  }

}

manageStakeWithdrawal.description = `Signal withdrawal or withdraw your stake and stop being a host`

manageStakeWithdrawal.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  type: flags.string({char: 't', description: 'The type can be "signal" (signalling the fact that you will soon unstake) or "withdraw" (which withdraws your stake given you waited enough)'})
}

function createJSON(type, action) {

  return '{"type":' + '"' + type.toString()
         + '", "action":' + '"' + action.toString()
         + '"}';

}

module.exports = manageStakeWithdrawal
