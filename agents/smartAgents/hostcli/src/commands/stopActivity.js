const {Command, flags} = require('@oclif/command')
var ethUtils = require('ethereumjs-util')

class stopActivity extends Command {

  async run() {

    const {flags} = this.parse(stopActivity)

    this.log(createJSON("stopActivity"));

  }

}

stopActivity.description = `Announce that you stop being available to execute agents`

stopActivity.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'})
}

function createJSON(type) {

  return '{"type":' + '"' + type.toString() + '"}';

}

module.exports = stopActivity
