const {Command, flags} = require('@oclif/command')
var ethUtils = require('ethereumjs-util')

var cyberlifeIDLength = 46

class setID extends Command {

  async run() {

    const {flags} = this.parse(setID);

    if (flags.id.length != cyberlifeIDLength) this.log("Please provide a valid Cyberlife ID")

    else this.log(createJSON("setID", flags.id));

  }

}

setID.description = `Set a Cyberlife ID to your key`

setID.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  id: flags.string({char: 'i', description: 'Your Cyberlife ID'})
}

function createJSON(type, id) {

  return '{"type":' + '"' + type.toString()
         + '", "id":' + '"' + id.toString()
         + '"}';

}

module.exports = setID
