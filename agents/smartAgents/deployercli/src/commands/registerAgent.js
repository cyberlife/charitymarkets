const {Command, flags} = require('@oclif/command')

class registerAgent extends Command {

  async run() {

    const {flags} = this.parse(registerAgent);

    if (flags.name == undefined) this.log("You need to provide a name for an agent");

    else this.log(createJSON("registerAgent", flags.name));

  }

}

registerAgent.description = `Register the agent in the treasury contract`

registerAgent.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: 'Name of agent'})
}

function createJSON(action_type, name, root, ipfs, hostsNumber) {

  return '{"type":' + '"' + action_type.toString()
         + '", "name":' + '"' + name.toString()
         + '"}';

}

module.exports = registerAgent
