const {Command, flags} = require('@oclif/command')

class update_code extends Command {

  async run() {

    const {flags} = this.parse(update_code);

    if (flags.name == undefined
        || flags.source == undefined
        || flags.hash == undefined) {this.log("You need to provide all important variables");}

    else if (flags.name.length == 0) this.log("You need to provide a name for the agent")

    else if (flags.source.length == 0) this.log("You need to provide a Github path to the agent's new code")

    else if (flags.hash.length == 0) this.log("You need to provide a hash for the code of the agent")

    else {this.log(createJSON("update_code", flags.name, flags.path, flags.installation));}

  }

}

update_code.description = `Update the code of an agent`

update_code.flags = {
  version: flags.version({char: 'v'}),
  help: flags.help({char: 'h'}),
  name: flags.string({char: 'n', description: 'Name of agent to update'}),
  source: flags.string({char: 's', description: 'Github URL of the updated agent'}),
  hash: flags.string({char: 'd', description: 'Hash of the source code'})
}

function createJSON(action_type, name, source, hash) {

  return '{"type":"' + action_type
          + '", "name":"' + name
          + '", "source":"' + source
          + '", "hash":"' + hash
          + '"}';

}

module.exports = update_code
