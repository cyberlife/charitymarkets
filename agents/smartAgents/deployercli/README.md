agentcli
========

CLI to deploy, upgrade and manage autonomous agents

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/agentcli.svg)](https://npmjs.org/package/agentcli)
[![Downloads/week](https://img.shields.io/npm/dw/agentcli.svg)](https://npmjs.org/package/agentcli)
[![License](https://img.shields.io/npm/l/agentcli.svg)](https://github.com/stefanionescu/agentcli/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g agentcli
$ agent COMMAND
running command...
$ agent (-v|--version|version)
agentcli/1.0.0 linux-x64 node-v8.10.0
$ agent --help [COMMAND]
USAGE
  $ agent COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`agent hello`](#agent-hello)
* [`agent help [COMMAND]`](#agent-help-command)

## `agent hello`

Describe the command here

```
USAGE
  $ agent hello

OPTIONS
  -n, --name=name  name to print

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src/commands/hello.js](https://github.com/stefanionescu/agentcli/blob/v1.0.0/src/commands/hello.js)_

## `agent help [COMMAND]`

display help for agent

```
USAGE
  $ agent help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.1.2/src/commands/help.ts)_
<!-- commandsstop -->
