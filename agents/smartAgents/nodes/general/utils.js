var ethUtils = require('ethereumjs-util')
var chain = require('../chainInteraction')
var sha3 = require('ethereumjs-util').sha3
var shell = require('shelljs')
var fs = require('fs')
const symlinks = require('symlinks')
const symlinkDir = require('symlink-dir')

async function unlinkResources(agents_location, name) {

  console.log("Unlinking resources from " + name)

  symlinks(agents_location.toString() + name.toString() + '/node_modules/*')
    .then(links => links.forEach(link => { fs.unlinkSync(link) }) )
    .then()
    .catch()

}

async function linkResource(agent_path, agent_name, resourceName) {

  console.log("Linking " + resourceName + " to " + agent_name)

  var linkReceiver = agent_path + '/node_modules/' + resourceName

  executeCommand('npm root -g', async function(path, err) {

    try { await fs.unlinkSync(linkReceiver) } catch(err) {}

    var linkSender = path.replace('\n', '') + '/' + resourceName

    await symlinkDir(linkSender, linkReceiver)

  })

}

async function findHostInArray(element, array) {

  if (element == undefined) return -1

  var pos = -1

  for (var j = 0; j < array.length; j++) {

    if (array[j].id.toB58String() == element) {pos = j; break}

  }

  return pos

}

async function getHostsLeaves(hosts) {

  var leaves = []

  for (var i = 0; i < hosts.length; i++) {

    leaves.push(sha3(hosts[i].id.toB58String()))

  }

  return leaves

}

async function getHashedLeaves(lvs) {

  var leaves = []

  for (var i = 0; i < lvs.length; i++) {

    leaves.push(sha3(lvs[i]))

  }

  return leaves

}

async function getCurrentShardLeaves(shard) {

  var curLeaves = []

  var hostAddresses = []

  var billingLeaves = []

  var bufShardToStr = shard.toString('utf8')

  var leaves = bufShardToStr.split('\n')

  var splitLeaf
  var idPosition
  var i

  for (i = 0; i < leaves.length; i++) {

    if (leaves[i] == '' || leaves[i] == ' ') continue

    billingLeaves.push(leaves[i])

    splitLeaf = leaves[i].split(' ')

    hostAddresses.push(splitLeaf[0])

  }

  if (hostAddresses.length > 0 && billingLeaves.length > 0) {

    curLeaves.push(hostAddresses)
    curLeaves.push(billingLeaves)

  }

  return curLeaves

}

async function stringArrToBuffArr(stringArr) {

  var buffArr = []

  var bufferized

  for (var i = 0; i < stringArr.length; i++) {

    if (i == stringArr.length - 1) bufferized = Buffer.from(stringArr[i], 'utf8')

    else bufferized = Buffer.from(stringArr[i] + '\n', 'utf8')

    buffArr.push(bufferized)

  }

  return buffArr

}

async function createJSON(namesArr, typesArr, dataArr) {

  var json = "{"

  if (namesArr == undefined || typesArr == undefined || dataArr == undefined) {

    console.log("Please provide all arrays")

    return

  }

  for (var i = 0; i < namesArr.length; i++) {

    json = json + '"' + namesArr[i] + '":'

    if (typesArr[i] == 'bool' || typesArr[i] == 'string') json = json + '"'

    json = json + dataArr[i]

    if (typesArr[i] == 'bool' || typesArr[i] == 'string') json = json + '"'

    if (i < namesArr.length - 1) json = json + ', '

    else json = json + ' }'

  }

  return json

}

async function deleteElementFromArray(element, arr, type) {

  var auxArr = arr.concat()

  var sanitizedArr = []

  var found = false

  if (arr.length == 1) {

    return []

  }

  for (var i = 0; i < auxArr.length; i++) {

    if (type == "buffer") {

      if (Buffer.compare(element, auxArr[i]) == 0) {delete(auxArr[i]); found = true}

    } else {

      if (element.toString() == auxArr[i].toString()) {delete(auxArr[i]); found = true}

    }

  }

  if (found) {

    for (i = 0; i < auxArr.length; i++) {

      if (auxArr[i] != undefined && auxArr[i] != NaN && auxArr[i] != '') sanitizedArr.push(auxArr[i])

    }

  }

  return sanitizedArr

}

async function decomposeBillings(billings) {

  var decomposed = []

  var hosts = []

  var timesToHost = []

  var prices = []

  var startTimes = []

  var splitBill

  for (var i = 0; i < billings.length; i++) {

    splitBill = billings[i].split(' ')

    hosts.push(splitBill[0])

    startTimes.push(splitBill[3])

    timesToHost.push(splitBill[1])

    prices.push(splitBill[2])

  }

  decomposed.push(hosts)
  decomposed.push(timesToHost)
  decomposed.push(prices)
  decomposed.push(startTimes)

  return decomposed

}

async function idsToAddresses(ids) {

  var addrs = []

  var fetchedAddr = ''

  for (var i = 0; i < ids.length; i++) {

    fetchedAddr = await chain.getHostAddrFromID(ids[i])

    addrs.push(fetchedAddr)

  }

  return addrs

}

async function executeCommand(command, callback) {

  shell.exec(command, {silent:true}, function(code, stdout, stderr) {

      if (callback != undefined) {

        return callback(stdout, stderr)

      }

  })

}

async function prepareInstruction(command, agentPath) {

  var absolutePath = agentPath.concat('/')

  var instr

  if (command != undefined && command.constructor === Array) {

    absolutePath = absolutePath.concat(command[0])

    instr = command[1].concat()

  } else {

    instr = command.concat()

  }

  if (instr.trim() == 'npm install') return instr.concat(' --prefix ').concat(agentPath)

  return undefined

}

async function splitBuffer(ipfsBuffer) {

  var leaves = []

  for (var i = 0; i < ipfsBuffer.length / 32; i++) {

    leaves.push(ipfsBuffer.slice(32 * i, 32 * (i + 1)))

  }

  return leaves

}

async function findElementInArray(element, array) {

  if (array.constructor !== Array || array.length == 0) return false

  for (var i = 0; i < array.length; i++) {

    if (array[i] == element) return true

  }

  return false

}

async function concatProof(proofs) {

  var leaves = []

  for (var i = 0; i < proofs.length; i++) {

    leaves.push(proofs[i].data)

  }

  return Buffer.concat(leaves)

}

async function getRegisteredHosts(hosts) {

  var auxHosts = hosts

  var id
  var eligible
  var i

  var foundUnregistered = 0

  for (i = 0; i < auxHosts.length; i++) {

    id = await chain.stringToBytes32(auxHosts[i].id.toB58String())

    eligible = await chain.hostCanInteract(id)

    if (!eligible) {

      auxHosts[i] = undefined

      foundUnregistered++

      if (auxHosts.length == 1) return []

    }

  }

  for (i = 0; i < auxHosts.length; i++) {

    if (auxHosts[i] == undefined) {

      for (var j = i; j < auxHosts.length - 1; j++) {

        auxHosts[j] = auxHosts[j + 1]

      }

    }

    if (i == auxHosts.length - 1 && foundUnregistered > 0) {

      auxHosts.length -= foundUnregistered

    }

  }

  return auxHosts

}

module.exports = {

  unlinkResources,
  linkResource,
  findHostInArray,
  getHostsLeaves,
  getHashedLeaves,
  getCurrentShardLeaves,
  stringArrToBuffArr,
  createJSON,
  deleteElementFromArray,
  decomposeBillings,
  idsToAddresses,
  executeCommand,
  prepareInstruction,
  splitBuffer,
  findElementInArray,
  concatProof,
  getRegisteredHosts

}
