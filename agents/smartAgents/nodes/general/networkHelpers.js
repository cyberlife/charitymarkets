var chain = require('../chainInteraction')
var utils = require('./utils')
const async = require('async')
const BigNumber = require('bignumber.js')

const PeerId = require('peer-id')
const PeerInfo = require('peer-info')
const Node = require('../libp2p-bundle')
const pull = require('pull-stream')
const Pushable = require('pull-pushable')
const p = Pushable()

var web3 = chain.web3

async function getHostsWhichInteract(peerList) {

  var staked = []

  for (var i = 0; i < peerList.length; i++) {

    var idToBytes = await chain.stringToBytes32(peerList[i].id.toB58String())

    var interaction = await chain.hostCanInteract(idToBytes)

    if (interaction != undefined && interaction == true) staked.push(peerList[i])

  }

  return staked

}

async function ignoreOccupiedHosts(shard, staked) {

  var auxStaked = staked.slice(0)

  var bufShardToStr = shard.toString('utf8')

  var leaves = bufShardToStr.split('\n')

  var splitLeaf
  var idPosition
  var i

  for (i = 0; i < leaves.length; i++) {

    if (leaves[i] != '' || leaves[i] != ' ' || leaves[i] != '\n') {

      splitLeaf = leaves[i].split(' ')

      idPosition = await utils.findHostInArray(splitLeaf[0], staked)

      if (idPosition != -1) delete(auxStaked[idPosition])

    }

  }

  var sanitizedHosts = []

  for (i = 0; i < auxStaked.length; i++) {

    if (auxStaked[i] != undefined && auxStaked[i] != NaN) sanitizedHosts.push(auxStaked[i])

  }

  return sanitizedHosts

}

async function getRequestData(sortedByRent, responsesNr, time, upstart) {

  var currentDate = new BigNumber(Math.floor(Date.now() / 1000))

  currentDate = currentDate.plus(upstart).plus(time)

  var rents = sortedByRent[0]

  var reqData = []

  var chosenHosts = new Array()

  var billingData = []

  var totalRent = new BigNumber(0)

  for (var i = 0; i < responsesNr; i++) {

    if (sortedByRent[1][0][i] != undefined) {

      totalRent = totalRent.plus(rents[i].times(time))

      chosenHosts.push(sortedByRent[1][0][i])

    }

  }

  reqData.push(totalRent)
  reqData.push(chosenHosts)

  var deploymentTime = new BigNumber(Math.floor(Date.now() / 1000))

  for (i = 0; i < chosenHosts.length; i++) {

    billingData.push(chosenHosts[i].id.toB58String()
                     + " "
                     + time.toString()
                     + " "
                     + rents[i].toString()
                     + " "
                     + currentDate.toString()
                   )

  }

  reqData.push(billingData)

  return reqData

}

function dialNode(personalNode, protocolName, node, dataArray) {

  if (dataArray.constructor !== Array || dataArray.length == 0) {

    return -1

  }

  personalNode.dialProtocol(node, protocolName, (err, conn) => {

    if (err) {
      return undefined
    }

    pull(pull.values(dataArray), conn)

  })

}

async function getWithdrawalLeaves(subarr, arr) {

  if (subarr.length > arr.length) return undefined

  var resultArr = []

  var secondsToHost = []

  var prices = []

  var found = false

  for (var i = 0; i < arr.length; i++) {

    found = false

    for (var j = 0; j < subarr.length; j++) {

      if (subarr[j] == arr[i].split(' ')[0]) {found = true; break}

    }

    if (!found) {

      resultArr.push(arr[i])

      secondsToHost.push(arr[i].split(' ')[1])

      prices.push(arr[i].split(' ')[2])

    }

  }

  return [resultArr, secondsToHost, prices]

}

async function getHostsPhoneBooks(peerList, hostsIds) {

  if (hostsIds == undefined || hostsIds.constructor !== Array) return undefined

  var phoneBooks = []

  for (var i = 0; i < hostsIds.length; i++) {

    for (var j = 0; j < peerList.length; j++) {

      if (peerList[j].id.toB58String() == hostsIds[i]) {phoneBooks.push(peerList[j]); break}

    }

  }

  return phoneBooks

}

module.exports = {

  getHostsWhichInteract,
  ignoreOccupiedHosts,
  getRequestData,
  dialNode,
  getWithdrawalLeaves,
  getHostsPhoneBooks

}
