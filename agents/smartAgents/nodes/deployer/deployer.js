'use strict'
/* eslint-disable no-console */

const PeerId = require('peer-id')
const PeerInfo = require('peer-info')
const Node = require('../libp2p-bundle')
const pull = require('pull-stream')
const async = require('async')
var shell = require('shelljs')
var argv = require('optimist').argv
var fs = require('fs')
var ipfsUtils = require('../general/ipfsInteraction')
const ConnManager = require('libp2p-connection-manager')

var chain = require('../chainInteraction')
var manager = require('./deployerAgentManager')

var networkHelpers = require('../general/networkHelpers')

var personal_id
var personalNode
var privateKey
var pubKey
var id
var nodeMultiaddr
var connManager
var cliPath;

var web3 = chain.web3
var peerList = []
var protocolName = '/cyberlife'
var pingDelay = 10000

//Default create agent vars
var agentIsCreator = false
var agentCreatorAddr = ''
var permissionPos = 0

//GET DATA FROM CONSOLE

cliPath = argv.cli
privateKey = argv.privateKey
pubKey = argv.pubKey
id = argv.id
nodeMultiaddr = argv.address

pubKey = "0x" + pubKey.toString()

//CHECK CONSOLE DATA

if (cliPath == undefined) {console.log("Please input a valid CLI path"); return}

if (privateKey == undefined)
  {console.log("Please input a valid private key"); return}

if (nodeMultiaddr == undefined)
  {console.log("Please input a valid multiaddr for the node"); return}

//CONFIGURE NODE

async.parallel([
  (cb) => nodeCreation(cb)
], (err, nodes) => {
  if (err) { throw err }

  personalNode = nodes[0]

  personalNode.on('peer:discovery', (peer) => saveNewPeer(peer))

  personalNode.handle(protocolName, (protocol, conn) => {

    /*pull(
      p,
      conn
    ) */

    pull(
      conn,
      pull.map((data) => {
        return data.toString('utf8').replace('\n', '')
      }),
      pull.drain(console.log)
    )

  })

})

//START DEPLOYER CLI

shell.exec(cliPath, function(code, stdout, stderr) {})

function nodeCreation(callback) {

  if (id == undefined) {

    createNode(nodeMultiaddr, function(err, result) {

      if (err) callback(undefined, undefined)

      if (result != undefined) callback(undefined, result)

    })

  } else {

    nodeFromID(id, nodeMultiaddr, function(err, result) {

      if (err) callback(undefined, undefined)

      if (result != undefined) callback(undefined, result)

    })

  }

}

//CREATE NODE FROM ID COMING FROM THE CONSOLE

function nodeFromID(idPath, address, callback) {

  if(idPath == undefined) {console.log('You did not give any ID'); return}

  let node

  var id = fs.readFileSync('./ids/id' + idPath.toString()).toString()

  PeerId.createFromJSON(JSON.parse(id), (err, id) => {

    if (err) throw err;

    const peer = new PeerInfo(id);

    peer.multiaddrs.add(address);

    node = new Node({

      peerInfo: peer

    })

    node.start((err) => {

      if (err) {
        throw err

      }

      process.stdin.setEncoding('utf8')

      process.openStdin().on('data', (chunk) => {

        var adjusted_command = chunk.replace('agent ','').trim();

        shell.exec(cliPath + adjusted_command.toString(), {silent:true}, function(code, stdout, stderr) {

        var action

          try{

            action = JSON.parse(JSON.stringify(stdout))

            if (stdout.charAt(0) == '{' && stdout.charAt(stdout.length - 2) == '}') {

              executeAction(action, stdout)

            } else {

              console.log(stdout);

            }

          } catch(exception) {console.log("Exception: " + exception)}

      })

      })

    })

    callback(null, node)

  })

}

//CREATE ENTIRELY NEW NODE

function createNode(address, callback) {

  let node

  PeerId.create({bits:512}, (err, id) => {

    if (err) throw err;

    const peer = new PeerInfo(id)

    peer.multiaddrs.add(address)

    node = new Node({

      peerInfo: peer

    })

    node.start((err) => {

      if (err) {
        throw err

      }

      process.stdin.setEncoding('utf8')

      process.openStdin().on('data', (chunk) => {

        var adjusted_command = chunk.replace('agent ','');

        shell.exec(cliPath + adjusted_command.toString(), {silent:true}, function(code, stdout, stderr) {

        var action;

        try{

          action = JSON.parse(JSON.stringify(stdout))

          if (stdout.charAt(0) == '{' && stdout.charAt(stdout.length - 2) == '}') {

            executeAction(action, stdout)

          } else {

            console.log(stdout);

          }

        } catch(exception) {}

       })

      })

    })

    callback(null, node)

  })
}

//EXECUTE ACTIONS COMING FROM CLI

async function executeAction(_action, consoleOutput) {

  var action = JSON.parse(_action)

  if (action.type == 'deleteAgent') {

    await manager.deleteAgent(pubKey, action.name)

  }

  if (action.type == 'deploy') {

    await manager.deployAgent(personalNode, protocolName, pubKey, privateKey, peerList, _action)

  }

  if (action.type == 'fund') {

    await manager.fundAgent(pubKey, action.name, action.funds)

  }

  if (action.type == 'grant_autonomy') {

    await chain.makeAutonomous(pubKey, action.name, 0)

  }

  if (action.type == 'newAgent') {

    //TODO: generate key for agent if not autonomous and save private key somewhere; else, let a host generate a key and not share the private key

    var newAccount = web3.eth.accounts.create();

    await chain.createAgent(pubKey, agentIsCreator,
          agentCreatorAddr, permissionPos,
          action.name.toString(), action.source.toString(), action.source_hash.toString(),
          newAccount.address);

  }

  if (action.type == 'registerAgent') {

    await manager.registerInTreasury(action)

  }

  if (action.type == 'update_code') {

    await chain.updateAgentCode(pubKey, action.name, 0, action.source, action.hash)

  }

  if (action.type == 'withdraw') {

    await manager.withdrawAgent(personalNode, protocolName, pubKey, peerList, _action)

  }

  if (action.type == 'withdrawFunds') {

    await chain.withdrawFunds(pubKey, action.name)

  }

}

function saveNewPeer(peer) {

  if (!isPeerSaved(peer.id.toB58String())) {

    peerList.push(peer);

  }

}

function isPeerSaved(peerID) {

  for (var i = 0; i < peerList.length; i++) {

    if (peerID == peerList[i].id.toB58String()) return true;

  }

  return false;

}
