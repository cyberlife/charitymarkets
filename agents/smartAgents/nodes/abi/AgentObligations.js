var agentObligations = {
  "contractName": "AgentObligations",
  "abi": [
    {
      "inputs": [
        {
          "name": "_management",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "receiver",
          "type": "address"
        },
        {
          "name": "agentName",
          "type": "string"
        },
        {
          "name": "newEarnings",
          "type": "uint256"
        }
      ],
      "name": "changeObligation",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "agent",
          "type": "address"
        }
      ],
      "name": "deleteAllObligations",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "agentAddress",
          "type": "address"
        },
        {
          "name": "obligationPosition",
          "type": "uint256"
        }
      ],
      "name": "deleteObligation",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "agent",
          "type": "address"
        }
      ],
      "name": "getObligationsLength",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "agent",
          "type": "address"
        }
      ],
      "name": "getTotalObligation",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "agent",
          "type": "address"
        },
        {
          "name": "position",
          "type": "uint256"
        }
      ],
      "name": "getObligationReceiver",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "agent",
          "type": "address"
        },
        {
          "name": "position",
          "type": "uint256"
        }
      ],
      "name": "getObligationPayment",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    }
  ],
  "bytecode": "0x608060405234801561001057600080fd5b50604051602080610d0e833981016040525160028054600160a060020a031916600160a060020a03909216919091179055610cbe806100506000396000f3006080604052600436106100695763ffffffff60e060020a600035041663838eb814811461006e5780638d019ca1146100a45780638dbe5cab146100e457806394fc94a514610105578063ecbe7caa1461012b578063ef08f3c21461014c578063ef1b2e051461016d575b600080fd5b34801561007a57600080fd5b50610092600160a060020a03600435166024356101d6565b60408051918252519081900360200190f35b3480156100b057600080fd5b506100c8600160a060020a0360043516602435610214565b60408051600160a060020a039092168252519081900360200190f35b3480156100f057600080fd5b50610092600160a060020a0360043516610259565b34801561011157600080fd5b50610129600160a060020a0360043516602435610274565b005b34801561013757600080fd5b50610092600160a060020a0360043516610499565b34801561015857600080fd5b50610129600160a060020a03600435166104b4565b34801561017957600080fd5b5060408051602060046024803582810135601f8101859004850286018501909652858552610129958335600160a060020a031695369560449491939091019190819084018382808284375094975050933594506104e49350505050565b600160a060020a03821660009081526020819052604081208054839081106101fa57fe5b906000526020600020906002020160010154905092915050565b600160a060020a038216600090815260208190526040812080548390811061023857fe5b6000918252602090912060029091020154600160a060020a03169392505050565b600160a060020a031660009081526020819052604090205490565b600061027f83610259565b821061028a57600080fd5b600160a060020a03831660009081526020819052604090208054339190839081106102b157fe5b6000918252602090912060029091020154600160a060020a0316146102d557600080fd5b600160a060020a03831660009081526020819052604090208054829081106102f957fe5b600091825260208083206001600290930201820154600160a060020a03871684529190526040909120805491909103905550805b600160a060020a0383166000908152602081905260409020546000190181101561046a57600160a060020a038316600090815260208190526040902080546001830190811061037857fe5b60009182526020808320600290920290910154600160a060020a0386811684529183905260409092208054919092169190839081106103b357fe5b60009182526020808320600292909202909101805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a039485161790559185168152908190526040902080546001830190811061040957fe5b90600052602060002090600202016001015460008085600160a060020a0316600160a060020a031681526020019081526020016000208281548110151561044c57fe5b6000918252602090912060016002909202018101919091550161032d565b600160a060020a0383166000908152602081905260409020805490610493906000198301610bea565b50505050565b600160a060020a031660009081526001602052604090205490565b600160a060020a03811660009081526001602090815260408083208390559082905281206104e191610c1b565b50565b6000808080808080808080808b116104fb57600080fd5b60648b111561050957600080fd5b339950600160a060020a038d161561051f578c99505b600260009054906101000a9004600160a060020a0316600160a060020a031663fd4eb5b08d6040518263ffffffff1660e060020a0281526004018080602001828103825283818151815260200191508051906020019080838360005b8381101561059357818101518382015260200161057b565b50505050905090810190601f1680156105c05780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b1580156105df57600080fd5b505af11580156105f3573d6000803e3d6000fd5b505050506040513d602081101561060957600080fd5b81019080805190602001909291905050509850600260009054906101000a9004600160a060020a0316600160a060020a031663139103058d6040518263ffffffff1660e060020a0281526004018080602001828103825283818151815260200191508051906020019080838360005b83811015610690578181015183820152602001610678565b50505050905090810190601f1680156106bd5780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b1580156106dc57600080fd5b505af11580156106f0573d6000803e3d6000fd5b505050506040513d602081101561070657600080fd5b5051600254604080517f3f1a8ae3000000000000000000000000000000000000000000000000000000008152600160a060020a038d81166004830152602482018590529151939b50911691633f1a8ae3916044808201926020929091908290030181600087803b15801561077957600080fd5b505af115801561078d573d6000803e3d6000fd5b505050506040513d60208110156107a357600080fd5b5051600254604080517f1505e14c000000000000000000000000000000000000000000000000000000008152600160a060020a038d81166004830152602482018d90529151939a50911691631505e14c916044808201926020929091908290030181600087803b15801561081657600080fd5b505af115801561082a573d6000803e3d6000fd5b505050506040513d602081101561084057600080fd5b50519550600094508493508392508291505b600160a060020a03861660009081526020819052604090205482101561090a57600160a060020a0386811660009081526020819052604090208054918c16918490811061089b57fe5b6000918252602090912060029091020154600160a060020a031614156108c357819450600193505b600160a060020a03861660009081526020819052604090208054839081106108e757fe5b906000526020600020906002020160010154830192508180600101925050610852565b88600160a060020a03168a600160a060020a03161480156109285750865b1561097b578315806109705750600160a060020a038616600090815260208190526040902080548c91908790811061095c57fe5b906000526020600020906002020160010154115b151561097b57600080fd5b50600060018415151415610ad957600160a060020a038616600090815260208190526040902080548c9190879081106109b057fe5b9060005260206000209060020201600101541115610a2b57600160a060020a038616600090815260208190526040902080548c9190879081106109ef57fe5b600091825260208083206001600290930201820154600160a060020a038b16845291905260409091208054929091039182900390559050610ad4565b600160a060020a038616600090815260208190526040902080548c919087908110610a5257fe5b9060005260206000209060020201600101541015610ad457600160a060020a0386166000908152602081905260409020805486908110610a8e57fe5b9060005260206000209060020201600101548b0390506064818401101515610ab557600080fd5b600160a060020a03861660009081526001602052604090208054820190555b610b07565b6064838c0110610ae857600080fd5b600160a060020a038616600090815260016020526040902080548c0190555b610b1484868d8d8a610b23565b50505050505050505050505050565b610b2b610c3c565b8515610b7257600160a060020a0382166000908152602081905260409020805485919087908110610b5857fe5b906000526020600020906002020160010181905550610be2565b50604080518082018252600160a060020a03848116825260208083018781528583166000908152808352948520805460018082018355918752929095208451600290930201805473ffffffffffffffffffffffffffffffffffffffff191692909316919091178255519201919091555b505050505050565b815481835581811115610c1657600202816002028360005260206000209182019101610c169190610c53565b505050565b50805460008255600202906000526020600020908101906104e19190610c53565b604080518082019091526000808252602082015290565b610c8f91905b80821115610c8b57805473ffffffffffffffffffffffffffffffffffffffff1916815560006001820155600201610c59565b5090565b905600a165627a7a72305820057e42febd80babb912568f34dd3bc0f895a51ec0b1fc3308f9b304664ff62e70029",
  "deployedBytecode": "0x6080604052600436106100695763ffffffff60e060020a600035041663838eb814811461006e5780638d019ca1146100a45780638dbe5cab146100e457806394fc94a514610105578063ecbe7caa1461012b578063ef08f3c21461014c578063ef1b2e051461016d575b600080fd5b34801561007a57600080fd5b50610092600160a060020a03600435166024356101d6565b60408051918252519081900360200190f35b3480156100b057600080fd5b506100c8600160a060020a0360043516602435610214565b60408051600160a060020a039092168252519081900360200190f35b3480156100f057600080fd5b50610092600160a060020a0360043516610259565b34801561011157600080fd5b50610129600160a060020a0360043516602435610274565b005b34801561013757600080fd5b50610092600160a060020a0360043516610499565b34801561015857600080fd5b50610129600160a060020a03600435166104b4565b34801561017957600080fd5b5060408051602060046024803582810135601f8101859004850286018501909652858552610129958335600160a060020a031695369560449491939091019190819084018382808284375094975050933594506104e49350505050565b600160a060020a03821660009081526020819052604081208054839081106101fa57fe5b906000526020600020906002020160010154905092915050565b600160a060020a038216600090815260208190526040812080548390811061023857fe5b6000918252602090912060029091020154600160a060020a03169392505050565b600160a060020a031660009081526020819052604090205490565b600061027f83610259565b821061028a57600080fd5b600160a060020a03831660009081526020819052604090208054339190839081106102b157fe5b6000918252602090912060029091020154600160a060020a0316146102d557600080fd5b600160a060020a03831660009081526020819052604090208054829081106102f957fe5b600091825260208083206001600290930201820154600160a060020a03871684529190526040909120805491909103905550805b600160a060020a0383166000908152602081905260409020546000190181101561046a57600160a060020a038316600090815260208190526040902080546001830190811061037857fe5b60009182526020808320600290920290910154600160a060020a0386811684529183905260409092208054919092169190839081106103b357fe5b60009182526020808320600292909202909101805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a039485161790559185168152908190526040902080546001830190811061040957fe5b90600052602060002090600202016001015460008085600160a060020a0316600160a060020a031681526020019081526020016000208281548110151561044c57fe5b6000918252602090912060016002909202018101919091550161032d565b600160a060020a0383166000908152602081905260409020805490610493906000198301610bea565b50505050565b600160a060020a031660009081526001602052604090205490565b600160a060020a03811660009081526001602090815260408083208390559082905281206104e191610c1b565b50565b6000808080808080808080808b116104fb57600080fd5b60648b111561050957600080fd5b339950600160a060020a038d161561051f578c99505b600260009054906101000a9004600160a060020a0316600160a060020a031663fd4eb5b08d6040518263ffffffff1660e060020a0281526004018080602001828103825283818151815260200191508051906020019080838360005b8381101561059357818101518382015260200161057b565b50505050905090810190601f1680156105c05780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b1580156105df57600080fd5b505af11580156105f3573d6000803e3d6000fd5b505050506040513d602081101561060957600080fd5b81019080805190602001909291905050509850600260009054906101000a9004600160a060020a0316600160a060020a031663139103058d6040518263ffffffff1660e060020a0281526004018080602001828103825283818151815260200191508051906020019080838360005b83811015610690578181015183820152602001610678565b50505050905090810190601f1680156106bd5780820380516001836020036101000a031916815260200191505b5092505050602060405180830381600087803b1580156106dc57600080fd5b505af11580156106f0573d6000803e3d6000fd5b505050506040513d602081101561070657600080fd5b5051600254604080517f3f1a8ae3000000000000000000000000000000000000000000000000000000008152600160a060020a038d81166004830152602482018590529151939b50911691633f1a8ae3916044808201926020929091908290030181600087803b15801561077957600080fd5b505af115801561078d573d6000803e3d6000fd5b505050506040513d60208110156107a357600080fd5b5051600254604080517f1505e14c000000000000000000000000000000000000000000000000000000008152600160a060020a038d81166004830152602482018d90529151939a50911691631505e14c916044808201926020929091908290030181600087803b15801561081657600080fd5b505af115801561082a573d6000803e3d6000fd5b505050506040513d602081101561084057600080fd5b50519550600094508493508392508291505b600160a060020a03861660009081526020819052604090205482101561090a57600160a060020a0386811660009081526020819052604090208054918c16918490811061089b57fe5b6000918252602090912060029091020154600160a060020a031614156108c357819450600193505b600160a060020a03861660009081526020819052604090208054839081106108e757fe5b906000526020600020906002020160010154830192508180600101925050610852565b88600160a060020a03168a600160a060020a03161480156109285750865b1561097b578315806109705750600160a060020a038616600090815260208190526040902080548c91908790811061095c57fe5b906000526020600020906002020160010154115b151561097b57600080fd5b50600060018415151415610ad957600160a060020a038616600090815260208190526040902080548c9190879081106109b057fe5b9060005260206000209060020201600101541115610a2b57600160a060020a038616600090815260208190526040902080548c9190879081106109ef57fe5b600091825260208083206001600290930201820154600160a060020a038b16845291905260409091208054929091039182900390559050610ad4565b600160a060020a038616600090815260208190526040902080548c919087908110610a5257fe5b9060005260206000209060020201600101541015610ad457600160a060020a0386166000908152602081905260409020805486908110610a8e57fe5b9060005260206000209060020201600101548b0390506064818401101515610ab557600080fd5b600160a060020a03861660009081526001602052604090208054820190555b610b07565b6064838c0110610ae857600080fd5b600160a060020a038616600090815260016020526040902080548c0190555b610b1484868d8d8a610b23565b50505050505050505050505050565b610b2b610c3c565b8515610b7257600160a060020a0382166000908152602081905260409020805485919087908110610b5857fe5b906000526020600020906002020160010181905550610be2565b50604080518082018252600160a060020a03848116825260208083018781528583166000908152808352948520805460018082018355918752929095208451600290930201805473ffffffffffffffffffffffffffffffffffffffff191692909316919091178255519201919091555b505050505050565b815481835581811115610c1657600202816002028360005260206000209182019101610c169190610c53565b505050565b50805460008255600202906000526020600020908101906104e19190610c53565b604080518082019091526000808252602082015290565b610c8f91905b80821115610c8b57805473ffffffffffffffffffffffffffffffffffffffff1916815560006001820155600201610c59565b5090565b905600a165627a7a72305820057e42febd80babb912568f34dd3bc0f895a51ec0b1fc3308f9b304664ff62e70029",
  "sourceMap": "131:4427:2:-;;;486:95;8:9:-1;5:2;;;30:1;27;20:12;5:2;486:95:2;;;;;;;;;;;;;533:10;:42;;-1:-1:-1;;;;;;533:42:2;-1:-1:-1;;;;;533:42:2;;;;;;;;;131:4427;;;-1:-1:-1;131:4427:2;;",
  "deployedSourceMap": "131:4427:2:-;;;;;;;;;-1:-1:-1;;;131:4427:2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;4393:162;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;4393:162:2;-1:-1:-1;;;;;4393:162:2;;;;;;;;;;;;;;;;;;;;;;;4236:153;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;4236:153:2;-1:-1:-1;;;;;4236:153:2;;;;;;;;;;;-1:-1:-1;;;;;4236:153:2;;;;;;;;;;;;;;3989:122;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;3989:122:2;-1:-1:-1;;;;;3989:122:2;;;;;2856:685;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;2856:685:2;-1:-1:-1;;;;;2856:685:2;;;;;;;;;4115:117;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;4115:117:2;-1:-1:-1;;;;;4115:117:2;;;;;2728:124;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;2728:124:2;-1:-1:-1;;;;;2728:124:2;;;;;585:2139;;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;585:2139:2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;;;;;585:2139:2;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;585:2139:2;;-1:-1:-1;;585:2139:2;;;-1:-1:-1;585:2139:2;;-1:-1:-1;;;;585:2139:2;4393:162;-1:-1:-1;;;;;4502:18:2;;4477:7;4502:18;;;;;;;;;;:28;;4521:8;;4502:28;;;;;;;;;;;;;;;;:47;;;4495:54;;4393:162;;;;:::o;4236:153::-;-1:-1:-1;;;;;4346:18:2;;4321:7;4346:18;;;;;;;;;;:28;;4365:8;;4346:28;;;;;;;;;;;;;;;;;;;:37;-1:-1:-1;;;;;4346:37:2;;4236:153;-1:-1:-1;;;4236:153:2:o;3989:122::-;-1:-1:-1;;;;;4080:18:2;4055:7;4080:18;;;;;;;;;;:25;;3989:122::o;2856:685::-;3212:6;2975:34;2996:12;2975:20;:34::i;:::-;2954:55;;2946:64;;;;;;-1:-1:-1;;;;;3024:25:2;;:11;:25;;;;;;;;;;:28;;3065:10;;3024:25;3050:1;;3024:28;;;;;;;;;;;;;;;;;;;:37;-1:-1:-1;;;;;3024:37:2;:51;3016:60;;;;;;-1:-1:-1;;;;;3153:25:2;;:11;:25;;;;;;;;;;:28;;3179:1;;3153:28;;;;;;;;;;;;;;:47;:28;;;;;:47;;;-1:-1:-1;;;;;3115:29:2;;;;;;;;;;;;;:85;;;;3083:117;;-1:-1:-1;3221:18:2;3207:288;-1:-1:-1;;;;;3245:25:2;;:11;:25;;;;;;;;;;:32;-1:-1:-1;;3245:36:2;3241:40;;3207:288;;;-1:-1:-1;;;;;3337:25:2;;:11;:25;;;;;;;;;;:32;;3367:1;3363:5;;;3337:32;;;;;;;;;;;;;;;;;;;;;:41;-1:-1:-1;;;;;3297:25:2;;;;;;;;;;;;;:28;;3337:41;;;;;3297:25;3323:1;;3297:28;;;;;;;;;;;;;;;;;;;;;;:81;;-1:-1:-1;;3297:81:2;-1:-1:-1;;;;;3297:81:2;;;;;;3436:25;;;;;;;;;;;;:32;;-1:-1:-1;3462:5:2;;;3436:32;;;;;;;;;;;;;;;;:51;;;3386:11;:25;3398:12;-1:-1:-1;;;;;3386:25:2;-1:-1:-1;;;;;3386:25:2;;;;;;;;;;;;3412:1;3386:28;;;;;;;;;;;;;;;;;;:47;:28;;;;;:47;;:101;;;;3283:3;3207:288;;;-1:-1:-1;;;;;3501:25:2;;:11;:25;;;;;;;;;;:34;;;;;-1:-1:-1;;3501:34:2;;;:::i;:::-;;2856:685;;;:::o;4115:117::-;-1:-1:-1;;;;;4204:22:2;4179:7;4204:22;;;:15;:22;;;;;;;4115:117::o;2728:124::-;-1:-1:-1;;;;;2787:22:2;;2812:1;2787:22;;;:15;:22;;;;;;;;:26;;;2827:18;;;;;;2820:26;;;:::i;:::-;2728:124;:::o;585:2139::-;746:24;;;;;;;;;;690:15;;;682:24;;;;;;735:3;720:18;;;712:27;;;;;;773:10;;-1:-1:-1;;;;;;794:22:2;;;790:55;;837:8;818:27;;790:55;870:10;;;;;;;;;-1:-1:-1;;;;;870:10:2;-1:-1:-1;;;;;870:29:2;;900:9;870:40;;;;;-1:-1:-1;;;870:40:2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;23:1:-1;8:100;33:3;30:1;27:10;8:100;;;90:11;;;84:18;71:11;;;64:39;52:2;45:10;8:100;;;12:14;870:40:2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;8:9:-1;5:2;;;30:1;27;20:12;5:2;870:40:2;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;870:40:2;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;870:40:2;;;;;;;;;;;;;;;;852:58;;941:10;;;;;;;;;-1:-1:-1;;;;;941:10:2;-1:-1:-1;;;;;941:30:2;;972:9;941:41;;;;;-1:-1:-1;;;941:41:2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;23:1:-1;8:100;33:3;30:1;27:10;8:100;;;90:11;;;84:18;71:11;;;64:39;52:2;45:10;8:100;;;12:14;941:41:2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;8:9:-1;5:2;;;30:1;27;20:12;5:2;941:41:2;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;941:41:2;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;941:41:2;1005:10;;:51;;;;;;-1:-1:-1;;;;;1005:51:2;;;;;;;;;;;;;;;941:41;;-1:-1:-1;1005:10:2;;;:27;;:51;;;;;941:41;;1005:51;;;;;;;;:10;;:51;;;5:2:-1;;;;30:1;27;20:12;5:2;1005:51:2;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;1005:51:2;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1005:51:2;1086:10;;:50;;;;;;-1:-1:-1;;;;;1086:50:2;;;;;;;;;;;;;;;1005:51;;-1:-1:-1;1086:10:2;;;:26;;:50;;;;;1005:51;;1086:50;;;;;;;;:10;;:50;;;5:2:-1;;;;30:1;27;20:12;5:2;1086:50:2;;;;8:9:-1;5:2;;;45:16;42:1;39;24:38;77:16;74:1;67:27;5:2;1086:50:2;;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1086:50:2;;-1:-1:-1;1172:1:2;;-1:-1:-1;1172:1:2;;-1:-1:-1;1172:1:2;;-1:-1:-1;1172:1:2;;-1:-1:-1;1249:308:2;-1:-1:-1;;;;;1270:25:2;;:11;:25;;;;;;;;;;:32;1266:36;;1249:308;;;-1:-1:-1;;;;;1322:25:2;;;:11;:25;;;;;;;;;;:28;;:57;;;;1348:1;;1322:28;;;;;;;;;;;;;;;;;;;:37;-1:-1:-1;;;;;1322:37:2;:57;1318:138;;;1413:1;1392:22;;1442:4;1424:22;;1318:138;-1:-1:-1;;;;;1502:25:2;;:11;:25;;;;;;;;;;:28;;1528:1;;1502:28;;;;;;;;;;;;;;;;:47;;;1483:16;:66;1464:85;;1304:3;;;;;;;1249:308;;;1587:7;-1:-1:-1;;;;;1567:27:2;:16;-1:-1:-1;;;;;1567:27:2;;:39;;;;;1598:8;1567:39;1563:187;;;1625:24;;;:116;;-1:-1:-1;;;;;;1663:25:2;;:11;:25;;;;;;;;;;:45;;1730:11;;1663:25;1689:18;;1663:45;;;;;;;;;;;;;;;;:64;;;:78;1625:116;1617:125;;;;;;;;-1:-1:-1;1777:1:2;1808:4;1789:23;;;;1785:820;;;-1:-1:-1;;;;;1827:25:2;;:11;:25;;;;;;;;;;:45;;1894:11;;1827:25;1853:18;;1827:45;;;;;;;;;;;;;;;;:64;;;:78;1823:623;;;-1:-1:-1;;;;;1931:25:2;;:11;:25;;;;;;;;;;:45;;1998:11;;1931:25;1957:18;;1931:45;;;;;;;;;;;;;;:64;:45;;;;;:64;;;-1:-1:-1;;;;;2052:29:2;;;;;;;;;;;;;1931:78;;;;2052:42;;;;2020:74;;1931:78;-1:-1:-1;1823:623:2;;;-1:-1:-1;;;;;2114:25:2;;:11;:25;;;;;;;;;;:45;;2181:11;;2114:25;2140:18;;2114:45;;;;;;;;;;;;;;;;:64;;;:78;2110:336;;;-1:-1:-1;;;;;2232:25:2;;:11;:25;;;;;;;;;;:45;;2258:18;;2232:45;;;;;;;;;;;;;;;;:64;;;2218:11;:78;2205:91;;2347:3;2334:10;2315:16;:29;:35;2307:44;;;;;;;;-1:-1:-1;;;;;2394:29:2;;;;;;:15;:29;;;;;;;:42;;2362:74;;2110:336;1785:820;;;2509:3;2476:30;;;:36;2468:45;;;;;;-1:-1:-1;;;;;2554:29:2;;;;;;:15;:29;;;;;;;:43;;2522:75;;1785:820;2611:107;2629:15;2646:18;2674:11;2687:16;2705:12;2611:17;:107::i;:::-;585:2139;;;;;;;;;;;;;:::o;3558:414::-;3841:31;;:::i;:::-;3719:15;3715:252;;;-1:-1:-1;;;;;3742:25:2;;:11;:25;;;;;;;;;;:45;;3809:11;;3742:25;3768:18;;3742:45;;;;;;;;;;;;;;;;:64;;:78;;;;3715:252;;;-1:-1:-1;3875:31:2;;;;;;;;-1:-1:-1;;;;;3875:31:2;;;;;;;;;;;;3914:25;;;-1:-1:-1;3914:25:2;;;;;;;;;27:10:-1;;39:1;23:18;;;45:23;;3914:45:2;;;;;;;;;;;;;;;;-1:-1:-1;;3914:45:2;;;;;;;;;;;;;;;;;;3715:252;3558:414;;;;;;:::o;131:4427::-;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;:::i;:::-;;;;:::o;:::-;;;;;;;;;;;;;;;;;;;;;;;:::i;:::-;;;;;;;;;;-1:-1:-1;131:4427:2;;;;;;;;:::o;:::-;;;;;;;;;;;;;-1:-1:-1;;131:4427:2;;;;;;;;;;;;;;;;;;:::o",
  "source": "pragma solidity 0.4.25;\n\nimport \"contracts/interfaces/IAgentObligations.sol\";\nimport \"contracts/interfaces/IAgentManagement.sol\";\n\ncontract AgentObligations is IAgentObligations {\n\n  struct Obligation {\n\n    address receiver;\n    uint256 percentageEarnings;\n\n  }\n\n  mapping(address => Obligation[]) obligations;\n  mapping(address => uint256) totalObligation;\n\n  IAgentManagement management;\n\n  modifier onlyManagement() {\n\n    require(msg.sender == address(management));\n    _;\n\n  }\n\n  constructor(address _management) public {\n\n    management = IAgentManagement(_management);\n\n  }\n\n  function changeObligation(address receiver, string agentName, uint256 newEarnings) public {\n\n    require(newEarnings > 0);\n    require(newEarnings <= 100);\n\n    address obligationTarget = msg.sender;\n\n    if (receiver != address(0)) obligationTarget = receiver;\n\n    address creator = management.getCreatorFromName(agentName);\n\n    uint256 agentPosition = management.getPositionFromName(agentName);\n\n    bool autonomy = management.getAgentAutonomy(creator, agentPosition);\n\n    address agentAddress = management.getAgentAddress(creator, agentPosition);\n\n    uint256 obligationPosition = 0;\n    bool foundObligation = false;\n\n    uint256 agentObligations = 0;\n\n    for (uint i = 0; i < obligations[agentAddress].length; i++) {\n\n      if (obligations[agentAddress][i].receiver == obligationTarget) {\n\n        obligationPosition = i;\n        foundObligation = true;\n\n      }\n\n      agentObligations = agentObligations + obligations[agentAddress][i].percentageEarnings;\n\n    }\n\n    if (obligationTarget == creator && autonomy) {\n\n      require(foundObligation == false ||\n          obligations[agentAddress][obligationPosition].percentageEarnings > newEarnings);\n\n    }\n\n    uint256 difference = 0;\n\n    if (foundObligation == true) {\n\n      if (obligations[agentAddress][obligationPosition].percentageEarnings > newEarnings) {\n\n        difference = obligations[agentAddress][obligationPosition].percentageEarnings - newEarnings;\n\n        totalObligation[agentAddress] = totalObligation[agentAddress] - difference;\n\n      } else if (obligations[agentAddress][obligationPosition].percentageEarnings < newEarnings) {\n\n        difference = newEarnings - obligations[agentAddress][obligationPosition].percentageEarnings;\n\n        require(agentObligations + difference < 100);\n\n        totalObligation[agentAddress] = totalObligation[agentAddress] + difference;\n\n      }\n\n    } else {\n\n      require(agentObligations + newEarnings < 100);\n\n      totalObligation[agentAddress] = totalObligation[agentAddress] + newEarnings;\n\n    }\n\n    applyModification(foundObligation, obligationPosition,\n        newEarnings, obligationTarget, agentAddress);\n\n  }\n\n  function deleteAllObligations(address agent) public {\n\n    totalObligation[agent] = 0;\n\n    delete(obligations[agent]);\n\n  }\n\n  function deleteObligation(address agentAddress, uint256 obligationPosition) public {\n\n    require(obligationPosition < getObligationsLength(agentAddress));\n    require(obligations[agentAddress][i].receiver == msg.sender);\n\n    totalObligation[agentAddress] = totalObligation[agentAddress] -\n      obligations[agentAddress][i].percentageEarnings;\n\n    for (uint i = obligationPosition; i < obligations[agentAddress].length - 1; i++) {\n\n      obligations[agentAddress][i].receiver = obligations[agentAddress][i + 1].receiver;\n      obligations[agentAddress][i].percentageEarnings = obligations[agentAddress][i + 1].percentageEarnings;\n\n    }\n\n    obligations[agentAddress].length--;\n\n  }\n\n  //PRIVATE\n\n  function applyModification(bool foundObligation, uint256 obligationPosition,\n      uint256 newEarnings, address caller, address agentAddress) private {\n\n    if (foundObligation)\n      obligations[agentAddress][obligationPosition].percentageEarnings = newEarnings;\n\n    else {\n\n      Obligation memory newObligation = Obligation(caller, newEarnings);\n      obligations[agentAddress].push(newObligation);\n\n    }\n\n  }\n\n  //GETTERS\n\n  function getObligationsLength(address agent) public view returns (uint256) {\n\n      return obligations[agent].length;\n\n  }\n\n  function getTotalObligation(address agent) public view returns (uint256) {\n\n      return totalObligation[agent];\n\n  }\n\n  function getObligationReceiver(address agent, uint256 position) public view returns (address) {\n\n      return obligations[agent][position].receiver;\n\n  }\n\n  function getObligationPayment(address agent, uint256 position) public view returns (uint256) {\n\n      return obligations[agent][position].percentageEarnings;\n\n  }\n\n}\n",
  "sourcePath": "/home/stefan/Work/Cyberlife/chain/protocol/contracts/agents/AgentObligations.sol",
  "ast": {
    "absolutePath": "/home/stefan/Work/Cyberlife/chain/protocol/contracts/agents/AgentObligations.sol",
    "exportedSymbols": {
      "AgentObligations": [
        1534
      ]
    },
    "id": 1535,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 1022,
        "literals": [
          "solidity",
          "0.4",
          ".25"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:23:2"
      },
      {
        "absolutePath": "contracts/interfaces/IAgentObligations.sol",
        "file": "contracts/interfaces/IAgentObligations.sol",
        "id": 1023,
        "nodeType": "ImportDirective",
        "scope": 1535,
        "sourceUnit": 11021,
        "src": "25:52:2",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/IAgentManagement.sol",
        "file": "contracts/interfaces/IAgentManagement.sol",
        "id": 1024,
        "nodeType": "ImportDirective",
        "scope": 1535,
        "sourceUnit": 10965,
        "src": "78:51:2",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 1025,
              "name": "IAgentObligations",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 11020,
              "src": "160:17:2",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IAgentObligations_$11020",
                "typeString": "contract IAgentObligations"
              }
            },
            "id": 1026,
            "nodeType": "InheritanceSpecifier",
            "src": "160:17:2"
          }
        ],
        "contractDependencies": [
          11020
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 1534,
        "linearizedBaseContracts": [
          1534,
          11020
        ],
        "name": "AgentObligations",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "canonicalName": "AgentObligations.Obligation",
            "id": 1031,
            "members": [
              {
                "constant": false,
                "id": 1028,
                "name": "receiver",
                "nodeType": "VariableDeclaration",
                "scope": 1031,
                "src": "208:16:2",
                "stateVariable": false,
                "storageLocation": "default",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                },
                "typeName": {
                  "id": 1027,
                  "name": "address",
                  "nodeType": "ElementaryTypeName",
                  "src": "208:7:2",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  }
                },
                "value": null,
                "visibility": "internal"
              },
              {
                "constant": false,
                "id": 1030,
                "name": "percentageEarnings",
                "nodeType": "VariableDeclaration",
                "scope": 1031,
                "src": "230:26:2",
                "stateVariable": false,
                "storageLocation": "default",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                },
                "typeName": {
                  "id": 1029,
                  "name": "uint256",
                  "nodeType": "ElementaryTypeName",
                  "src": "230:7:2",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  }
                },
                "value": null,
                "visibility": "internal"
              }
            ],
            "name": "Obligation",
            "nodeType": "StructDefinition",
            "scope": 1534,
            "src": "183:79:2",
            "visibility": "public"
          },
          {
            "constant": false,
            "id": 1036,
            "name": "obligations",
            "nodeType": "VariableDeclaration",
            "scope": 1534,
            "src": "266:44:2",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
              "typeString": "mapping(address => struct AgentObligations.Obligation[])"
            },
            "typeName": {
              "id": 1035,
              "keyType": {
                "id": 1032,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "274:7:2",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "266:32:2",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                "typeString": "mapping(address => struct AgentObligations.Obligation[])"
              },
              "valueType": {
                "baseType": {
                  "contractScope": null,
                  "id": 1033,
                  "name": "Obligation",
                  "nodeType": "UserDefinedTypeName",
                  "referencedDeclaration": 1031,
                  "src": "285:10:2",
                  "typeDescriptions": {
                    "typeIdentifier": "t_struct$_Obligation_$1031_storage_ptr",
                    "typeString": "struct AgentObligations.Obligation"
                  }
                },
                "id": 1034,
                "length": null,
                "nodeType": "ArrayTypeName",
                "src": "285:12:2",
                "typeDescriptions": {
                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_ptr",
                  "typeString": "struct AgentObligations.Obligation[]"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 1040,
            "name": "totalObligation",
            "nodeType": "VariableDeclaration",
            "scope": 1534,
            "src": "314:43:2",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
              "typeString": "mapping(address => uint256)"
            },
            "typeName": {
              "id": 1039,
              "keyType": {
                "id": 1037,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "322:7:2",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "314:27:2",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                "typeString": "mapping(address => uint256)"
              },
              "valueType": {
                "id": 1038,
                "name": "uint256",
                "nodeType": "ElementaryTypeName",
                "src": "333:7:2",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 1042,
            "name": "management",
            "nodeType": "VariableDeclaration",
            "scope": 1534,
            "src": "362:27:2",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_IAgentManagement_$10964",
              "typeString": "contract IAgentManagement"
            },
            "typeName": {
              "contractScope": null,
              "id": 1041,
              "name": "IAgentManagement",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10964,
              "src": "362:16:2",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                "typeString": "contract IAgentManagement"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 1054,
              "nodeType": "Block",
              "src": "420:62:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 1050,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 1045,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 13139,
                            "src": "435:3:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 1046,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "435:10:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 1048,
                              "name": "management",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1042,
                              "src": "457:10:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                                "typeString": "contract IAgentManagement"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                                "typeString": "contract IAgentManagement"
                              }
                            ],
                            "id": 1047,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "449:7:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 1049,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "449:19:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "435:33:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 1044,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        13142,
                        13143
                      ],
                      "referencedDeclaration": 13142,
                      "src": "427:7:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 1051,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "427:42:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1052,
                  "nodeType": "ExpressionStatement",
                  "src": "427:42:2"
                },
                {
                  "id": 1053,
                  "nodeType": "PlaceholderStatement",
                  "src": "475:1:2"
                }
              ]
            },
            "documentation": null,
            "id": 1055,
            "name": "onlyManagement",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 1043,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "417:2:2"
            },
            "src": "394:88:2",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 1066,
              "nodeType": "Block",
              "src": "526:55:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 1064,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 1060,
                      "name": "management",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1042,
                      "src": "533:10:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 1062,
                          "name": "_management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1057,
                          "src": "563:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 1061,
                        "name": "IAgentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 10964,
                        "src": "546:16:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IAgentManagement_$10964_$",
                          "typeString": "type(contract IAgentManagement)"
                        }
                      },
                      "id": 1063,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "546:29:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "src": "533:42:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                      "typeString": "contract IAgentManagement"
                    }
                  },
                  "id": 1065,
                  "nodeType": "ExpressionStatement",
                  "src": "533:42:2"
                }
              ]
            },
            "documentation": null,
            "id": 1067,
            "implemented": true,
            "isConstructor": true,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1058,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1057,
                  "name": "_management",
                  "nodeType": "VariableDeclaration",
                  "scope": 1067,
                  "src": "498:19:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1056,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "498:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "497:21:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1059,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "526:0:2"
            },
            "scope": 1534,
            "src": "486:95:2",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1317,
              "nodeType": "Block",
              "src": "675:2049:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 1079,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 1077,
                          "name": "newEarnings",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1073,
                          "src": "690:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": ">",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "30",
                          "id": 1078,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "number",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "704:1:2",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_rational_0_by_1",
                            "typeString": "int_const 0"
                          },
                          "value": "0"
                        },
                        "src": "690:15:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 1076,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        13142,
                        13143
                      ],
                      "referencedDeclaration": 13142,
                      "src": "682:7:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 1080,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "682:24:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1081,
                  "nodeType": "ExpressionStatement",
                  "src": "682:24:2"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 1085,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 1083,
                          "name": "newEarnings",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1073,
                          "src": "720:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "<=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "313030",
                          "id": 1084,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "number",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "735:3:2",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_rational_100_by_1",
                            "typeString": "int_const 100"
                          },
                          "value": "100"
                        },
                        "src": "720:18:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 1082,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        13142,
                        13143
                      ],
                      "referencedDeclaration": 13142,
                      "src": "712:7:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 1086,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "712:27:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1087,
                  "nodeType": "ExpressionStatement",
                  "src": "712:27:2"
                },
                {
                  "assignments": [
                    1089
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1089,
                      "name": "obligationTarget",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "746:24:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 1088,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "746:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1092,
                  "initialValue": {
                    "argumentTypes": null,
                    "expression": {
                      "argumentTypes": null,
                      "id": 1090,
                      "name": "msg",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 13139,
                      "src": "773:3:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_magic_message",
                        "typeString": "msg"
                      }
                    },
                    "id": 1091,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "memberName": "sender",
                    "nodeType": "MemberAccess",
                    "referencedDeclaration": null,
                    "src": "773:10:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "746:37:2"
                },
                {
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    },
                    "id": 1097,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "id": 1093,
                      "name": "receiver",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1069,
                      "src": "794:8:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": "!=",
                    "rightExpression": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "hexValue": "30",
                          "id": 1095,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "number",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "814:1:2",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_rational_0_by_1",
                            "typeString": "int_const 0"
                          },
                          "value": "0"
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_rational_0_by_1",
                            "typeString": "int_const 0"
                          }
                        ],
                        "id": 1094,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "lValueRequested": false,
                        "nodeType": "ElementaryTypeNameExpression",
                        "src": "806:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_address_$",
                          "typeString": "type(address)"
                        },
                        "typeName": "address"
                      },
                      "id": 1096,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "806:10:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "794:22:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "falseBody": null,
                  "id": 1102,
                  "nodeType": "IfStatement",
                  "src": "790:55:2",
                  "trueBody": {
                    "expression": {
                      "argumentTypes": null,
                      "id": 1100,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftHandSide": {
                        "argumentTypes": null,
                        "id": 1098,
                        "name": "obligationTarget",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1089,
                        "src": "818:16:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "nodeType": "Assignment",
                      "operator": "=",
                      "rightHandSide": {
                        "argumentTypes": null,
                        "id": 1099,
                        "name": "receiver",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1069,
                        "src": "837:8:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "src": "818:27:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "id": 1101,
                    "nodeType": "ExpressionStatement",
                    "src": "818:27:2"
                  }
                },
                {
                  "assignments": [
                    1104
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1104,
                      "name": "creator",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "852:15:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 1103,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "852:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1109,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 1107,
                        "name": "agentName",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1071,
                        "src": "900:9:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 1105,
                        "name": "management",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1042,
                        "src": "870:10:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 1106,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getCreatorFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10895,
                      "src": "870:29:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_address_$",
                        "typeString": "function (string memory) view external returns (address)"
                      }
                    },
                    "id": 1108,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "870:40:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "852:58:2"
                },
                {
                  "assignments": [
                    1111
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1111,
                      "name": "agentPosition",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "917:21:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 1110,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "917:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1116,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 1114,
                        "name": "agentName",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1071,
                        "src": "972:9:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 1112,
                        "name": "management",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1042,
                        "src": "941:10:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 1113,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getPositionFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10902,
                      "src": "941:30:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_uint256_$",
                        "typeString": "function (string memory) view external returns (uint256)"
                      }
                    },
                    "id": 1115,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "941:41:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "917:65:2"
                },
                {
                  "assignments": [
                    1118
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1118,
                      "name": "autonomy",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "989:13:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "typeName": {
                        "id": 1117,
                        "name": "bool",
                        "nodeType": "ElementaryTypeName",
                        "src": "989:4:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1124,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 1121,
                        "name": "creator",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1104,
                        "src": "1033:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1122,
                        "name": "agentPosition",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1111,
                        "src": "1042:13:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 1119,
                        "name": "management",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1042,
                        "src": "1005:10:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 1120,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getAgentAutonomy",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10956,
                      "src": "1005:27:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_address_$_t_uint256_$returns$_t_bool_$",
                        "typeString": "function (address,uint256) view external returns (bool)"
                      }
                    },
                    "id": 1123,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1005:51:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "989:67:2"
                },
                {
                  "assignments": [
                    1126
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1126,
                      "name": "agentAddress",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "1063:20:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 1125,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "1063:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1132,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 1129,
                        "name": "creator",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1104,
                        "src": "1113:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1130,
                        "name": "agentPosition",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1111,
                        "src": "1122:13:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 1127,
                        "name": "management",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1042,
                        "src": "1086:10:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 1128,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getAgentAddress",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10938,
                      "src": "1086:26:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_address_$_t_uint256_$returns$_t_address_$",
                        "typeString": "function (address,uint256) view external returns (address)"
                      }
                    },
                    "id": 1131,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1086:50:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1063:73:2"
                },
                {
                  "assignments": [
                    1134
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1134,
                      "name": "obligationPosition",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "1143:26:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 1133,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1143:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1136,
                  "initialValue": {
                    "argumentTypes": null,
                    "hexValue": "30",
                    "id": 1135,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": true,
                    "kind": "number",
                    "lValueRequested": false,
                    "nodeType": "Literal",
                    "src": "1172:1:2",
                    "subdenomination": null,
                    "typeDescriptions": {
                      "typeIdentifier": "t_rational_0_by_1",
                      "typeString": "int_const 0"
                    },
                    "value": "0"
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1143:30:2"
                },
                {
                  "assignments": [
                    1138
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1138,
                      "name": "foundObligation",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "1179:20:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "typeName": {
                        "id": 1137,
                        "name": "bool",
                        "nodeType": "ElementaryTypeName",
                        "src": "1179:4:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1140,
                  "initialValue": {
                    "argumentTypes": null,
                    "hexValue": "66616c7365",
                    "id": 1139,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": true,
                    "kind": "bool",
                    "lValueRequested": false,
                    "nodeType": "Literal",
                    "src": "1202:5:2",
                    "subdenomination": null,
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    },
                    "value": "false"
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1179:28:2"
                },
                {
                  "assignments": [
                    1142
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1142,
                      "name": "agentObligations",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "1214:24:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 1141,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1214:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1144,
                  "initialValue": {
                    "argumentTypes": null,
                    "hexValue": "30",
                    "id": 1143,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": true,
                    "kind": "number",
                    "lValueRequested": false,
                    "nodeType": "Literal",
                    "src": "1241:1:2",
                    "subdenomination": null,
                    "typeDescriptions": {
                      "typeIdentifier": "t_rational_0_by_1",
                      "typeString": "int_const 0"
                    },
                    "value": "0"
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1214:28:2"
                },
                {
                  "body": {
                    "id": 1187,
                    "nodeType": "Block",
                    "src": "1309:248:2",
                    "statements": [
                      {
                        "condition": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          },
                          "id": 1165,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1158,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "1322:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1160,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1159,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1126,
                                  "src": "1334:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "1322:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1162,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1161,
                                "name": "i",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1146,
                                "src": "1348:1:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "1322:28:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1163,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "receiver",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1028,
                            "src": "1322:37:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "nodeType": "BinaryOperation",
                          "operator": "==",
                          "rightExpression": {
                            "argumentTypes": null,
                            "id": 1164,
                            "name": "obligationTarget",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1089,
                            "src": "1363:16:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "src": "1322:57:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "falseBody": null,
                        "id": 1175,
                        "nodeType": "IfStatement",
                        "src": "1318:138:2",
                        "trueBody": {
                          "id": 1174,
                          "nodeType": "Block",
                          "src": "1381:75:2",
                          "statements": [
                            {
                              "expression": {
                                "argumentTypes": null,
                                "id": 1168,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftHandSide": {
                                  "argumentTypes": null,
                                  "id": 1166,
                                  "name": "obligationPosition",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1134,
                                  "src": "1392:18:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "Assignment",
                                "operator": "=",
                                "rightHandSide": {
                                  "argumentTypes": null,
                                  "id": 1167,
                                  "name": "i",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1146,
                                  "src": "1413:1:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "src": "1392:22:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "id": 1169,
                              "nodeType": "ExpressionStatement",
                              "src": "1392:22:2"
                            },
                            {
                              "expression": {
                                "argumentTypes": null,
                                "id": 1172,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftHandSide": {
                                  "argumentTypes": null,
                                  "id": 1170,
                                  "name": "foundObligation",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1138,
                                  "src": "1424:15:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_bool",
                                    "typeString": "bool"
                                  }
                                },
                                "nodeType": "Assignment",
                                "operator": "=",
                                "rightHandSide": {
                                  "argumentTypes": null,
                                  "hexValue": "74727565",
                                  "id": 1171,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": true,
                                  "kind": "bool",
                                  "lValueRequested": false,
                                  "nodeType": "Literal",
                                  "src": "1442:4:2",
                                  "subdenomination": null,
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_bool",
                                    "typeString": "bool"
                                  },
                                  "value": "true"
                                },
                                "src": "1424:22:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_bool",
                                  "typeString": "bool"
                                }
                              },
                              "id": 1173,
                              "nodeType": "ExpressionStatement",
                              "src": "1424:22:2"
                            }
                          ]
                        }
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 1185,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "id": 1176,
                            "name": "agentObligations",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1142,
                            "src": "1464:16:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "commonType": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "id": 1184,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "leftExpression": {
                              "argumentTypes": null,
                              "id": 1177,
                              "name": "agentObligations",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1142,
                              "src": "1483:16:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "nodeType": "BinaryOperation",
                            "operator": "+",
                            "rightExpression": {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "baseExpression": {
                                    "argumentTypes": null,
                                    "id": 1178,
                                    "name": "obligations",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1036,
                                    "src": "1502:11:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                      "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                    }
                                  },
                                  "id": 1180,
                                  "indexExpression": {
                                    "argumentTypes": null,
                                    "id": 1179,
                                    "name": "agentAddress",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1126,
                                    "src": "1514:12:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_address",
                                      "typeString": "address"
                                    }
                                  },
                                  "isConstant": false,
                                  "isLValue": true,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "nodeType": "IndexAccess",
                                  "src": "1502:25:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                    "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                  }
                                },
                                "id": 1182,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1181,
                                  "name": "i",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1146,
                                  "src": "1528:1:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "1502:28:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref"
                                }
                              },
                              "id": 1183,
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "percentageEarnings",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": 1030,
                              "src": "1502:47:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "src": "1483:66:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "1464:85:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 1186,
                        "nodeType": "ExpressionStatement",
                        "src": "1464:85:2"
                      }
                    ]
                  },
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    },
                    "id": 1154,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "id": 1149,
                      "name": "i",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1146,
                      "src": "1266:1:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": "<",
                    "rightExpression": {
                      "argumentTypes": null,
                      "expression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 1150,
                          "name": "obligations",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1036,
                          "src": "1270:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                            "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                          }
                        },
                        "id": 1152,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 1151,
                          "name": "agentAddress",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1126,
                          "src": "1282:12:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1270:25:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                          "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                        }
                      },
                      "id": 1153,
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "length",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "1270:32:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "1266:36:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 1188,
                  "initializationExpression": {
                    "assignments": [
                      1146
                    ],
                    "declarations": [
                      {
                        "constant": false,
                        "id": 1146,
                        "name": "i",
                        "nodeType": "VariableDeclaration",
                        "scope": 1318,
                        "src": "1254:6:2",
                        "stateVariable": false,
                        "storageLocation": "default",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "typeName": {
                          "id": 1145,
                          "name": "uint",
                          "nodeType": "ElementaryTypeName",
                          "src": "1254:4:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "value": null,
                        "visibility": "internal"
                      }
                    ],
                    "id": 1148,
                    "initialValue": {
                      "argumentTypes": null,
                      "hexValue": "30",
                      "id": 1147,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "number",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "1263:1:2",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_rational_0_by_1",
                        "typeString": "int_const 0"
                      },
                      "value": "0"
                    },
                    "nodeType": "VariableDeclarationStatement",
                    "src": "1254:10:2"
                  },
                  "loopExpression": {
                    "expression": {
                      "argumentTypes": null,
                      "id": 1156,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "UnaryOperation",
                      "operator": "++",
                      "prefix": false,
                      "src": "1304:3:2",
                      "subExpression": {
                        "argumentTypes": null,
                        "id": 1155,
                        "name": "i",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1146,
                        "src": "1304:1:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "id": 1157,
                    "nodeType": "ExpressionStatement",
                    "src": "1304:3:2"
                  },
                  "nodeType": "ForStatement",
                  "src": "1249:308:2"
                },
                {
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    },
                    "id": 1193,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "commonType": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "id": 1191,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftExpression": {
                        "argumentTypes": null,
                        "id": 1189,
                        "name": "obligationTarget",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1089,
                        "src": "1567:16:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "nodeType": "BinaryOperation",
                      "operator": "==",
                      "rightExpression": {
                        "argumentTypes": null,
                        "id": 1190,
                        "name": "creator",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1104,
                        "src": "1587:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "src": "1567:27:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": "&&",
                    "rightExpression": {
                      "argumentTypes": null,
                      "id": 1192,
                      "name": "autonomy",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1118,
                      "src": "1598:8:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "src": "1567:39:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "falseBody": null,
                  "id": 1210,
                  "nodeType": "IfStatement",
                  "src": "1563:187:2",
                  "trueBody": {
                    "id": 1209,
                    "nodeType": "Block",
                    "src": "1608:142:2",
                    "statements": [
                      {
                        "expression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "commonType": {
                                "typeIdentifier": "t_bool",
                                "typeString": "bool"
                              },
                              "id": 1206,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "leftExpression": {
                                "argumentTypes": null,
                                "commonType": {
                                  "typeIdentifier": "t_bool",
                                  "typeString": "bool"
                                },
                                "id": 1197,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftExpression": {
                                  "argumentTypes": null,
                                  "id": 1195,
                                  "name": "foundObligation",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1138,
                                  "src": "1625:15:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_bool",
                                    "typeString": "bool"
                                  }
                                },
                                "nodeType": "BinaryOperation",
                                "operator": "==",
                                "rightExpression": {
                                  "argumentTypes": null,
                                  "hexValue": "66616c7365",
                                  "id": 1196,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": true,
                                  "kind": "bool",
                                  "lValueRequested": false,
                                  "nodeType": "Literal",
                                  "src": "1644:5:2",
                                  "subdenomination": null,
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_bool",
                                    "typeString": "bool"
                                  },
                                  "value": "false"
                                },
                                "src": "1625:24:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_bool",
                                  "typeString": "bool"
                                }
                              },
                              "nodeType": "BinaryOperation",
                              "operator": "||",
                              "rightExpression": {
                                "argumentTypes": null,
                                "commonType": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                },
                                "id": 1205,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftExpression": {
                                  "argumentTypes": null,
                                  "expression": {
                                    "argumentTypes": null,
                                    "baseExpression": {
                                      "argumentTypes": null,
                                      "baseExpression": {
                                        "argumentTypes": null,
                                        "id": 1198,
                                        "name": "obligations",
                                        "nodeType": "Identifier",
                                        "overloadedDeclarations": [],
                                        "referencedDeclaration": 1036,
                                        "src": "1663:11:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                          "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                        }
                                      },
                                      "id": 1200,
                                      "indexExpression": {
                                        "argumentTypes": null,
                                        "id": 1199,
                                        "name": "agentAddress",
                                        "nodeType": "Identifier",
                                        "overloadedDeclarations": [],
                                        "referencedDeclaration": 1126,
                                        "src": "1675:12:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_address",
                                          "typeString": "address"
                                        }
                                      },
                                      "isConstant": false,
                                      "isLValue": true,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "nodeType": "IndexAccess",
                                      "src": "1663:25:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                        "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                      }
                                    },
                                    "id": 1202,
                                    "indexExpression": {
                                      "argumentTypes": null,
                                      "id": 1201,
                                      "name": "obligationPosition",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1134,
                                      "src": "1689:18:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      }
                                    },
                                    "isConstant": false,
                                    "isLValue": true,
                                    "isPure": false,
                                    "lValueRequested": false,
                                    "nodeType": "IndexAccess",
                                    "src": "1663:45:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                      "typeString": "struct AgentObligations.Obligation storage ref"
                                    }
                                  },
                                  "id": 1203,
                                  "isConstant": false,
                                  "isLValue": true,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "memberName": "percentageEarnings",
                                  "nodeType": "MemberAccess",
                                  "referencedDeclaration": 1030,
                                  "src": "1663:64:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "BinaryOperation",
                                "operator": ">",
                                "rightExpression": {
                                  "argumentTypes": null,
                                  "id": 1204,
                                  "name": "newEarnings",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1073,
                                  "src": "1730:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "src": "1663:78:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_bool",
                                  "typeString": "bool"
                                }
                              },
                              "src": "1625:116:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_bool",
                                "typeString": "bool"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_bool",
                                "typeString": "bool"
                              }
                            ],
                            "id": 1194,
                            "name": "require",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [
                              13142,
                              13143
                            ],
                            "referencedDeclaration": 13142,
                            "src": "1617:7:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                              "typeString": "function (bool) pure"
                            }
                          },
                          "id": 1207,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "1617:125:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_tuple$__$",
                            "typeString": "tuple()"
                          }
                        },
                        "id": 1208,
                        "nodeType": "ExpressionStatement",
                        "src": "1617:125:2"
                      }
                    ]
                  }
                },
                {
                  "assignments": [
                    1212
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1212,
                      "name": "difference",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "1756:18:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 1211,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1756:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1214,
                  "initialValue": {
                    "argumentTypes": null,
                    "hexValue": "30",
                    "id": 1213,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": true,
                    "kind": "number",
                    "lValueRequested": false,
                    "nodeType": "Literal",
                    "src": "1777:1:2",
                    "subdenomination": null,
                    "typeDescriptions": {
                      "typeIdentifier": "t_rational_0_by_1",
                      "typeString": "int_const 0"
                    },
                    "value": "0"
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1756:22:2"
                },
                {
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    },
                    "id": 1217,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "id": 1215,
                      "name": "foundObligation",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1138,
                      "src": "1789:15:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": "==",
                    "rightExpression": {
                      "argumentTypes": null,
                      "hexValue": "74727565",
                      "id": 1216,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "bool",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "1808:4:2",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "value": "true"
                    },
                    "src": "1789:23:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "falseBody": {
                    "id": 1307,
                    "nodeType": "Block",
                    "src": "2459:146:2",
                    "statements": [
                      {
                        "expression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "commonType": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              },
                              "id": 1294,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "leftExpression": {
                                "argumentTypes": null,
                                "commonType": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                },
                                "id": 1292,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftExpression": {
                                  "argumentTypes": null,
                                  "id": 1290,
                                  "name": "agentObligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1142,
                                  "src": "2476:16:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "BinaryOperation",
                                "operator": "+",
                                "rightExpression": {
                                  "argumentTypes": null,
                                  "id": 1291,
                                  "name": "newEarnings",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1073,
                                  "src": "2495:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "src": "2476:30:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "nodeType": "BinaryOperation",
                              "operator": "<",
                              "rightExpression": {
                                "argumentTypes": null,
                                "hexValue": "313030",
                                "id": 1293,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": true,
                                "kind": "number",
                                "lValueRequested": false,
                                "nodeType": "Literal",
                                "src": "2509:3:2",
                                "subdenomination": null,
                                "typeDescriptions": {
                                  "typeIdentifier": "t_rational_100_by_1",
                                  "typeString": "int_const 100"
                                },
                                "value": "100"
                              },
                              "src": "2476:36:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_bool",
                                "typeString": "bool"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_bool",
                                "typeString": "bool"
                              }
                            ],
                            "id": 1289,
                            "name": "require",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [
                              13142,
                              13143
                            ],
                            "referencedDeclaration": 13142,
                            "src": "2468:7:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                              "typeString": "function (bool) pure"
                            }
                          },
                          "id": 1295,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "2468:45:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_tuple$__$",
                            "typeString": "tuple()"
                          }
                        },
                        "id": 1296,
                        "nodeType": "ExpressionStatement",
                        "src": "2468:45:2"
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 1305,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 1297,
                              "name": "totalObligation",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1040,
                              "src": "2522:15:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 1299,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 1298,
                              "name": "agentAddress",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1126,
                              "src": "2538:12:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "nodeType": "IndexAccess",
                            "src": "2522:29:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "commonType": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "id": 1304,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "leftExpression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "id": 1300,
                                "name": "totalObligation",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1040,
                                "src": "2554:15:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                  "typeString": "mapping(address => uint256)"
                                }
                              },
                              "id": 1302,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1301,
                                "name": "agentAddress",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1126,
                                "src": "2570:12:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_address",
                                  "typeString": "address"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "2554:29:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "nodeType": "BinaryOperation",
                            "operator": "+",
                            "rightExpression": {
                              "argumentTypes": null,
                              "id": 1303,
                              "name": "newEarnings",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1073,
                              "src": "2586:11:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "src": "2554:43:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "2522:75:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 1306,
                        "nodeType": "ExpressionStatement",
                        "src": "2522:75:2"
                      }
                    ]
                  },
                  "id": 1308,
                  "nodeType": "IfStatement",
                  "src": "1785:820:2",
                  "trueBody": {
                    "id": 1288,
                    "nodeType": "Block",
                    "src": "1814:639:2",
                    "statements": [
                      {
                        "condition": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          },
                          "id": 1225,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1218,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "1827:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1220,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1219,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1126,
                                  "src": "1839:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "1827:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1222,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1221,
                                "name": "obligationPosition",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1134,
                                "src": "1853:18:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "1827:45:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1223,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "percentageEarnings",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1030,
                            "src": "1827:64:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "BinaryOperation",
                          "operator": ">",
                          "rightExpression": {
                            "argumentTypes": null,
                            "id": 1224,
                            "name": "newEarnings",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1073,
                            "src": "1894:11:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "1827:78:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "falseBody": {
                          "condition": {
                            "argumentTypes": null,
                            "commonType": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "id": 1255,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "leftExpression": {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "baseExpression": {
                                    "argumentTypes": null,
                                    "id": 1248,
                                    "name": "obligations",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1036,
                                    "src": "2114:11:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                      "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                    }
                                  },
                                  "id": 1250,
                                  "indexExpression": {
                                    "argumentTypes": null,
                                    "id": 1249,
                                    "name": "agentAddress",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1126,
                                    "src": "2126:12:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_address",
                                      "typeString": "address"
                                    }
                                  },
                                  "isConstant": false,
                                  "isLValue": true,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "nodeType": "IndexAccess",
                                  "src": "2114:25:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                    "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                  }
                                },
                                "id": 1252,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1251,
                                  "name": "obligationPosition",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1134,
                                  "src": "2140:18:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "2114:45:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref"
                                }
                              },
                              "id": 1253,
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "percentageEarnings",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": 1030,
                              "src": "2114:64:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "nodeType": "BinaryOperation",
                            "operator": "<",
                            "rightExpression": {
                              "argumentTypes": null,
                              "id": 1254,
                              "name": "newEarnings",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1073,
                              "src": "2181:11:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "src": "2114:78:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_bool",
                              "typeString": "bool"
                            }
                          },
                          "falseBody": null,
                          "id": 1286,
                          "nodeType": "IfStatement",
                          "src": "2110:336:2",
                          "trueBody": {
                            "id": 1285,
                            "nodeType": "Block",
                            "src": "2194:252:2",
                            "statements": [
                              {
                                "expression": {
                                  "argumentTypes": null,
                                  "id": 1265,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "leftHandSide": {
                                    "argumentTypes": null,
                                    "id": 1256,
                                    "name": "difference",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1212,
                                    "src": "2205:10:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "nodeType": "Assignment",
                                  "operator": "=",
                                  "rightHandSide": {
                                    "argumentTypes": null,
                                    "commonType": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    },
                                    "id": 1264,
                                    "isConstant": false,
                                    "isLValue": false,
                                    "isPure": false,
                                    "lValueRequested": false,
                                    "leftExpression": {
                                      "argumentTypes": null,
                                      "id": 1257,
                                      "name": "newEarnings",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1073,
                                      "src": "2218:11:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      }
                                    },
                                    "nodeType": "BinaryOperation",
                                    "operator": "-",
                                    "rightExpression": {
                                      "argumentTypes": null,
                                      "expression": {
                                        "argumentTypes": null,
                                        "baseExpression": {
                                          "argumentTypes": null,
                                          "baseExpression": {
                                            "argumentTypes": null,
                                            "id": 1258,
                                            "name": "obligations",
                                            "nodeType": "Identifier",
                                            "overloadedDeclarations": [],
                                            "referencedDeclaration": 1036,
                                            "src": "2232:11:2",
                                            "typeDescriptions": {
                                              "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                              "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                            }
                                          },
                                          "id": 1260,
                                          "indexExpression": {
                                            "argumentTypes": null,
                                            "id": 1259,
                                            "name": "agentAddress",
                                            "nodeType": "Identifier",
                                            "overloadedDeclarations": [],
                                            "referencedDeclaration": 1126,
                                            "src": "2244:12:2",
                                            "typeDescriptions": {
                                              "typeIdentifier": "t_address",
                                              "typeString": "address"
                                            }
                                          },
                                          "isConstant": false,
                                          "isLValue": true,
                                          "isPure": false,
                                          "lValueRequested": false,
                                          "nodeType": "IndexAccess",
                                          "src": "2232:25:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                            "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                          }
                                        },
                                        "id": 1262,
                                        "indexExpression": {
                                          "argumentTypes": null,
                                          "id": 1261,
                                          "name": "obligationPosition",
                                          "nodeType": "Identifier",
                                          "overloadedDeclarations": [],
                                          "referencedDeclaration": 1134,
                                          "src": "2258:18:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_uint256",
                                            "typeString": "uint256"
                                          }
                                        },
                                        "isConstant": false,
                                        "isLValue": true,
                                        "isPure": false,
                                        "lValueRequested": false,
                                        "nodeType": "IndexAccess",
                                        "src": "2232:45:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                          "typeString": "struct AgentObligations.Obligation storage ref"
                                        }
                                      },
                                      "id": 1263,
                                      "isConstant": false,
                                      "isLValue": true,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "memberName": "percentageEarnings",
                                      "nodeType": "MemberAccess",
                                      "referencedDeclaration": 1030,
                                      "src": "2232:64:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      }
                                    },
                                    "src": "2218:78:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "src": "2205:91:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "id": 1266,
                                "nodeType": "ExpressionStatement",
                                "src": "2205:91:2"
                              },
                              {
                                "expression": {
                                  "argumentTypes": null,
                                  "arguments": [
                                    {
                                      "argumentTypes": null,
                                      "commonType": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      },
                                      "id": 1272,
                                      "isConstant": false,
                                      "isLValue": false,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "leftExpression": {
                                        "argumentTypes": null,
                                        "commonType": {
                                          "typeIdentifier": "t_uint256",
                                          "typeString": "uint256"
                                        },
                                        "id": 1270,
                                        "isConstant": false,
                                        "isLValue": false,
                                        "isPure": false,
                                        "lValueRequested": false,
                                        "leftExpression": {
                                          "argumentTypes": null,
                                          "id": 1268,
                                          "name": "agentObligations",
                                          "nodeType": "Identifier",
                                          "overloadedDeclarations": [],
                                          "referencedDeclaration": 1142,
                                          "src": "2315:16:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_uint256",
                                            "typeString": "uint256"
                                          }
                                        },
                                        "nodeType": "BinaryOperation",
                                        "operator": "+",
                                        "rightExpression": {
                                          "argumentTypes": null,
                                          "id": 1269,
                                          "name": "difference",
                                          "nodeType": "Identifier",
                                          "overloadedDeclarations": [],
                                          "referencedDeclaration": 1212,
                                          "src": "2334:10:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_uint256",
                                            "typeString": "uint256"
                                          }
                                        },
                                        "src": "2315:29:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_uint256",
                                          "typeString": "uint256"
                                        }
                                      },
                                      "nodeType": "BinaryOperation",
                                      "operator": "<",
                                      "rightExpression": {
                                        "argumentTypes": null,
                                        "hexValue": "313030",
                                        "id": 1271,
                                        "isConstant": false,
                                        "isLValue": false,
                                        "isPure": true,
                                        "kind": "number",
                                        "lValueRequested": false,
                                        "nodeType": "Literal",
                                        "src": "2347:3:2",
                                        "subdenomination": null,
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_rational_100_by_1",
                                          "typeString": "int_const 100"
                                        },
                                        "value": "100"
                                      },
                                      "src": "2315:35:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_bool",
                                        "typeString": "bool"
                                      }
                                    }
                                  ],
                                  "expression": {
                                    "argumentTypes": [
                                      {
                                        "typeIdentifier": "t_bool",
                                        "typeString": "bool"
                                      }
                                    ],
                                    "id": 1267,
                                    "name": "require",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [
                                      13142,
                                      13143
                                    ],
                                    "referencedDeclaration": 13142,
                                    "src": "2307:7:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                                      "typeString": "function (bool) pure"
                                    }
                                  },
                                  "id": 1273,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "kind": "functionCall",
                                  "lValueRequested": false,
                                  "names": [],
                                  "nodeType": "FunctionCall",
                                  "src": "2307:44:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_tuple$__$",
                                    "typeString": "tuple()"
                                  }
                                },
                                "id": 1274,
                                "nodeType": "ExpressionStatement",
                                "src": "2307:44:2"
                              },
                              {
                                "expression": {
                                  "argumentTypes": null,
                                  "id": 1283,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "leftHandSide": {
                                    "argumentTypes": null,
                                    "baseExpression": {
                                      "argumentTypes": null,
                                      "id": 1275,
                                      "name": "totalObligation",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1040,
                                      "src": "2362:15:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                        "typeString": "mapping(address => uint256)"
                                      }
                                    },
                                    "id": 1277,
                                    "indexExpression": {
                                      "argumentTypes": null,
                                      "id": 1276,
                                      "name": "agentAddress",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1126,
                                      "src": "2378:12:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_address",
                                        "typeString": "address"
                                      }
                                    },
                                    "isConstant": false,
                                    "isLValue": true,
                                    "isPure": false,
                                    "lValueRequested": true,
                                    "nodeType": "IndexAccess",
                                    "src": "2362:29:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "nodeType": "Assignment",
                                  "operator": "=",
                                  "rightHandSide": {
                                    "argumentTypes": null,
                                    "commonType": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    },
                                    "id": 1282,
                                    "isConstant": false,
                                    "isLValue": false,
                                    "isPure": false,
                                    "lValueRequested": false,
                                    "leftExpression": {
                                      "argumentTypes": null,
                                      "baseExpression": {
                                        "argumentTypes": null,
                                        "id": 1278,
                                        "name": "totalObligation",
                                        "nodeType": "Identifier",
                                        "overloadedDeclarations": [],
                                        "referencedDeclaration": 1040,
                                        "src": "2394:15:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                          "typeString": "mapping(address => uint256)"
                                        }
                                      },
                                      "id": 1280,
                                      "indexExpression": {
                                        "argumentTypes": null,
                                        "id": 1279,
                                        "name": "agentAddress",
                                        "nodeType": "Identifier",
                                        "overloadedDeclarations": [],
                                        "referencedDeclaration": 1126,
                                        "src": "2410:12:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_address",
                                          "typeString": "address"
                                        }
                                      },
                                      "isConstant": false,
                                      "isLValue": true,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "nodeType": "IndexAccess",
                                      "src": "2394:29:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      }
                                    },
                                    "nodeType": "BinaryOperation",
                                    "operator": "+",
                                    "rightExpression": {
                                      "argumentTypes": null,
                                      "id": 1281,
                                      "name": "difference",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1212,
                                      "src": "2426:10:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      }
                                    },
                                    "src": "2394:42:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "src": "2362:74:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "id": 1284,
                                "nodeType": "ExpressionStatement",
                                "src": "2362:74:2"
                              }
                            ]
                          }
                        },
                        "id": 1287,
                        "nodeType": "IfStatement",
                        "src": "1823:623:2",
                        "trueBody": {
                          "id": 1247,
                          "nodeType": "Block",
                          "src": "1907:197:2",
                          "statements": [
                            {
                              "expression": {
                                "argumentTypes": null,
                                "id": 1235,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftHandSide": {
                                  "argumentTypes": null,
                                  "id": 1226,
                                  "name": "difference",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1212,
                                  "src": "1918:10:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "Assignment",
                                "operator": "=",
                                "rightHandSide": {
                                  "argumentTypes": null,
                                  "commonType": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  },
                                  "id": 1234,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "leftExpression": {
                                    "argumentTypes": null,
                                    "expression": {
                                      "argumentTypes": null,
                                      "baseExpression": {
                                        "argumentTypes": null,
                                        "baseExpression": {
                                          "argumentTypes": null,
                                          "id": 1227,
                                          "name": "obligations",
                                          "nodeType": "Identifier",
                                          "overloadedDeclarations": [],
                                          "referencedDeclaration": 1036,
                                          "src": "1931:11:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                            "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                          }
                                        },
                                        "id": 1229,
                                        "indexExpression": {
                                          "argumentTypes": null,
                                          "id": 1228,
                                          "name": "agentAddress",
                                          "nodeType": "Identifier",
                                          "overloadedDeclarations": [],
                                          "referencedDeclaration": 1126,
                                          "src": "1943:12:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_address",
                                            "typeString": "address"
                                          }
                                        },
                                        "isConstant": false,
                                        "isLValue": true,
                                        "isPure": false,
                                        "lValueRequested": false,
                                        "nodeType": "IndexAccess",
                                        "src": "1931:25:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                          "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                        }
                                      },
                                      "id": 1231,
                                      "indexExpression": {
                                        "argumentTypes": null,
                                        "id": 1230,
                                        "name": "obligationPosition",
                                        "nodeType": "Identifier",
                                        "overloadedDeclarations": [],
                                        "referencedDeclaration": 1134,
                                        "src": "1957:18:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_uint256",
                                          "typeString": "uint256"
                                        }
                                      },
                                      "isConstant": false,
                                      "isLValue": true,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "nodeType": "IndexAccess",
                                      "src": "1931:45:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                        "typeString": "struct AgentObligations.Obligation storage ref"
                                      }
                                    },
                                    "id": 1232,
                                    "isConstant": false,
                                    "isLValue": true,
                                    "isPure": false,
                                    "lValueRequested": false,
                                    "memberName": "percentageEarnings",
                                    "nodeType": "MemberAccess",
                                    "referencedDeclaration": 1030,
                                    "src": "1931:64:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "nodeType": "BinaryOperation",
                                  "operator": "-",
                                  "rightExpression": {
                                    "argumentTypes": null,
                                    "id": 1233,
                                    "name": "newEarnings",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1073,
                                    "src": "1998:11:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "src": "1931:78:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "src": "1918:91:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "id": 1236,
                              "nodeType": "ExpressionStatement",
                              "src": "1918:91:2"
                            },
                            {
                              "expression": {
                                "argumentTypes": null,
                                "id": 1245,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftHandSide": {
                                  "argumentTypes": null,
                                  "baseExpression": {
                                    "argumentTypes": null,
                                    "id": 1237,
                                    "name": "totalObligation",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1040,
                                    "src": "2020:15:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                      "typeString": "mapping(address => uint256)"
                                    }
                                  },
                                  "id": 1239,
                                  "indexExpression": {
                                    "argumentTypes": null,
                                    "id": 1238,
                                    "name": "agentAddress",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1126,
                                    "src": "2036:12:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_address",
                                      "typeString": "address"
                                    }
                                  },
                                  "isConstant": false,
                                  "isLValue": true,
                                  "isPure": false,
                                  "lValueRequested": true,
                                  "nodeType": "IndexAccess",
                                  "src": "2020:29:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "Assignment",
                                "operator": "=",
                                "rightHandSide": {
                                  "argumentTypes": null,
                                  "commonType": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  },
                                  "id": 1244,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "leftExpression": {
                                    "argumentTypes": null,
                                    "baseExpression": {
                                      "argumentTypes": null,
                                      "id": 1240,
                                      "name": "totalObligation",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1040,
                                      "src": "2052:15:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                        "typeString": "mapping(address => uint256)"
                                      }
                                    },
                                    "id": 1242,
                                    "indexExpression": {
                                      "argumentTypes": null,
                                      "id": 1241,
                                      "name": "agentAddress",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1126,
                                      "src": "2068:12:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_address",
                                        "typeString": "address"
                                      }
                                    },
                                    "isConstant": false,
                                    "isLValue": true,
                                    "isPure": false,
                                    "lValueRequested": false,
                                    "nodeType": "IndexAccess",
                                    "src": "2052:29:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "nodeType": "BinaryOperation",
                                  "operator": "-",
                                  "rightExpression": {
                                    "argumentTypes": null,
                                    "id": 1243,
                                    "name": "difference",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1212,
                                    "src": "2084:10:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "src": "2052:42:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "src": "2020:74:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "id": 1246,
                              "nodeType": "ExpressionStatement",
                              "src": "2020:74:2"
                            }
                          ]
                        }
                      }
                    ]
                  }
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 1310,
                        "name": "foundObligation",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1138,
                        "src": "2629:15:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1311,
                        "name": "obligationPosition",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1134,
                        "src": "2646:18:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1312,
                        "name": "newEarnings",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1073,
                        "src": "2674:11:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1313,
                        "name": "obligationTarget",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1089,
                        "src": "2687:16:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1314,
                        "name": "agentAddress",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1126,
                        "src": "2705:12:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      ],
                      "id": 1309,
                      "name": "applyModification",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1474,
                      "src": "2611:17:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_internal_nonpayable$_t_bool_$_t_uint256_$_t_uint256_$_t_address_$_t_address_$returns$__$",
                        "typeString": "function (bool,uint256,uint256,address,address)"
                      }
                    },
                    "id": 1315,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2611:107:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1316,
                  "nodeType": "ExpressionStatement",
                  "src": "2611:107:2"
                }
              ]
            },
            "documentation": null,
            "id": 1318,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "changeObligation",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1074,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1069,
                  "name": "receiver",
                  "nodeType": "VariableDeclaration",
                  "scope": 1318,
                  "src": "611:16:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1068,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "611:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1071,
                  "name": "agentName",
                  "nodeType": "VariableDeclaration",
                  "scope": 1318,
                  "src": "629:16:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 1070,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "629:6:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1073,
                  "name": "newEarnings",
                  "nodeType": "VariableDeclaration",
                  "scope": 1318,
                  "src": "647:19:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1072,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "647:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "610:57:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1075,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "675:0:2"
            },
            "scope": 1534,
            "src": "585:2139:2",
            "stateMutability": "nonpayable",
            "superFunction": 10975,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1335,
              "nodeType": "Block",
              "src": "2780:72:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 1327,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 1323,
                        "name": "totalObligation",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1040,
                        "src": "2787:15:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 1325,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 1324,
                        "name": "agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1320,
                        "src": "2803:5:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "2787:22:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "30",
                      "id": 1326,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "number",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "2812:1:2",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_rational_0_by_1",
                        "typeString": "int_const 0"
                      },
                      "value": "0"
                    },
                    "src": "2787:26:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 1328,
                  "nodeType": "ExpressionStatement",
                  "src": "2787:26:2"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 1333,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "UnaryOperation",
                    "operator": "delete",
                    "prefix": true,
                    "src": "2820:26:2",
                    "subExpression": {
                      "argumentTypes": null,
                      "components": [
                        {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 1329,
                            "name": "obligations",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1036,
                            "src": "2827:11:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                              "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                            }
                          },
                          "id": 1331,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 1330,
                            "name": "agent",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1320,
                            "src": "2839:5:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": true,
                          "nodeType": "IndexAccess",
                          "src": "2827:18:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                            "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                          }
                        }
                      ],
                      "id": 1332,
                      "isConstant": false,
                      "isInlineArray": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "TupleExpression",
                      "src": "2826:20:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                        "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                      }
                    },
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1334,
                  "nodeType": "ExpressionStatement",
                  "src": "2820:26:2"
                }
              ]
            },
            "documentation": null,
            "id": 1336,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "deleteAllObligations",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1321,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1320,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 1336,
                  "src": "2758:13:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1319,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "2758:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "2757:15:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1322,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "2780:0:2"
            },
            "scope": 1534,
            "src": "2728:124:2",
            "stateMutability": "nonpayable",
            "superFunction": 10980,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1433,
              "nodeType": "Block",
              "src": "2939:602:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 1348,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 1344,
                          "name": "obligationPosition",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1340,
                          "src": "2954:18:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "<",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 1346,
                              "name": "agentAddress",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1338,
                              "src": "2996:12:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            ],
                            "id": 1345,
                            "name": "getObligationsLength",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [
                              1487
                            ],
                            "referencedDeclaration": 1487,
                            "src": "2975:20:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_internal_view$_t_address_$returns$_t_uint256_$",
                              "typeString": "function (address) view returns (uint256)"
                            }
                          },
                          "id": 1347,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "2975:34:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "src": "2954:55:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 1343,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        13142,
                        13143
                      ],
                      "referencedDeclaration": 13142,
                      "src": "2946:7:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 1349,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2946:64:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1350,
                  "nodeType": "ExpressionStatement",
                  "src": "2946:64:2"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 1360,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "id": 1352,
                                "name": "obligations",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1036,
                                "src": "3024:11:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                  "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                }
                              },
                              "id": 1354,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1353,
                                "name": "agentAddress",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1338,
                                "src": "3036:12:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_address",
                                  "typeString": "address"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3024:25:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                              }
                            },
                            "id": 1356,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 1355,
                              "name": "i",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1379,
                              "src": "3050:1:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "3024:28:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                              "typeString": "struct AgentObligations.Obligation storage ref"
                            }
                          },
                          "id": 1357,
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "receiver",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": 1028,
                          "src": "3024:37:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 1358,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 13139,
                            "src": "3065:3:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 1359,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "3065:10:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "3024:51:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 1351,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        13142,
                        13143
                      ],
                      "referencedDeclaration": 13142,
                      "src": "3016:7:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 1361,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "3016:60:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1362,
                  "nodeType": "ExpressionStatement",
                  "src": "3016:60:2"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 1376,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 1363,
                        "name": "totalObligation",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1040,
                        "src": "3083:15:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 1365,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 1364,
                        "name": "agentAddress",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1338,
                        "src": "3099:12:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "3083:29:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "commonType": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "id": 1375,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 1366,
                          "name": "totalObligation",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1040,
                          "src": "3115:15:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                            "typeString": "mapping(address => uint256)"
                          }
                        },
                        "id": 1368,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 1367,
                          "name": "agentAddress",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1338,
                          "src": "3131:12:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "3115:29:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "nodeType": "BinaryOperation",
                      "operator": "-",
                      "rightExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 1369,
                              "name": "obligations",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1036,
                              "src": "3153:11:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                              }
                            },
                            "id": 1371,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 1370,
                              "name": "agentAddress",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1338,
                              "src": "3165:12:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "3153:25:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                              "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                            }
                          },
                          "id": 1373,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 1372,
                            "name": "i",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1379,
                            "src": "3179:1:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "3153:28:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                            "typeString": "struct AgentObligations.Obligation storage ref"
                          }
                        },
                        "id": 1374,
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "percentageEarnings",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 1030,
                        "src": "3153:47:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "src": "3115:85:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "3083:117:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 1377,
                  "nodeType": "ExpressionStatement",
                  "src": "3083:117:2"
                },
                {
                  "body": {
                    "id": 1425,
                    "nodeType": "Block",
                    "src": "3288:207:2",
                    "statements": [
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 1407,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1393,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "3297:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1396,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1394,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1338,
                                  "src": "3309:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "3297:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1397,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1395,
                                "name": "i",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1379,
                                "src": "3323:1:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3297:28:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1398,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "memberName": "receiver",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1028,
                            "src": "3297:37:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1399,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "3337:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1401,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1400,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1338,
                                  "src": "3349:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "3337:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1405,
                              "indexExpression": {
                                "argumentTypes": null,
                                "commonType": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                },
                                "id": 1404,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftExpression": {
                                  "argumentTypes": null,
                                  "id": 1402,
                                  "name": "i",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1379,
                                  "src": "3363:1:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "BinaryOperation",
                                "operator": "+",
                                "rightExpression": {
                                  "argumentTypes": null,
                                  "hexValue": "31",
                                  "id": 1403,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": true,
                                  "kind": "number",
                                  "lValueRequested": false,
                                  "nodeType": "Literal",
                                  "src": "3367:1:2",
                                  "subdenomination": null,
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_rational_1_by_1",
                                    "typeString": "int_const 1"
                                  },
                                  "value": "1"
                                },
                                "src": "3363:5:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3337:32:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1406,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "receiver",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1028,
                            "src": "3337:41:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "src": "3297:81:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "id": 1408,
                        "nodeType": "ExpressionStatement",
                        "src": "3297:81:2"
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 1423,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1409,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "3386:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1412,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1410,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1338,
                                  "src": "3398:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "3386:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1413,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1411,
                                "name": "i",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1379,
                                "src": "3412:1:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3386:28:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1414,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "memberName": "percentageEarnings",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1030,
                            "src": "3386:47:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1415,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "3436:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1417,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1416,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1338,
                                  "src": "3448:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "3436:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1421,
                              "indexExpression": {
                                "argumentTypes": null,
                                "commonType": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                },
                                "id": 1420,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftExpression": {
                                  "argumentTypes": null,
                                  "id": 1418,
                                  "name": "i",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1379,
                                  "src": "3462:1:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "BinaryOperation",
                                "operator": "+",
                                "rightExpression": {
                                  "argumentTypes": null,
                                  "hexValue": "31",
                                  "id": 1419,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": true,
                                  "kind": "number",
                                  "lValueRequested": false,
                                  "nodeType": "Literal",
                                  "src": "3466:1:2",
                                  "subdenomination": null,
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_rational_1_by_1",
                                    "typeString": "int_const 1"
                                  },
                                  "value": "1"
                                },
                                "src": "3462:5:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3436:32:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1422,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "percentageEarnings",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1030,
                            "src": "3436:51:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "3386:101:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 1424,
                        "nodeType": "ExpressionStatement",
                        "src": "3386:101:2"
                      }
                    ]
                  },
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    },
                    "id": 1389,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "id": 1382,
                      "name": "i",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1379,
                      "src": "3241:1:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": "<",
                    "rightExpression": {
                      "argumentTypes": null,
                      "commonType": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "id": 1388,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 1383,
                            "name": "obligations",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1036,
                            "src": "3245:11:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                              "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                            }
                          },
                          "id": 1385,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 1384,
                            "name": "agentAddress",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1338,
                            "src": "3257:12:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "3245:25:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                            "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                          }
                        },
                        "id": 1386,
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "length",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "3245:32:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "nodeType": "BinaryOperation",
                      "operator": "-",
                      "rightExpression": {
                        "argumentTypes": null,
                        "hexValue": "31",
                        "id": 1387,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "number",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "3280:1:2",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_rational_1_by_1",
                          "typeString": "int_const 1"
                        },
                        "value": "1"
                      },
                      "src": "3245:36:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "3241:40:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 1426,
                  "initializationExpression": {
                    "assignments": [
                      1379
                    ],
                    "declarations": [
                      {
                        "constant": false,
                        "id": 1379,
                        "name": "i",
                        "nodeType": "VariableDeclaration",
                        "scope": 1434,
                        "src": "3212:6:2",
                        "stateVariable": false,
                        "storageLocation": "default",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "typeName": {
                          "id": 1378,
                          "name": "uint",
                          "nodeType": "ElementaryTypeName",
                          "src": "3212:4:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "value": null,
                        "visibility": "internal"
                      }
                    ],
                    "id": 1381,
                    "initialValue": {
                      "argumentTypes": null,
                      "id": 1380,
                      "name": "obligationPosition",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1340,
                      "src": "3221:18:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "VariableDeclarationStatement",
                    "src": "3212:27:2"
                  },
                  "loopExpression": {
                    "expression": {
                      "argumentTypes": null,
                      "id": 1391,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "UnaryOperation",
                      "operator": "++",
                      "prefix": false,
                      "src": "3283:3:2",
                      "subExpression": {
                        "argumentTypes": null,
                        "id": 1390,
                        "name": "i",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1379,
                        "src": "3283:1:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "id": 1392,
                    "nodeType": "ExpressionStatement",
                    "src": "3283:3:2"
                  },
                  "nodeType": "ForStatement",
                  "src": "3207:288:2"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 1431,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "UnaryOperation",
                    "operator": "--",
                    "prefix": false,
                    "src": "3501:34:2",
                    "subExpression": {
                      "argumentTypes": null,
                      "expression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 1427,
                          "name": "obligations",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1036,
                          "src": "3501:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                            "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                          }
                        },
                        "id": 1429,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 1428,
                          "name": "agentAddress",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1338,
                          "src": "3513:12:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "3501:25:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                          "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                        }
                      },
                      "id": 1430,
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "memberName": "length",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "3501:32:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 1432,
                  "nodeType": "ExpressionStatement",
                  "src": "3501:34:2"
                }
              ]
            },
            "documentation": null,
            "id": 1434,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "deleteObligation",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1341,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1338,
                  "name": "agentAddress",
                  "nodeType": "VariableDeclaration",
                  "scope": 1434,
                  "src": "2882:20:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1337,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "2882:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1340,
                  "name": "obligationPosition",
                  "nodeType": "VariableDeclaration",
                  "scope": 1434,
                  "src": "2904:26:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1339,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "2904:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "2881:50:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1342,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "2939:0:2"
            },
            "scope": 1534,
            "src": "2856:685:2",
            "stateMutability": "nonpayable",
            "superFunction": 10987,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1473,
              "nodeType": "Block",
              "src": "3708:264:2",
              "statements": [
                {
                  "condition": {
                    "argumentTypes": null,
                    "id": 1447,
                    "name": "foundObligation",
                    "nodeType": "Identifier",
                    "overloadedDeclarations": [],
                    "referencedDeclaration": 1436,
                    "src": "3719:15:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "falseBody": {
                    "id": 1471,
                    "nodeType": "Block",
                    "src": "3832:135:2",
                    "statements": [
                      {
                        "assignments": [
                          1458
                        ],
                        "declarations": [
                          {
                            "constant": false,
                            "id": 1458,
                            "name": "newObligation",
                            "nodeType": "VariableDeclaration",
                            "scope": 1474,
                            "src": "3841:31:2",
                            "stateVariable": false,
                            "storageLocation": "memory",
                            "typeDescriptions": {
                              "typeIdentifier": "t_struct$_Obligation_$1031_memory_ptr",
                              "typeString": "struct AgentObligations.Obligation"
                            },
                            "typeName": {
                              "contractScope": null,
                              "id": 1457,
                              "name": "Obligation",
                              "nodeType": "UserDefinedTypeName",
                              "referencedDeclaration": 1031,
                              "src": "3841:10:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage_ptr",
                                "typeString": "struct AgentObligations.Obligation"
                              }
                            },
                            "value": null,
                            "visibility": "internal"
                          }
                        ],
                        "id": 1463,
                        "initialValue": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 1460,
                              "name": "caller",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1442,
                              "src": "3886:6:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            {
                              "argumentTypes": null,
                              "id": 1461,
                              "name": "newEarnings",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1440,
                              "src": "3894:11:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              },
                              {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            ],
                            "id": 1459,
                            "name": "Obligation",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1031,
                            "src": "3875:10:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_struct$_Obligation_$1031_storage_ptr_$",
                              "typeString": "type(struct AgentObligations.Obligation storage pointer)"
                            }
                          },
                          "id": 1462,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "structConstructorCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "3875:31:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_struct$_Obligation_$1031_memory",
                            "typeString": "struct AgentObligations.Obligation memory"
                          }
                        },
                        "nodeType": "VariableDeclarationStatement",
                        "src": "3841:65:2"
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 1468,
                              "name": "newObligation",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1458,
                              "src": "3945:13:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_memory_ptr",
                                "typeString": "struct AgentObligations.Obligation memory"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_struct$_Obligation_$1031_memory_ptr",
                                "typeString": "struct AgentObligations.Obligation memory"
                              }
                            ],
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "id": 1464,
                                "name": "obligations",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1036,
                                "src": "3914:11:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                  "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                }
                              },
                              "id": 1466,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1465,
                                "name": "agentAddress",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1444,
                                "src": "3926:12:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_address",
                                  "typeString": "address"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3914:25:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                              }
                            },
                            "id": 1467,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "push",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "3914:30:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_arraypush_nonpayable$_t_struct$_Obligation_$1031_storage_$returns$_t_uint256_$",
                              "typeString": "function (struct AgentObligations.Obligation storage ref) returns (uint256)"
                            }
                          },
                          "id": 1469,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "3914:45:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 1470,
                        "nodeType": "ExpressionStatement",
                        "src": "3914:45:2"
                      }
                    ]
                  },
                  "id": 1472,
                  "nodeType": "IfStatement",
                  "src": "3715:252:2",
                  "trueBody": {
                    "expression": {
                      "argumentTypes": null,
                      "id": 1455,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftHandSide": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 1448,
                              "name": "obligations",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1036,
                              "src": "3742:11:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                              }
                            },
                            "id": 1451,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 1449,
                              "name": "agentAddress",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1444,
                              "src": "3754:12:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "3742:25:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                              "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                            }
                          },
                          "id": 1452,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 1450,
                            "name": "obligationPosition",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1438,
                            "src": "3768:18:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "3742:45:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                            "typeString": "struct AgentObligations.Obligation storage ref"
                          }
                        },
                        "id": 1453,
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": true,
                        "memberName": "percentageEarnings",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 1030,
                        "src": "3742:64:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "nodeType": "Assignment",
                      "operator": "=",
                      "rightHandSide": {
                        "argumentTypes": null,
                        "id": 1454,
                        "name": "newEarnings",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1440,
                        "src": "3809:11:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "src": "3742:78:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "id": 1456,
                    "nodeType": "ExpressionStatement",
                    "src": "3742:78:2"
                  }
                }
              ]
            },
            "documentation": null,
            "id": 1474,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "applyModification",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1445,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1436,
                  "name": "foundObligation",
                  "nodeType": "VariableDeclaration",
                  "scope": 1474,
                  "src": "3585:20:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_bool",
                    "typeString": "bool"
                  },
                  "typeName": {
                    "id": 1435,
                    "name": "bool",
                    "nodeType": "ElementaryTypeName",
                    "src": "3585:4:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1438,
                  "name": "obligationPosition",
                  "nodeType": "VariableDeclaration",
                  "scope": 1474,
                  "src": "3607:26:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1437,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "3607:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1440,
                  "name": "newEarnings",
                  "nodeType": "VariableDeclaration",
                  "scope": 1474,
                  "src": "3641:19:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1439,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "3641:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1442,
                  "name": "caller",
                  "nodeType": "VariableDeclaration",
                  "scope": 1474,
                  "src": "3662:14:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1441,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "3662:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1444,
                  "name": "agentAddress",
                  "nodeType": "VariableDeclaration",
                  "scope": 1474,
                  "src": "3678:20:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1443,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "3678:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "3584:115:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1446,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "3708:0:2"
            },
            "scope": 1534,
            "src": "3558:414:2",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "private"
          },
          {
            "body": {
              "id": 1486,
              "nodeType": "Block",
              "src": "4064:47:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "expression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 1481,
                        "name": "obligations",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1036,
                        "src": "4080:11:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                          "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                        }
                      },
                      "id": 1483,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 1482,
                        "name": "agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1476,
                        "src": "4092:5:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "4080:18:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                        "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                      }
                    },
                    "id": 1484,
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "memberName": "length",
                    "nodeType": "MemberAccess",
                    "referencedDeclaration": null,
                    "src": "4080:25:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 1480,
                  "id": 1485,
                  "nodeType": "Return",
                  "src": "4073:32:2"
                }
              ]
            },
            "documentation": null,
            "id": 1487,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getObligationsLength",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1477,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1476,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 1487,
                  "src": "4019:13:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1475,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "4019:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4018:15:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1480,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1479,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 1487,
                  "src": "4055:7:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1478,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "4055:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4054:9:2"
            },
            "scope": 1534,
            "src": "3989:122:2",
            "stateMutability": "view",
            "superFunction": 10994,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1498,
              "nodeType": "Block",
              "src": "4188:44:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "id": 1494,
                      "name": "totalObligation",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1040,
                      "src": "4204:15:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                        "typeString": "mapping(address => uint256)"
                      }
                    },
                    "id": 1496,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 1495,
                      "name": "agent",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1489,
                      "src": "4220:5:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "4204:22:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 1493,
                  "id": 1497,
                  "nodeType": "Return",
                  "src": "4197:29:2"
                }
              ]
            },
            "documentation": null,
            "id": 1499,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getTotalObligation",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1490,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1489,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 1499,
                  "src": "4143:13:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1488,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "4143:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4142:15:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1493,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1492,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 1499,
                  "src": "4179:7:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1491,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "4179:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4178:9:2"
            },
            "scope": 1534,
            "src": "4115:117:2",
            "stateMutability": "view",
            "superFunction": 11001,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1515,
              "nodeType": "Block",
              "src": "4330:59:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "expression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 1508,
                          "name": "obligations",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1036,
                          "src": "4346:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                            "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                          }
                        },
                        "id": 1510,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 1509,
                          "name": "agent",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1501,
                          "src": "4358:5:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "4346:18:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                          "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                        }
                      },
                      "id": 1512,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 1511,
                        "name": "position",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1503,
                        "src": "4365:8:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "4346:28:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                        "typeString": "struct AgentObligations.Obligation storage ref"
                      }
                    },
                    "id": 1513,
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "memberName": "receiver",
                    "nodeType": "MemberAccess",
                    "referencedDeclaration": 1028,
                    "src": "4346:37:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "functionReturnParameters": 1507,
                  "id": 1514,
                  "nodeType": "Return",
                  "src": "4339:44:2"
                }
              ]
            },
            "documentation": null,
            "id": 1516,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getObligationReceiver",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1504,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1501,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 1516,
                  "src": "4267:13:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1500,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "4267:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1503,
                  "name": "position",
                  "nodeType": "VariableDeclaration",
                  "scope": 1516,
                  "src": "4282:16:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1502,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "4282:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4266:33:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1507,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1506,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 1516,
                  "src": "4321:7:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1505,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "4321:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4320:9:2"
            },
            "scope": 1534,
            "src": "4236:153:2",
            "stateMutability": "view",
            "superFunction": 11010,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1532,
              "nodeType": "Block",
              "src": "4486:69:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "expression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 1525,
                          "name": "obligations",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1036,
                          "src": "4502:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                            "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                          }
                        },
                        "id": 1527,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 1526,
                          "name": "agent",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1518,
                          "src": "4514:5:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "4502:18:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                          "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                        }
                      },
                      "id": 1529,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 1528,
                        "name": "position",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1520,
                        "src": "4521:8:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "4502:28:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                        "typeString": "struct AgentObligations.Obligation storage ref"
                      }
                    },
                    "id": 1530,
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "memberName": "percentageEarnings",
                    "nodeType": "MemberAccess",
                    "referencedDeclaration": 1030,
                    "src": "4502:47:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 1524,
                  "id": 1531,
                  "nodeType": "Return",
                  "src": "4495:54:2"
                }
              ]
            },
            "documentation": null,
            "id": 1533,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getObligationPayment",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1521,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1518,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 1533,
                  "src": "4423:13:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1517,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "4423:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1520,
                  "name": "position",
                  "nodeType": "VariableDeclaration",
                  "scope": 1533,
                  "src": "4438:16:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1519,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "4438:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4422:33:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1524,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1523,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 1533,
                  "src": "4477:7:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1522,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "4477:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4476:9:2"
            },
            "scope": 1534,
            "src": "4393:162:2",
            "stateMutability": "view",
            "superFunction": 11019,
            "visibility": "public"
          }
        ],
        "scope": 1535,
        "src": "131:4427:2"
      }
    ],
    "src": "0:4559:2"
  },
  "legacyAST": {
    "absolutePath": "/home/stefan/Work/Cyberlife/chain/protocol/contracts/agents/AgentObligations.sol",
    "exportedSymbols": {
      "AgentObligations": [
        1534
      ]
    },
    "id": 1535,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 1022,
        "literals": [
          "solidity",
          "0.4",
          ".25"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:23:2"
      },
      {
        "absolutePath": "contracts/interfaces/IAgentObligations.sol",
        "file": "contracts/interfaces/IAgentObligations.sol",
        "id": 1023,
        "nodeType": "ImportDirective",
        "scope": 1535,
        "sourceUnit": 11021,
        "src": "25:52:2",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/IAgentManagement.sol",
        "file": "contracts/interfaces/IAgentManagement.sol",
        "id": 1024,
        "nodeType": "ImportDirective",
        "scope": 1535,
        "sourceUnit": 10965,
        "src": "78:51:2",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 1025,
              "name": "IAgentObligations",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 11020,
              "src": "160:17:2",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IAgentObligations_$11020",
                "typeString": "contract IAgentObligations"
              }
            },
            "id": 1026,
            "nodeType": "InheritanceSpecifier",
            "src": "160:17:2"
          }
        ],
        "contractDependencies": [
          11020
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 1534,
        "linearizedBaseContracts": [
          1534,
          11020
        ],
        "name": "AgentObligations",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "canonicalName": "AgentObligations.Obligation",
            "id": 1031,
            "members": [
              {
                "constant": false,
                "id": 1028,
                "name": "receiver",
                "nodeType": "VariableDeclaration",
                "scope": 1031,
                "src": "208:16:2",
                "stateVariable": false,
                "storageLocation": "default",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                },
                "typeName": {
                  "id": 1027,
                  "name": "address",
                  "nodeType": "ElementaryTypeName",
                  "src": "208:7:2",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  }
                },
                "value": null,
                "visibility": "internal"
              },
              {
                "constant": false,
                "id": 1030,
                "name": "percentageEarnings",
                "nodeType": "VariableDeclaration",
                "scope": 1031,
                "src": "230:26:2",
                "stateVariable": false,
                "storageLocation": "default",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                },
                "typeName": {
                  "id": 1029,
                  "name": "uint256",
                  "nodeType": "ElementaryTypeName",
                  "src": "230:7:2",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  }
                },
                "value": null,
                "visibility": "internal"
              }
            ],
            "name": "Obligation",
            "nodeType": "StructDefinition",
            "scope": 1534,
            "src": "183:79:2",
            "visibility": "public"
          },
          {
            "constant": false,
            "id": 1036,
            "name": "obligations",
            "nodeType": "VariableDeclaration",
            "scope": 1534,
            "src": "266:44:2",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
              "typeString": "mapping(address => struct AgentObligations.Obligation[])"
            },
            "typeName": {
              "id": 1035,
              "keyType": {
                "id": 1032,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "274:7:2",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "266:32:2",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                "typeString": "mapping(address => struct AgentObligations.Obligation[])"
              },
              "valueType": {
                "baseType": {
                  "contractScope": null,
                  "id": 1033,
                  "name": "Obligation",
                  "nodeType": "UserDefinedTypeName",
                  "referencedDeclaration": 1031,
                  "src": "285:10:2",
                  "typeDescriptions": {
                    "typeIdentifier": "t_struct$_Obligation_$1031_storage_ptr",
                    "typeString": "struct AgentObligations.Obligation"
                  }
                },
                "id": 1034,
                "length": null,
                "nodeType": "ArrayTypeName",
                "src": "285:12:2",
                "typeDescriptions": {
                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_ptr",
                  "typeString": "struct AgentObligations.Obligation[]"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 1040,
            "name": "totalObligation",
            "nodeType": "VariableDeclaration",
            "scope": 1534,
            "src": "314:43:2",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
              "typeString": "mapping(address => uint256)"
            },
            "typeName": {
              "id": 1039,
              "keyType": {
                "id": 1037,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "322:7:2",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "314:27:2",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                "typeString": "mapping(address => uint256)"
              },
              "valueType": {
                "id": 1038,
                "name": "uint256",
                "nodeType": "ElementaryTypeName",
                "src": "333:7:2",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 1042,
            "name": "management",
            "nodeType": "VariableDeclaration",
            "scope": 1534,
            "src": "362:27:2",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_contract$_IAgentManagement_$10964",
              "typeString": "contract IAgentManagement"
            },
            "typeName": {
              "contractScope": null,
              "id": 1041,
              "name": "IAgentManagement",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10964,
              "src": "362:16:2",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                "typeString": "contract IAgentManagement"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 1054,
              "nodeType": "Block",
              "src": "420:62:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 1050,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 1045,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 13139,
                            "src": "435:3:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 1046,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "435:10:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 1048,
                              "name": "management",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1042,
                              "src": "457:10:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                                "typeString": "contract IAgentManagement"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                                "typeString": "contract IAgentManagement"
                              }
                            ],
                            "id": 1047,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": true,
                            "lValueRequested": false,
                            "nodeType": "ElementaryTypeNameExpression",
                            "src": "449:7:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_address_$",
                              "typeString": "type(address)"
                            },
                            "typeName": "address"
                          },
                          "id": 1049,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "typeConversion",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "449:19:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "435:33:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 1044,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        13142,
                        13143
                      ],
                      "referencedDeclaration": 13142,
                      "src": "427:7:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 1051,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "427:42:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1052,
                  "nodeType": "ExpressionStatement",
                  "src": "427:42:2"
                },
                {
                  "id": 1053,
                  "nodeType": "PlaceholderStatement",
                  "src": "475:1:2"
                }
              ]
            },
            "documentation": null,
            "id": 1055,
            "name": "onlyManagement",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 1043,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "417:2:2"
            },
            "src": "394:88:2",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 1066,
              "nodeType": "Block",
              "src": "526:55:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 1064,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 1060,
                      "name": "management",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1042,
                      "src": "533:10:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "id": 1062,
                          "name": "_management",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1057,
                          "src": "563:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        ],
                        "id": 1061,
                        "name": "IAgentManagement",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 10964,
                        "src": "546:16:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_contract$_IAgentManagement_$10964_$",
                          "typeString": "type(contract IAgentManagement)"
                        }
                      },
                      "id": 1063,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "546:29:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                        "typeString": "contract IAgentManagement"
                      }
                    },
                    "src": "533:42:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                      "typeString": "contract IAgentManagement"
                    }
                  },
                  "id": 1065,
                  "nodeType": "ExpressionStatement",
                  "src": "533:42:2"
                }
              ]
            },
            "documentation": null,
            "id": 1067,
            "implemented": true,
            "isConstructor": true,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1058,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1057,
                  "name": "_management",
                  "nodeType": "VariableDeclaration",
                  "scope": 1067,
                  "src": "498:19:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1056,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "498:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "497:21:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1059,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "526:0:2"
            },
            "scope": 1534,
            "src": "486:95:2",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1317,
              "nodeType": "Block",
              "src": "675:2049:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 1079,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 1077,
                          "name": "newEarnings",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1073,
                          "src": "690:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": ">",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "30",
                          "id": 1078,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "number",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "704:1:2",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_rational_0_by_1",
                            "typeString": "int_const 0"
                          },
                          "value": "0"
                        },
                        "src": "690:15:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 1076,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        13142,
                        13143
                      ],
                      "referencedDeclaration": 13142,
                      "src": "682:7:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 1080,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "682:24:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1081,
                  "nodeType": "ExpressionStatement",
                  "src": "682:24:2"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 1085,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 1083,
                          "name": "newEarnings",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1073,
                          "src": "720:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "<=",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "313030",
                          "id": 1084,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "number",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "735:3:2",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_rational_100_by_1",
                            "typeString": "int_const 100"
                          },
                          "value": "100"
                        },
                        "src": "720:18:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 1082,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        13142,
                        13143
                      ],
                      "referencedDeclaration": 13142,
                      "src": "712:7:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 1086,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "712:27:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1087,
                  "nodeType": "ExpressionStatement",
                  "src": "712:27:2"
                },
                {
                  "assignments": [
                    1089
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1089,
                      "name": "obligationTarget",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "746:24:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 1088,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "746:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1092,
                  "initialValue": {
                    "argumentTypes": null,
                    "expression": {
                      "argumentTypes": null,
                      "id": 1090,
                      "name": "msg",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 13139,
                      "src": "773:3:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_magic_message",
                        "typeString": "msg"
                      }
                    },
                    "id": 1091,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "memberName": "sender",
                    "nodeType": "MemberAccess",
                    "referencedDeclaration": null,
                    "src": "773:10:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "746:37:2"
                },
                {
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    },
                    "id": 1097,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "id": 1093,
                      "name": "receiver",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1069,
                      "src": "794:8:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": "!=",
                    "rightExpression": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "hexValue": "30",
                          "id": 1095,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "number",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "814:1:2",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_rational_0_by_1",
                            "typeString": "int_const 0"
                          },
                          "value": "0"
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_rational_0_by_1",
                            "typeString": "int_const 0"
                          }
                        ],
                        "id": 1094,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "lValueRequested": false,
                        "nodeType": "ElementaryTypeNameExpression",
                        "src": "806:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_type$_t_address_$",
                          "typeString": "type(address)"
                        },
                        "typeName": "address"
                      },
                      "id": 1096,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "typeConversion",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "806:10:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "794:22:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "falseBody": null,
                  "id": 1102,
                  "nodeType": "IfStatement",
                  "src": "790:55:2",
                  "trueBody": {
                    "expression": {
                      "argumentTypes": null,
                      "id": 1100,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftHandSide": {
                        "argumentTypes": null,
                        "id": 1098,
                        "name": "obligationTarget",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1089,
                        "src": "818:16:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "nodeType": "Assignment",
                      "operator": "=",
                      "rightHandSide": {
                        "argumentTypes": null,
                        "id": 1099,
                        "name": "receiver",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1069,
                        "src": "837:8:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "src": "818:27:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "id": 1101,
                    "nodeType": "ExpressionStatement",
                    "src": "818:27:2"
                  }
                },
                {
                  "assignments": [
                    1104
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1104,
                      "name": "creator",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "852:15:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 1103,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "852:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1109,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 1107,
                        "name": "agentName",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1071,
                        "src": "900:9:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 1105,
                        "name": "management",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1042,
                        "src": "870:10:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 1106,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getCreatorFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10895,
                      "src": "870:29:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_address_$",
                        "typeString": "function (string memory) view external returns (address)"
                      }
                    },
                    "id": 1108,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "870:40:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "852:58:2"
                },
                {
                  "assignments": [
                    1111
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1111,
                      "name": "agentPosition",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "917:21:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 1110,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "917:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1116,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 1114,
                        "name": "agentName",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1071,
                        "src": "972:9:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_string_memory_ptr",
                          "typeString": "string memory"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 1112,
                        "name": "management",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1042,
                        "src": "941:10:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 1113,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getPositionFromName",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10902,
                      "src": "941:30:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_string_memory_ptr_$returns$_t_uint256_$",
                        "typeString": "function (string memory) view external returns (uint256)"
                      }
                    },
                    "id": 1115,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "941:41:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "917:65:2"
                },
                {
                  "assignments": [
                    1118
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1118,
                      "name": "autonomy",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "989:13:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "typeName": {
                        "id": 1117,
                        "name": "bool",
                        "nodeType": "ElementaryTypeName",
                        "src": "989:4:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1124,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 1121,
                        "name": "creator",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1104,
                        "src": "1033:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1122,
                        "name": "agentPosition",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1111,
                        "src": "1042:13:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 1119,
                        "name": "management",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1042,
                        "src": "1005:10:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 1120,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getAgentAutonomy",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10956,
                      "src": "1005:27:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_address_$_t_uint256_$returns$_t_bool_$",
                        "typeString": "function (address,uint256) view external returns (bool)"
                      }
                    },
                    "id": 1123,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1005:51:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "989:67:2"
                },
                {
                  "assignments": [
                    1126
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1126,
                      "name": "agentAddress",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "1063:20:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "typeName": {
                        "id": 1125,
                        "name": "address",
                        "nodeType": "ElementaryTypeName",
                        "src": "1063:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1132,
                  "initialValue": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 1129,
                        "name": "creator",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1104,
                        "src": "1113:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1130,
                        "name": "agentPosition",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1111,
                        "src": "1122:13:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      ],
                      "expression": {
                        "argumentTypes": null,
                        "id": 1127,
                        "name": "management",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1042,
                        "src": "1086:10:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_contract$_IAgentManagement_$10964",
                          "typeString": "contract IAgentManagement"
                        }
                      },
                      "id": 1128,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "getAgentAddress",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": 10938,
                      "src": "1086:26:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_external_view$_t_address_$_t_uint256_$returns$_t_address_$",
                        "typeString": "function (address,uint256) view external returns (address)"
                      }
                    },
                    "id": 1131,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "1086:50:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1063:73:2"
                },
                {
                  "assignments": [
                    1134
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1134,
                      "name": "obligationPosition",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "1143:26:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 1133,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1143:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1136,
                  "initialValue": {
                    "argumentTypes": null,
                    "hexValue": "30",
                    "id": 1135,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": true,
                    "kind": "number",
                    "lValueRequested": false,
                    "nodeType": "Literal",
                    "src": "1172:1:2",
                    "subdenomination": null,
                    "typeDescriptions": {
                      "typeIdentifier": "t_rational_0_by_1",
                      "typeString": "int_const 0"
                    },
                    "value": "0"
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1143:30:2"
                },
                {
                  "assignments": [
                    1138
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1138,
                      "name": "foundObligation",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "1179:20:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "typeName": {
                        "id": 1137,
                        "name": "bool",
                        "nodeType": "ElementaryTypeName",
                        "src": "1179:4:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1140,
                  "initialValue": {
                    "argumentTypes": null,
                    "hexValue": "66616c7365",
                    "id": 1139,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": true,
                    "kind": "bool",
                    "lValueRequested": false,
                    "nodeType": "Literal",
                    "src": "1202:5:2",
                    "subdenomination": null,
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    },
                    "value": "false"
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1179:28:2"
                },
                {
                  "assignments": [
                    1142
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1142,
                      "name": "agentObligations",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "1214:24:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 1141,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1214:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1144,
                  "initialValue": {
                    "argumentTypes": null,
                    "hexValue": "30",
                    "id": 1143,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": true,
                    "kind": "number",
                    "lValueRequested": false,
                    "nodeType": "Literal",
                    "src": "1241:1:2",
                    "subdenomination": null,
                    "typeDescriptions": {
                      "typeIdentifier": "t_rational_0_by_1",
                      "typeString": "int_const 0"
                    },
                    "value": "0"
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1214:28:2"
                },
                {
                  "body": {
                    "id": 1187,
                    "nodeType": "Block",
                    "src": "1309:248:2",
                    "statements": [
                      {
                        "condition": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          },
                          "id": 1165,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1158,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "1322:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1160,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1159,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1126,
                                  "src": "1334:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "1322:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1162,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1161,
                                "name": "i",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1146,
                                "src": "1348:1:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "1322:28:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1163,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "receiver",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1028,
                            "src": "1322:37:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "nodeType": "BinaryOperation",
                          "operator": "==",
                          "rightExpression": {
                            "argumentTypes": null,
                            "id": 1164,
                            "name": "obligationTarget",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1089,
                            "src": "1363:16:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "src": "1322:57:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "falseBody": null,
                        "id": 1175,
                        "nodeType": "IfStatement",
                        "src": "1318:138:2",
                        "trueBody": {
                          "id": 1174,
                          "nodeType": "Block",
                          "src": "1381:75:2",
                          "statements": [
                            {
                              "expression": {
                                "argumentTypes": null,
                                "id": 1168,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftHandSide": {
                                  "argumentTypes": null,
                                  "id": 1166,
                                  "name": "obligationPosition",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1134,
                                  "src": "1392:18:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "Assignment",
                                "operator": "=",
                                "rightHandSide": {
                                  "argumentTypes": null,
                                  "id": 1167,
                                  "name": "i",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1146,
                                  "src": "1413:1:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "src": "1392:22:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "id": 1169,
                              "nodeType": "ExpressionStatement",
                              "src": "1392:22:2"
                            },
                            {
                              "expression": {
                                "argumentTypes": null,
                                "id": 1172,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftHandSide": {
                                  "argumentTypes": null,
                                  "id": 1170,
                                  "name": "foundObligation",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1138,
                                  "src": "1424:15:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_bool",
                                    "typeString": "bool"
                                  }
                                },
                                "nodeType": "Assignment",
                                "operator": "=",
                                "rightHandSide": {
                                  "argumentTypes": null,
                                  "hexValue": "74727565",
                                  "id": 1171,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": true,
                                  "kind": "bool",
                                  "lValueRequested": false,
                                  "nodeType": "Literal",
                                  "src": "1442:4:2",
                                  "subdenomination": null,
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_bool",
                                    "typeString": "bool"
                                  },
                                  "value": "true"
                                },
                                "src": "1424:22:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_bool",
                                  "typeString": "bool"
                                }
                              },
                              "id": 1173,
                              "nodeType": "ExpressionStatement",
                              "src": "1424:22:2"
                            }
                          ]
                        }
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 1185,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "id": 1176,
                            "name": "agentObligations",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1142,
                            "src": "1464:16:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "commonType": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "id": 1184,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "leftExpression": {
                              "argumentTypes": null,
                              "id": 1177,
                              "name": "agentObligations",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1142,
                              "src": "1483:16:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "nodeType": "BinaryOperation",
                            "operator": "+",
                            "rightExpression": {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "baseExpression": {
                                    "argumentTypes": null,
                                    "id": 1178,
                                    "name": "obligations",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1036,
                                    "src": "1502:11:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                      "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                    }
                                  },
                                  "id": 1180,
                                  "indexExpression": {
                                    "argumentTypes": null,
                                    "id": 1179,
                                    "name": "agentAddress",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1126,
                                    "src": "1514:12:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_address",
                                      "typeString": "address"
                                    }
                                  },
                                  "isConstant": false,
                                  "isLValue": true,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "nodeType": "IndexAccess",
                                  "src": "1502:25:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                    "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                  }
                                },
                                "id": 1182,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1181,
                                  "name": "i",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1146,
                                  "src": "1528:1:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "1502:28:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref"
                                }
                              },
                              "id": 1183,
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "percentageEarnings",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": 1030,
                              "src": "1502:47:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "src": "1483:66:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "1464:85:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 1186,
                        "nodeType": "ExpressionStatement",
                        "src": "1464:85:2"
                      }
                    ]
                  },
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    },
                    "id": 1154,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "id": 1149,
                      "name": "i",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1146,
                      "src": "1266:1:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": "<",
                    "rightExpression": {
                      "argumentTypes": null,
                      "expression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 1150,
                          "name": "obligations",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1036,
                          "src": "1270:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                            "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                          }
                        },
                        "id": 1152,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 1151,
                          "name": "agentAddress",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1126,
                          "src": "1282:12:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "1270:25:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                          "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                        }
                      },
                      "id": 1153,
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "memberName": "length",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "1270:32:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "1266:36:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 1188,
                  "initializationExpression": {
                    "assignments": [
                      1146
                    ],
                    "declarations": [
                      {
                        "constant": false,
                        "id": 1146,
                        "name": "i",
                        "nodeType": "VariableDeclaration",
                        "scope": 1318,
                        "src": "1254:6:2",
                        "stateVariable": false,
                        "storageLocation": "default",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "typeName": {
                          "id": 1145,
                          "name": "uint",
                          "nodeType": "ElementaryTypeName",
                          "src": "1254:4:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "value": null,
                        "visibility": "internal"
                      }
                    ],
                    "id": 1148,
                    "initialValue": {
                      "argumentTypes": null,
                      "hexValue": "30",
                      "id": 1147,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "number",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "1263:1:2",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_rational_0_by_1",
                        "typeString": "int_const 0"
                      },
                      "value": "0"
                    },
                    "nodeType": "VariableDeclarationStatement",
                    "src": "1254:10:2"
                  },
                  "loopExpression": {
                    "expression": {
                      "argumentTypes": null,
                      "id": 1156,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "UnaryOperation",
                      "operator": "++",
                      "prefix": false,
                      "src": "1304:3:2",
                      "subExpression": {
                        "argumentTypes": null,
                        "id": 1155,
                        "name": "i",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1146,
                        "src": "1304:1:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "id": 1157,
                    "nodeType": "ExpressionStatement",
                    "src": "1304:3:2"
                  },
                  "nodeType": "ForStatement",
                  "src": "1249:308:2"
                },
                {
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    },
                    "id": 1193,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "commonType": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      },
                      "id": 1191,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftExpression": {
                        "argumentTypes": null,
                        "id": 1189,
                        "name": "obligationTarget",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1089,
                        "src": "1567:16:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "nodeType": "BinaryOperation",
                      "operator": "==",
                      "rightExpression": {
                        "argumentTypes": null,
                        "id": 1190,
                        "name": "creator",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1104,
                        "src": "1587:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "src": "1567:27:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": "&&",
                    "rightExpression": {
                      "argumentTypes": null,
                      "id": 1192,
                      "name": "autonomy",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1118,
                      "src": "1598:8:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "src": "1567:39:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "falseBody": null,
                  "id": 1210,
                  "nodeType": "IfStatement",
                  "src": "1563:187:2",
                  "trueBody": {
                    "id": 1209,
                    "nodeType": "Block",
                    "src": "1608:142:2",
                    "statements": [
                      {
                        "expression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "commonType": {
                                "typeIdentifier": "t_bool",
                                "typeString": "bool"
                              },
                              "id": 1206,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "leftExpression": {
                                "argumentTypes": null,
                                "commonType": {
                                  "typeIdentifier": "t_bool",
                                  "typeString": "bool"
                                },
                                "id": 1197,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftExpression": {
                                  "argumentTypes": null,
                                  "id": 1195,
                                  "name": "foundObligation",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1138,
                                  "src": "1625:15:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_bool",
                                    "typeString": "bool"
                                  }
                                },
                                "nodeType": "BinaryOperation",
                                "operator": "==",
                                "rightExpression": {
                                  "argumentTypes": null,
                                  "hexValue": "66616c7365",
                                  "id": 1196,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": true,
                                  "kind": "bool",
                                  "lValueRequested": false,
                                  "nodeType": "Literal",
                                  "src": "1644:5:2",
                                  "subdenomination": null,
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_bool",
                                    "typeString": "bool"
                                  },
                                  "value": "false"
                                },
                                "src": "1625:24:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_bool",
                                  "typeString": "bool"
                                }
                              },
                              "nodeType": "BinaryOperation",
                              "operator": "||",
                              "rightExpression": {
                                "argumentTypes": null,
                                "commonType": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                },
                                "id": 1205,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftExpression": {
                                  "argumentTypes": null,
                                  "expression": {
                                    "argumentTypes": null,
                                    "baseExpression": {
                                      "argumentTypes": null,
                                      "baseExpression": {
                                        "argumentTypes": null,
                                        "id": 1198,
                                        "name": "obligations",
                                        "nodeType": "Identifier",
                                        "overloadedDeclarations": [],
                                        "referencedDeclaration": 1036,
                                        "src": "1663:11:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                          "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                        }
                                      },
                                      "id": 1200,
                                      "indexExpression": {
                                        "argumentTypes": null,
                                        "id": 1199,
                                        "name": "agentAddress",
                                        "nodeType": "Identifier",
                                        "overloadedDeclarations": [],
                                        "referencedDeclaration": 1126,
                                        "src": "1675:12:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_address",
                                          "typeString": "address"
                                        }
                                      },
                                      "isConstant": false,
                                      "isLValue": true,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "nodeType": "IndexAccess",
                                      "src": "1663:25:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                        "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                      }
                                    },
                                    "id": 1202,
                                    "indexExpression": {
                                      "argumentTypes": null,
                                      "id": 1201,
                                      "name": "obligationPosition",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1134,
                                      "src": "1689:18:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      }
                                    },
                                    "isConstant": false,
                                    "isLValue": true,
                                    "isPure": false,
                                    "lValueRequested": false,
                                    "nodeType": "IndexAccess",
                                    "src": "1663:45:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                      "typeString": "struct AgentObligations.Obligation storage ref"
                                    }
                                  },
                                  "id": 1203,
                                  "isConstant": false,
                                  "isLValue": true,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "memberName": "percentageEarnings",
                                  "nodeType": "MemberAccess",
                                  "referencedDeclaration": 1030,
                                  "src": "1663:64:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "BinaryOperation",
                                "operator": ">",
                                "rightExpression": {
                                  "argumentTypes": null,
                                  "id": 1204,
                                  "name": "newEarnings",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1073,
                                  "src": "1730:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "src": "1663:78:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_bool",
                                  "typeString": "bool"
                                }
                              },
                              "src": "1625:116:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_bool",
                                "typeString": "bool"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_bool",
                                "typeString": "bool"
                              }
                            ],
                            "id": 1194,
                            "name": "require",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [
                              13142,
                              13143
                            ],
                            "referencedDeclaration": 13142,
                            "src": "1617:7:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                              "typeString": "function (bool) pure"
                            }
                          },
                          "id": 1207,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "1617:125:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_tuple$__$",
                            "typeString": "tuple()"
                          }
                        },
                        "id": 1208,
                        "nodeType": "ExpressionStatement",
                        "src": "1617:125:2"
                      }
                    ]
                  }
                },
                {
                  "assignments": [
                    1212
                  ],
                  "declarations": [
                    {
                      "constant": false,
                      "id": 1212,
                      "name": "difference",
                      "nodeType": "VariableDeclaration",
                      "scope": 1318,
                      "src": "1756:18:2",
                      "stateVariable": false,
                      "storageLocation": "default",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "typeName": {
                        "id": 1211,
                        "name": "uint256",
                        "nodeType": "ElementaryTypeName",
                        "src": "1756:7:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "value": null,
                      "visibility": "internal"
                    }
                  ],
                  "id": 1214,
                  "initialValue": {
                    "argumentTypes": null,
                    "hexValue": "30",
                    "id": 1213,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": true,
                    "kind": "number",
                    "lValueRequested": false,
                    "nodeType": "Literal",
                    "src": "1777:1:2",
                    "subdenomination": null,
                    "typeDescriptions": {
                      "typeIdentifier": "t_rational_0_by_1",
                      "typeString": "int_const 0"
                    },
                    "value": "0"
                  },
                  "nodeType": "VariableDeclarationStatement",
                  "src": "1756:22:2"
                },
                {
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    },
                    "id": 1217,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "id": 1215,
                      "name": "foundObligation",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1138,
                      "src": "1789:15:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": "==",
                    "rightExpression": {
                      "argumentTypes": null,
                      "hexValue": "74727565",
                      "id": 1216,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "bool",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "1808:4:2",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "value": "true"
                    },
                    "src": "1789:23:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "falseBody": {
                    "id": 1307,
                    "nodeType": "Block",
                    "src": "2459:146:2",
                    "statements": [
                      {
                        "expression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "commonType": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              },
                              "id": 1294,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "leftExpression": {
                                "argumentTypes": null,
                                "commonType": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                },
                                "id": 1292,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftExpression": {
                                  "argumentTypes": null,
                                  "id": 1290,
                                  "name": "agentObligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1142,
                                  "src": "2476:16:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "BinaryOperation",
                                "operator": "+",
                                "rightExpression": {
                                  "argumentTypes": null,
                                  "id": 1291,
                                  "name": "newEarnings",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1073,
                                  "src": "2495:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "src": "2476:30:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "nodeType": "BinaryOperation",
                              "operator": "<",
                              "rightExpression": {
                                "argumentTypes": null,
                                "hexValue": "313030",
                                "id": 1293,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": true,
                                "kind": "number",
                                "lValueRequested": false,
                                "nodeType": "Literal",
                                "src": "2509:3:2",
                                "subdenomination": null,
                                "typeDescriptions": {
                                  "typeIdentifier": "t_rational_100_by_1",
                                  "typeString": "int_const 100"
                                },
                                "value": "100"
                              },
                              "src": "2476:36:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_bool",
                                "typeString": "bool"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_bool",
                                "typeString": "bool"
                              }
                            ],
                            "id": 1289,
                            "name": "require",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [
                              13142,
                              13143
                            ],
                            "referencedDeclaration": 13142,
                            "src": "2468:7:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                              "typeString": "function (bool) pure"
                            }
                          },
                          "id": 1295,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "2468:45:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_tuple$__$",
                            "typeString": "tuple()"
                          }
                        },
                        "id": 1296,
                        "nodeType": "ExpressionStatement",
                        "src": "2468:45:2"
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 1305,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 1297,
                              "name": "totalObligation",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1040,
                              "src": "2522:15:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                "typeString": "mapping(address => uint256)"
                              }
                            },
                            "id": 1299,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 1298,
                              "name": "agentAddress",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1126,
                              "src": "2538:12:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "nodeType": "IndexAccess",
                            "src": "2522:29:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "commonType": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "id": 1304,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "leftExpression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "id": 1300,
                                "name": "totalObligation",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1040,
                                "src": "2554:15:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                  "typeString": "mapping(address => uint256)"
                                }
                              },
                              "id": 1302,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1301,
                                "name": "agentAddress",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1126,
                                "src": "2570:12:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_address",
                                  "typeString": "address"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "2554:29:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "nodeType": "BinaryOperation",
                            "operator": "+",
                            "rightExpression": {
                              "argumentTypes": null,
                              "id": 1303,
                              "name": "newEarnings",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1073,
                              "src": "2586:11:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "src": "2554:43:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "2522:75:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 1306,
                        "nodeType": "ExpressionStatement",
                        "src": "2522:75:2"
                      }
                    ]
                  },
                  "id": 1308,
                  "nodeType": "IfStatement",
                  "src": "1785:820:2",
                  "trueBody": {
                    "id": 1288,
                    "nodeType": "Block",
                    "src": "1814:639:2",
                    "statements": [
                      {
                        "condition": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          },
                          "id": 1225,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1218,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "1827:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1220,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1219,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1126,
                                  "src": "1839:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "1827:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1222,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1221,
                                "name": "obligationPosition",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1134,
                                "src": "1853:18:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "1827:45:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1223,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "percentageEarnings",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1030,
                            "src": "1827:64:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "BinaryOperation",
                          "operator": ">",
                          "rightExpression": {
                            "argumentTypes": null,
                            "id": 1224,
                            "name": "newEarnings",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1073,
                            "src": "1894:11:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "1827:78:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "falseBody": {
                          "condition": {
                            "argumentTypes": null,
                            "commonType": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            },
                            "id": 1255,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "leftExpression": {
                              "argumentTypes": null,
                              "expression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "baseExpression": {
                                    "argumentTypes": null,
                                    "id": 1248,
                                    "name": "obligations",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1036,
                                    "src": "2114:11:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                      "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                    }
                                  },
                                  "id": 1250,
                                  "indexExpression": {
                                    "argumentTypes": null,
                                    "id": 1249,
                                    "name": "agentAddress",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1126,
                                    "src": "2126:12:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_address",
                                      "typeString": "address"
                                    }
                                  },
                                  "isConstant": false,
                                  "isLValue": true,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "nodeType": "IndexAccess",
                                  "src": "2114:25:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                    "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                  }
                                },
                                "id": 1252,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1251,
                                  "name": "obligationPosition",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1134,
                                  "src": "2140:18:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "2114:45:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref"
                                }
                              },
                              "id": 1253,
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "memberName": "percentageEarnings",
                              "nodeType": "MemberAccess",
                              "referencedDeclaration": 1030,
                              "src": "2114:64:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "nodeType": "BinaryOperation",
                            "operator": "<",
                            "rightExpression": {
                              "argumentTypes": null,
                              "id": 1254,
                              "name": "newEarnings",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1073,
                              "src": "2181:11:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "src": "2114:78:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_bool",
                              "typeString": "bool"
                            }
                          },
                          "falseBody": null,
                          "id": 1286,
                          "nodeType": "IfStatement",
                          "src": "2110:336:2",
                          "trueBody": {
                            "id": 1285,
                            "nodeType": "Block",
                            "src": "2194:252:2",
                            "statements": [
                              {
                                "expression": {
                                  "argumentTypes": null,
                                  "id": 1265,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "leftHandSide": {
                                    "argumentTypes": null,
                                    "id": 1256,
                                    "name": "difference",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1212,
                                    "src": "2205:10:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "nodeType": "Assignment",
                                  "operator": "=",
                                  "rightHandSide": {
                                    "argumentTypes": null,
                                    "commonType": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    },
                                    "id": 1264,
                                    "isConstant": false,
                                    "isLValue": false,
                                    "isPure": false,
                                    "lValueRequested": false,
                                    "leftExpression": {
                                      "argumentTypes": null,
                                      "id": 1257,
                                      "name": "newEarnings",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1073,
                                      "src": "2218:11:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      }
                                    },
                                    "nodeType": "BinaryOperation",
                                    "operator": "-",
                                    "rightExpression": {
                                      "argumentTypes": null,
                                      "expression": {
                                        "argumentTypes": null,
                                        "baseExpression": {
                                          "argumentTypes": null,
                                          "baseExpression": {
                                            "argumentTypes": null,
                                            "id": 1258,
                                            "name": "obligations",
                                            "nodeType": "Identifier",
                                            "overloadedDeclarations": [],
                                            "referencedDeclaration": 1036,
                                            "src": "2232:11:2",
                                            "typeDescriptions": {
                                              "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                              "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                            }
                                          },
                                          "id": 1260,
                                          "indexExpression": {
                                            "argumentTypes": null,
                                            "id": 1259,
                                            "name": "agentAddress",
                                            "nodeType": "Identifier",
                                            "overloadedDeclarations": [],
                                            "referencedDeclaration": 1126,
                                            "src": "2244:12:2",
                                            "typeDescriptions": {
                                              "typeIdentifier": "t_address",
                                              "typeString": "address"
                                            }
                                          },
                                          "isConstant": false,
                                          "isLValue": true,
                                          "isPure": false,
                                          "lValueRequested": false,
                                          "nodeType": "IndexAccess",
                                          "src": "2232:25:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                            "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                          }
                                        },
                                        "id": 1262,
                                        "indexExpression": {
                                          "argumentTypes": null,
                                          "id": 1261,
                                          "name": "obligationPosition",
                                          "nodeType": "Identifier",
                                          "overloadedDeclarations": [],
                                          "referencedDeclaration": 1134,
                                          "src": "2258:18:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_uint256",
                                            "typeString": "uint256"
                                          }
                                        },
                                        "isConstant": false,
                                        "isLValue": true,
                                        "isPure": false,
                                        "lValueRequested": false,
                                        "nodeType": "IndexAccess",
                                        "src": "2232:45:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                          "typeString": "struct AgentObligations.Obligation storage ref"
                                        }
                                      },
                                      "id": 1263,
                                      "isConstant": false,
                                      "isLValue": true,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "memberName": "percentageEarnings",
                                      "nodeType": "MemberAccess",
                                      "referencedDeclaration": 1030,
                                      "src": "2232:64:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      }
                                    },
                                    "src": "2218:78:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "src": "2205:91:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "id": 1266,
                                "nodeType": "ExpressionStatement",
                                "src": "2205:91:2"
                              },
                              {
                                "expression": {
                                  "argumentTypes": null,
                                  "arguments": [
                                    {
                                      "argumentTypes": null,
                                      "commonType": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      },
                                      "id": 1272,
                                      "isConstant": false,
                                      "isLValue": false,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "leftExpression": {
                                        "argumentTypes": null,
                                        "commonType": {
                                          "typeIdentifier": "t_uint256",
                                          "typeString": "uint256"
                                        },
                                        "id": 1270,
                                        "isConstant": false,
                                        "isLValue": false,
                                        "isPure": false,
                                        "lValueRequested": false,
                                        "leftExpression": {
                                          "argumentTypes": null,
                                          "id": 1268,
                                          "name": "agentObligations",
                                          "nodeType": "Identifier",
                                          "overloadedDeclarations": [],
                                          "referencedDeclaration": 1142,
                                          "src": "2315:16:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_uint256",
                                            "typeString": "uint256"
                                          }
                                        },
                                        "nodeType": "BinaryOperation",
                                        "operator": "+",
                                        "rightExpression": {
                                          "argumentTypes": null,
                                          "id": 1269,
                                          "name": "difference",
                                          "nodeType": "Identifier",
                                          "overloadedDeclarations": [],
                                          "referencedDeclaration": 1212,
                                          "src": "2334:10:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_uint256",
                                            "typeString": "uint256"
                                          }
                                        },
                                        "src": "2315:29:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_uint256",
                                          "typeString": "uint256"
                                        }
                                      },
                                      "nodeType": "BinaryOperation",
                                      "operator": "<",
                                      "rightExpression": {
                                        "argumentTypes": null,
                                        "hexValue": "313030",
                                        "id": 1271,
                                        "isConstant": false,
                                        "isLValue": false,
                                        "isPure": true,
                                        "kind": "number",
                                        "lValueRequested": false,
                                        "nodeType": "Literal",
                                        "src": "2347:3:2",
                                        "subdenomination": null,
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_rational_100_by_1",
                                          "typeString": "int_const 100"
                                        },
                                        "value": "100"
                                      },
                                      "src": "2315:35:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_bool",
                                        "typeString": "bool"
                                      }
                                    }
                                  ],
                                  "expression": {
                                    "argumentTypes": [
                                      {
                                        "typeIdentifier": "t_bool",
                                        "typeString": "bool"
                                      }
                                    ],
                                    "id": 1267,
                                    "name": "require",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [
                                      13142,
                                      13143
                                    ],
                                    "referencedDeclaration": 13142,
                                    "src": "2307:7:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                                      "typeString": "function (bool) pure"
                                    }
                                  },
                                  "id": 1273,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "kind": "functionCall",
                                  "lValueRequested": false,
                                  "names": [],
                                  "nodeType": "FunctionCall",
                                  "src": "2307:44:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_tuple$__$",
                                    "typeString": "tuple()"
                                  }
                                },
                                "id": 1274,
                                "nodeType": "ExpressionStatement",
                                "src": "2307:44:2"
                              },
                              {
                                "expression": {
                                  "argumentTypes": null,
                                  "id": 1283,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "leftHandSide": {
                                    "argumentTypes": null,
                                    "baseExpression": {
                                      "argumentTypes": null,
                                      "id": 1275,
                                      "name": "totalObligation",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1040,
                                      "src": "2362:15:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                        "typeString": "mapping(address => uint256)"
                                      }
                                    },
                                    "id": 1277,
                                    "indexExpression": {
                                      "argumentTypes": null,
                                      "id": 1276,
                                      "name": "agentAddress",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1126,
                                      "src": "2378:12:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_address",
                                        "typeString": "address"
                                      }
                                    },
                                    "isConstant": false,
                                    "isLValue": true,
                                    "isPure": false,
                                    "lValueRequested": true,
                                    "nodeType": "IndexAccess",
                                    "src": "2362:29:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "nodeType": "Assignment",
                                  "operator": "=",
                                  "rightHandSide": {
                                    "argumentTypes": null,
                                    "commonType": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    },
                                    "id": 1282,
                                    "isConstant": false,
                                    "isLValue": false,
                                    "isPure": false,
                                    "lValueRequested": false,
                                    "leftExpression": {
                                      "argumentTypes": null,
                                      "baseExpression": {
                                        "argumentTypes": null,
                                        "id": 1278,
                                        "name": "totalObligation",
                                        "nodeType": "Identifier",
                                        "overloadedDeclarations": [],
                                        "referencedDeclaration": 1040,
                                        "src": "2394:15:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                          "typeString": "mapping(address => uint256)"
                                        }
                                      },
                                      "id": 1280,
                                      "indexExpression": {
                                        "argumentTypes": null,
                                        "id": 1279,
                                        "name": "agentAddress",
                                        "nodeType": "Identifier",
                                        "overloadedDeclarations": [],
                                        "referencedDeclaration": 1126,
                                        "src": "2410:12:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_address",
                                          "typeString": "address"
                                        }
                                      },
                                      "isConstant": false,
                                      "isLValue": true,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "nodeType": "IndexAccess",
                                      "src": "2394:29:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      }
                                    },
                                    "nodeType": "BinaryOperation",
                                    "operator": "+",
                                    "rightExpression": {
                                      "argumentTypes": null,
                                      "id": 1281,
                                      "name": "difference",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1212,
                                      "src": "2426:10:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_uint256",
                                        "typeString": "uint256"
                                      }
                                    },
                                    "src": "2394:42:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "src": "2362:74:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "id": 1284,
                                "nodeType": "ExpressionStatement",
                                "src": "2362:74:2"
                              }
                            ]
                          }
                        },
                        "id": 1287,
                        "nodeType": "IfStatement",
                        "src": "1823:623:2",
                        "trueBody": {
                          "id": 1247,
                          "nodeType": "Block",
                          "src": "1907:197:2",
                          "statements": [
                            {
                              "expression": {
                                "argumentTypes": null,
                                "id": 1235,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftHandSide": {
                                  "argumentTypes": null,
                                  "id": 1226,
                                  "name": "difference",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1212,
                                  "src": "1918:10:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "Assignment",
                                "operator": "=",
                                "rightHandSide": {
                                  "argumentTypes": null,
                                  "commonType": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  },
                                  "id": 1234,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "leftExpression": {
                                    "argumentTypes": null,
                                    "expression": {
                                      "argumentTypes": null,
                                      "baseExpression": {
                                        "argumentTypes": null,
                                        "baseExpression": {
                                          "argumentTypes": null,
                                          "id": 1227,
                                          "name": "obligations",
                                          "nodeType": "Identifier",
                                          "overloadedDeclarations": [],
                                          "referencedDeclaration": 1036,
                                          "src": "1931:11:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                            "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                          }
                                        },
                                        "id": 1229,
                                        "indexExpression": {
                                          "argumentTypes": null,
                                          "id": 1228,
                                          "name": "agentAddress",
                                          "nodeType": "Identifier",
                                          "overloadedDeclarations": [],
                                          "referencedDeclaration": 1126,
                                          "src": "1943:12:2",
                                          "typeDescriptions": {
                                            "typeIdentifier": "t_address",
                                            "typeString": "address"
                                          }
                                        },
                                        "isConstant": false,
                                        "isLValue": true,
                                        "isPure": false,
                                        "lValueRequested": false,
                                        "nodeType": "IndexAccess",
                                        "src": "1931:25:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                          "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                        }
                                      },
                                      "id": 1231,
                                      "indexExpression": {
                                        "argumentTypes": null,
                                        "id": 1230,
                                        "name": "obligationPosition",
                                        "nodeType": "Identifier",
                                        "overloadedDeclarations": [],
                                        "referencedDeclaration": 1134,
                                        "src": "1957:18:2",
                                        "typeDescriptions": {
                                          "typeIdentifier": "t_uint256",
                                          "typeString": "uint256"
                                        }
                                      },
                                      "isConstant": false,
                                      "isLValue": true,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "nodeType": "IndexAccess",
                                      "src": "1931:45:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                        "typeString": "struct AgentObligations.Obligation storage ref"
                                      }
                                    },
                                    "id": 1232,
                                    "isConstant": false,
                                    "isLValue": true,
                                    "isPure": false,
                                    "lValueRequested": false,
                                    "memberName": "percentageEarnings",
                                    "nodeType": "MemberAccess",
                                    "referencedDeclaration": 1030,
                                    "src": "1931:64:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "nodeType": "BinaryOperation",
                                  "operator": "-",
                                  "rightExpression": {
                                    "argumentTypes": null,
                                    "id": 1233,
                                    "name": "newEarnings",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1073,
                                    "src": "1998:11:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "src": "1931:78:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "src": "1918:91:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "id": 1236,
                              "nodeType": "ExpressionStatement",
                              "src": "1918:91:2"
                            },
                            {
                              "expression": {
                                "argumentTypes": null,
                                "id": 1245,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftHandSide": {
                                  "argumentTypes": null,
                                  "baseExpression": {
                                    "argumentTypes": null,
                                    "id": 1237,
                                    "name": "totalObligation",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1040,
                                    "src": "2020:15:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                      "typeString": "mapping(address => uint256)"
                                    }
                                  },
                                  "id": 1239,
                                  "indexExpression": {
                                    "argumentTypes": null,
                                    "id": 1238,
                                    "name": "agentAddress",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1126,
                                    "src": "2036:12:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_address",
                                      "typeString": "address"
                                    }
                                  },
                                  "isConstant": false,
                                  "isLValue": true,
                                  "isPure": false,
                                  "lValueRequested": true,
                                  "nodeType": "IndexAccess",
                                  "src": "2020:29:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "Assignment",
                                "operator": "=",
                                "rightHandSide": {
                                  "argumentTypes": null,
                                  "commonType": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  },
                                  "id": 1244,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "leftExpression": {
                                    "argumentTypes": null,
                                    "baseExpression": {
                                      "argumentTypes": null,
                                      "id": 1240,
                                      "name": "totalObligation",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1040,
                                      "src": "2052:15:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                                        "typeString": "mapping(address => uint256)"
                                      }
                                    },
                                    "id": 1242,
                                    "indexExpression": {
                                      "argumentTypes": null,
                                      "id": 1241,
                                      "name": "agentAddress",
                                      "nodeType": "Identifier",
                                      "overloadedDeclarations": [],
                                      "referencedDeclaration": 1126,
                                      "src": "2068:12:2",
                                      "typeDescriptions": {
                                        "typeIdentifier": "t_address",
                                        "typeString": "address"
                                      }
                                    },
                                    "isConstant": false,
                                    "isLValue": true,
                                    "isPure": false,
                                    "lValueRequested": false,
                                    "nodeType": "IndexAccess",
                                    "src": "2052:29:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "nodeType": "BinaryOperation",
                                  "operator": "-",
                                  "rightExpression": {
                                    "argumentTypes": null,
                                    "id": 1243,
                                    "name": "difference",
                                    "nodeType": "Identifier",
                                    "overloadedDeclarations": [],
                                    "referencedDeclaration": 1212,
                                    "src": "2084:10:2",
                                    "typeDescriptions": {
                                      "typeIdentifier": "t_uint256",
                                      "typeString": "uint256"
                                    }
                                  },
                                  "src": "2052:42:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "src": "2020:74:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "id": 1246,
                              "nodeType": "ExpressionStatement",
                              "src": "2020:74:2"
                            }
                          ]
                        }
                      }
                    ]
                  }
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 1310,
                        "name": "foundObligation",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1138,
                        "src": "2629:15:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1311,
                        "name": "obligationPosition",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1134,
                        "src": "2646:18:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1312,
                        "name": "newEarnings",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1073,
                        "src": "2674:11:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1313,
                        "name": "obligationTarget",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1089,
                        "src": "2687:16:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "id": 1314,
                        "name": "agentAddress",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1126,
                        "src": "2705:12:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      ],
                      "id": 1309,
                      "name": "applyModification",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1474,
                      "src": "2611:17:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_internal_nonpayable$_t_bool_$_t_uint256_$_t_uint256_$_t_address_$_t_address_$returns$__$",
                        "typeString": "function (bool,uint256,uint256,address,address)"
                      }
                    },
                    "id": 1315,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2611:107:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1316,
                  "nodeType": "ExpressionStatement",
                  "src": "2611:107:2"
                }
              ]
            },
            "documentation": null,
            "id": 1318,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "changeObligation",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1074,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1069,
                  "name": "receiver",
                  "nodeType": "VariableDeclaration",
                  "scope": 1318,
                  "src": "611:16:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1068,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "611:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1071,
                  "name": "agentName",
                  "nodeType": "VariableDeclaration",
                  "scope": 1318,
                  "src": "629:16:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 1070,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "629:6:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1073,
                  "name": "newEarnings",
                  "nodeType": "VariableDeclaration",
                  "scope": 1318,
                  "src": "647:19:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1072,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "647:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "610:57:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1075,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "675:0:2"
            },
            "scope": 1534,
            "src": "585:2139:2",
            "stateMutability": "nonpayable",
            "superFunction": 10975,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1335,
              "nodeType": "Block",
              "src": "2780:72:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 1327,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 1323,
                        "name": "totalObligation",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1040,
                        "src": "2787:15:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 1325,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 1324,
                        "name": "agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1320,
                        "src": "2803:5:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "2787:22:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "30",
                      "id": 1326,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "number",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "2812:1:2",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_rational_0_by_1",
                        "typeString": "int_const 0"
                      },
                      "value": "0"
                    },
                    "src": "2787:26:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 1328,
                  "nodeType": "ExpressionStatement",
                  "src": "2787:26:2"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 1333,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "UnaryOperation",
                    "operator": "delete",
                    "prefix": true,
                    "src": "2820:26:2",
                    "subExpression": {
                      "argumentTypes": null,
                      "components": [
                        {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 1329,
                            "name": "obligations",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1036,
                            "src": "2827:11:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                              "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                            }
                          },
                          "id": 1331,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 1330,
                            "name": "agent",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1320,
                            "src": "2839:5:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": true,
                          "nodeType": "IndexAccess",
                          "src": "2827:18:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                            "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                          }
                        }
                      ],
                      "id": 1332,
                      "isConstant": false,
                      "isInlineArray": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "TupleExpression",
                      "src": "2826:20:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                        "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                      }
                    },
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1334,
                  "nodeType": "ExpressionStatement",
                  "src": "2820:26:2"
                }
              ]
            },
            "documentation": null,
            "id": 1336,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "deleteAllObligations",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1321,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1320,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 1336,
                  "src": "2758:13:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1319,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "2758:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "2757:15:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1322,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "2780:0:2"
            },
            "scope": 1534,
            "src": "2728:124:2",
            "stateMutability": "nonpayable",
            "superFunction": 10980,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1433,
              "nodeType": "Block",
              "src": "2939:602:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "id": 1348,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "id": 1344,
                          "name": "obligationPosition",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1340,
                          "src": "2954:18:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "<",
                        "rightExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 1346,
                              "name": "agentAddress",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1338,
                              "src": "2996:12:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            ],
                            "id": 1345,
                            "name": "getObligationsLength",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [
                              1487
                            ],
                            "referencedDeclaration": 1487,
                            "src": "2975:20:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_internal_view$_t_address_$returns$_t_uint256_$",
                              "typeString": "function (address) view returns (uint256)"
                            }
                          },
                          "id": 1347,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "2975:34:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "src": "2954:55:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 1343,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        13142,
                        13143
                      ],
                      "referencedDeclaration": 13142,
                      "src": "2946:7:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 1349,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "2946:64:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1350,
                  "nodeType": "ExpressionStatement",
                  "src": "2946:64:2"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 1360,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "id": 1352,
                                "name": "obligations",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1036,
                                "src": "3024:11:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                  "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                }
                              },
                              "id": 1354,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1353,
                                "name": "agentAddress",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1338,
                                "src": "3036:12:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_address",
                                  "typeString": "address"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3024:25:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                              }
                            },
                            "id": 1356,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 1355,
                              "name": "i",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1379,
                              "src": "3050:1:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "3024:28:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                              "typeString": "struct AgentObligations.Obligation storage ref"
                            }
                          },
                          "id": 1357,
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "receiver",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": 1028,
                          "src": "3024:37:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 1358,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 13139,
                            "src": "3065:3:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 1359,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "3065:10:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "3024:51:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 1351,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        13142,
                        13143
                      ],
                      "referencedDeclaration": 13142,
                      "src": "3016:7:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 1361,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "3016:60:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 1362,
                  "nodeType": "ExpressionStatement",
                  "src": "3016:60:2"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 1376,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 1363,
                        "name": "totalObligation",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1040,
                        "src": "3083:15:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 1365,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 1364,
                        "name": "agentAddress",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1338,
                        "src": "3099:12:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "3083:29:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "commonType": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "id": 1375,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 1366,
                          "name": "totalObligation",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1040,
                          "src": "3115:15:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                            "typeString": "mapping(address => uint256)"
                          }
                        },
                        "id": 1368,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 1367,
                          "name": "agentAddress",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1338,
                          "src": "3131:12:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "3115:29:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "nodeType": "BinaryOperation",
                      "operator": "-",
                      "rightExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 1369,
                              "name": "obligations",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1036,
                              "src": "3153:11:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                              }
                            },
                            "id": 1371,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 1370,
                              "name": "agentAddress",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1338,
                              "src": "3165:12:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "3153:25:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                              "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                            }
                          },
                          "id": 1373,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 1372,
                            "name": "i",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1379,
                            "src": "3179:1:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "3153:28:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                            "typeString": "struct AgentObligations.Obligation storage ref"
                          }
                        },
                        "id": 1374,
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "percentageEarnings",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 1030,
                        "src": "3153:47:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "src": "3115:85:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "3083:117:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 1377,
                  "nodeType": "ExpressionStatement",
                  "src": "3083:117:2"
                },
                {
                  "body": {
                    "id": 1425,
                    "nodeType": "Block",
                    "src": "3288:207:2",
                    "statements": [
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 1407,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1393,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "3297:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1396,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1394,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1338,
                                  "src": "3309:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "3297:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1397,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1395,
                                "name": "i",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1379,
                                "src": "3323:1:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3297:28:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1398,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "memberName": "receiver",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1028,
                            "src": "3297:37:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1399,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "3337:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1401,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1400,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1338,
                                  "src": "3349:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "3337:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1405,
                              "indexExpression": {
                                "argumentTypes": null,
                                "commonType": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                },
                                "id": 1404,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftExpression": {
                                  "argumentTypes": null,
                                  "id": 1402,
                                  "name": "i",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1379,
                                  "src": "3363:1:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "BinaryOperation",
                                "operator": "+",
                                "rightExpression": {
                                  "argumentTypes": null,
                                  "hexValue": "31",
                                  "id": 1403,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": true,
                                  "kind": "number",
                                  "lValueRequested": false,
                                  "nodeType": "Literal",
                                  "src": "3367:1:2",
                                  "subdenomination": null,
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_rational_1_by_1",
                                    "typeString": "int_const 1"
                                  },
                                  "value": "1"
                                },
                                "src": "3363:5:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3337:32:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1406,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "receiver",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1028,
                            "src": "3337:41:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "src": "3297:81:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "id": 1408,
                        "nodeType": "ExpressionStatement",
                        "src": "3297:81:2"
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "id": 1423,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "leftHandSide": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1409,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "3386:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1412,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1410,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1338,
                                  "src": "3398:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "3386:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1413,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1411,
                                "name": "i",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1379,
                                "src": "3412:1:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3386:28:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1414,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": true,
                            "memberName": "percentageEarnings",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1030,
                            "src": "3386:47:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "nodeType": "Assignment",
                          "operator": "=",
                          "rightHandSide": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "baseExpression": {
                                  "argumentTypes": null,
                                  "id": 1415,
                                  "name": "obligations",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1036,
                                  "src": "3436:11:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                    "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                  }
                                },
                                "id": 1417,
                                "indexExpression": {
                                  "argumentTypes": null,
                                  "id": 1416,
                                  "name": "agentAddress",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1338,
                                  "src": "3448:12:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_address",
                                    "typeString": "address"
                                  }
                                },
                                "isConstant": false,
                                "isLValue": true,
                                "isPure": false,
                                "lValueRequested": false,
                                "nodeType": "IndexAccess",
                                "src": "3436:25:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                  "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                                }
                              },
                              "id": 1421,
                              "indexExpression": {
                                "argumentTypes": null,
                                "commonType": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                },
                                "id": 1420,
                                "isConstant": false,
                                "isLValue": false,
                                "isPure": false,
                                "lValueRequested": false,
                                "leftExpression": {
                                  "argumentTypes": null,
                                  "id": 1418,
                                  "name": "i",
                                  "nodeType": "Identifier",
                                  "overloadedDeclarations": [],
                                  "referencedDeclaration": 1379,
                                  "src": "3462:1:2",
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_uint256",
                                    "typeString": "uint256"
                                  }
                                },
                                "nodeType": "BinaryOperation",
                                "operator": "+",
                                "rightExpression": {
                                  "argumentTypes": null,
                                  "hexValue": "31",
                                  "id": 1419,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": true,
                                  "kind": "number",
                                  "lValueRequested": false,
                                  "nodeType": "Literal",
                                  "src": "3466:1:2",
                                  "subdenomination": null,
                                  "typeDescriptions": {
                                    "typeIdentifier": "t_rational_1_by_1",
                                    "typeString": "int_const 1"
                                  },
                                  "value": "1"
                                },
                                "src": "3462:5:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_uint256",
                                  "typeString": "uint256"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3436:32:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref"
                              }
                            },
                            "id": 1422,
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "percentageEarnings",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": 1030,
                            "src": "3436:51:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "src": "3386:101:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 1424,
                        "nodeType": "ExpressionStatement",
                        "src": "3386:101:2"
                      }
                    ]
                  },
                  "condition": {
                    "argumentTypes": null,
                    "commonType": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    },
                    "id": 1389,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftExpression": {
                      "argumentTypes": null,
                      "id": 1382,
                      "name": "i",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1379,
                      "src": "3241:1:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "BinaryOperation",
                    "operator": "<",
                    "rightExpression": {
                      "argumentTypes": null,
                      "commonType": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      },
                      "id": 1388,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 1383,
                            "name": "obligations",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1036,
                            "src": "3245:11:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                              "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                            }
                          },
                          "id": 1385,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 1384,
                            "name": "agentAddress",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1338,
                            "src": "3257:12:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "3245:25:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                            "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                          }
                        },
                        "id": 1386,
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "length",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "3245:32:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "nodeType": "BinaryOperation",
                      "operator": "-",
                      "rightExpression": {
                        "argumentTypes": null,
                        "hexValue": "31",
                        "id": 1387,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "number",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "3280:1:2",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_rational_1_by_1",
                          "typeString": "int_const 1"
                        },
                        "value": "1"
                      },
                      "src": "3245:36:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "3241:40:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 1426,
                  "initializationExpression": {
                    "assignments": [
                      1379
                    ],
                    "declarations": [
                      {
                        "constant": false,
                        "id": 1379,
                        "name": "i",
                        "nodeType": "VariableDeclaration",
                        "scope": 1434,
                        "src": "3212:6:2",
                        "stateVariable": false,
                        "storageLocation": "default",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        },
                        "typeName": {
                          "id": 1378,
                          "name": "uint",
                          "nodeType": "ElementaryTypeName",
                          "src": "3212:4:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "value": null,
                        "visibility": "internal"
                      }
                    ],
                    "id": 1381,
                    "initialValue": {
                      "argumentTypes": null,
                      "id": 1380,
                      "name": "obligationPosition",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1340,
                      "src": "3221:18:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "VariableDeclarationStatement",
                    "src": "3212:27:2"
                  },
                  "loopExpression": {
                    "expression": {
                      "argumentTypes": null,
                      "id": 1391,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "UnaryOperation",
                      "operator": "++",
                      "prefix": false,
                      "src": "3283:3:2",
                      "subExpression": {
                        "argumentTypes": null,
                        "id": 1390,
                        "name": "i",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1379,
                        "src": "3283:1:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "id": 1392,
                    "nodeType": "ExpressionStatement",
                    "src": "3283:3:2"
                  },
                  "nodeType": "ForStatement",
                  "src": "3207:288:2"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 1431,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "UnaryOperation",
                    "operator": "--",
                    "prefix": false,
                    "src": "3501:34:2",
                    "subExpression": {
                      "argumentTypes": null,
                      "expression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 1427,
                          "name": "obligations",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1036,
                          "src": "3501:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                            "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                          }
                        },
                        "id": 1429,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 1428,
                          "name": "agentAddress",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1338,
                          "src": "3513:12:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "3501:25:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                          "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                        }
                      },
                      "id": 1430,
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "memberName": "length",
                      "nodeType": "MemberAccess",
                      "referencedDeclaration": null,
                      "src": "3501:32:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 1432,
                  "nodeType": "ExpressionStatement",
                  "src": "3501:34:2"
                }
              ]
            },
            "documentation": null,
            "id": 1434,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "deleteObligation",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1341,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1338,
                  "name": "agentAddress",
                  "nodeType": "VariableDeclaration",
                  "scope": 1434,
                  "src": "2882:20:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1337,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "2882:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1340,
                  "name": "obligationPosition",
                  "nodeType": "VariableDeclaration",
                  "scope": 1434,
                  "src": "2904:26:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1339,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "2904:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "2881:50:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1342,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "2939:0:2"
            },
            "scope": 1534,
            "src": "2856:685:2",
            "stateMutability": "nonpayable",
            "superFunction": 10987,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1473,
              "nodeType": "Block",
              "src": "3708:264:2",
              "statements": [
                {
                  "condition": {
                    "argumentTypes": null,
                    "id": 1447,
                    "name": "foundObligation",
                    "nodeType": "Identifier",
                    "overloadedDeclarations": [],
                    "referencedDeclaration": 1436,
                    "src": "3719:15:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "falseBody": {
                    "id": 1471,
                    "nodeType": "Block",
                    "src": "3832:135:2",
                    "statements": [
                      {
                        "assignments": [
                          1458
                        ],
                        "declarations": [
                          {
                            "constant": false,
                            "id": 1458,
                            "name": "newObligation",
                            "nodeType": "VariableDeclaration",
                            "scope": 1474,
                            "src": "3841:31:2",
                            "stateVariable": false,
                            "storageLocation": "memory",
                            "typeDescriptions": {
                              "typeIdentifier": "t_struct$_Obligation_$1031_memory_ptr",
                              "typeString": "struct AgentObligations.Obligation"
                            },
                            "typeName": {
                              "contractScope": null,
                              "id": 1457,
                              "name": "Obligation",
                              "nodeType": "UserDefinedTypeName",
                              "referencedDeclaration": 1031,
                              "src": "3841:10:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_storage_ptr",
                                "typeString": "struct AgentObligations.Obligation"
                              }
                            },
                            "value": null,
                            "visibility": "internal"
                          }
                        ],
                        "id": 1463,
                        "initialValue": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 1460,
                              "name": "caller",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1442,
                              "src": "3886:6:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            {
                              "argumentTypes": null,
                              "id": 1461,
                              "name": "newEarnings",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1440,
                              "src": "3894:11:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              },
                              {
                                "typeIdentifier": "t_uint256",
                                "typeString": "uint256"
                              }
                            ],
                            "id": 1459,
                            "name": "Obligation",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1031,
                            "src": "3875:10:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_type$_t_struct$_Obligation_$1031_storage_ptr_$",
                              "typeString": "type(struct AgentObligations.Obligation storage pointer)"
                            }
                          },
                          "id": 1462,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "structConstructorCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "3875:31:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_struct$_Obligation_$1031_memory",
                            "typeString": "struct AgentObligations.Obligation memory"
                          }
                        },
                        "nodeType": "VariableDeclarationStatement",
                        "src": "3841:65:2"
                      },
                      {
                        "expression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 1468,
                              "name": "newObligation",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1458,
                              "src": "3945:13:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_struct$_Obligation_$1031_memory_ptr",
                                "typeString": "struct AgentObligations.Obligation memory"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_struct$_Obligation_$1031_memory_ptr",
                                "typeString": "struct AgentObligations.Obligation memory"
                              }
                            ],
                            "expression": {
                              "argumentTypes": null,
                              "baseExpression": {
                                "argumentTypes": null,
                                "id": 1464,
                                "name": "obligations",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1036,
                                "src": "3914:11:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                  "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                                }
                              },
                              "id": 1466,
                              "indexExpression": {
                                "argumentTypes": null,
                                "id": 1465,
                                "name": "agentAddress",
                                "nodeType": "Identifier",
                                "overloadedDeclarations": [],
                                "referencedDeclaration": 1444,
                                "src": "3926:12:2",
                                "typeDescriptions": {
                                  "typeIdentifier": "t_address",
                                  "typeString": "address"
                                }
                              },
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "nodeType": "IndexAccess",
                              "src": "3914:25:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                                "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                              }
                            },
                            "id": 1467,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "push",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "3914:30:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_arraypush_nonpayable$_t_struct$_Obligation_$1031_storage_$returns$_t_uint256_$",
                              "typeString": "function (struct AgentObligations.Obligation storage ref) returns (uint256)"
                            }
                          },
                          "id": 1469,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "3914:45:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 1470,
                        "nodeType": "ExpressionStatement",
                        "src": "3914:45:2"
                      }
                    ]
                  },
                  "id": 1472,
                  "nodeType": "IfStatement",
                  "src": "3715:252:2",
                  "trueBody": {
                    "expression": {
                      "argumentTypes": null,
                      "id": 1455,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "lValueRequested": false,
                      "leftHandSide": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "baseExpression": {
                              "argumentTypes": null,
                              "id": 1448,
                              "name": "obligations",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1036,
                              "src": "3742:11:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                                "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                              }
                            },
                            "id": 1451,
                            "indexExpression": {
                              "argumentTypes": null,
                              "id": 1449,
                              "name": "agentAddress",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1444,
                              "src": "3754:12:2",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            },
                            "isConstant": false,
                            "isLValue": true,
                            "isPure": false,
                            "lValueRequested": false,
                            "nodeType": "IndexAccess",
                            "src": "3742:25:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                              "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                            }
                          },
                          "id": 1452,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 1450,
                            "name": "obligationPosition",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 1438,
                            "src": "3768:18:2",
                            "typeDescriptions": {
                              "typeIdentifier": "t_uint256",
                              "typeString": "uint256"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "3742:45:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                            "typeString": "struct AgentObligations.Obligation storage ref"
                          }
                        },
                        "id": 1453,
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": true,
                        "memberName": "percentageEarnings",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 1030,
                        "src": "3742:64:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "nodeType": "Assignment",
                      "operator": "=",
                      "rightHandSide": {
                        "argumentTypes": null,
                        "id": 1454,
                        "name": "newEarnings",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1440,
                        "src": "3809:11:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "src": "3742:78:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "id": 1456,
                    "nodeType": "ExpressionStatement",
                    "src": "3742:78:2"
                  }
                }
              ]
            },
            "documentation": null,
            "id": 1474,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": false,
            "modifiers": [],
            "name": "applyModification",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1445,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1436,
                  "name": "foundObligation",
                  "nodeType": "VariableDeclaration",
                  "scope": 1474,
                  "src": "3585:20:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_bool",
                    "typeString": "bool"
                  },
                  "typeName": {
                    "id": 1435,
                    "name": "bool",
                    "nodeType": "ElementaryTypeName",
                    "src": "3585:4:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1438,
                  "name": "obligationPosition",
                  "nodeType": "VariableDeclaration",
                  "scope": 1474,
                  "src": "3607:26:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1437,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "3607:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1440,
                  "name": "newEarnings",
                  "nodeType": "VariableDeclaration",
                  "scope": 1474,
                  "src": "3641:19:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1439,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "3641:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1442,
                  "name": "caller",
                  "nodeType": "VariableDeclaration",
                  "scope": 1474,
                  "src": "3662:14:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1441,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "3662:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1444,
                  "name": "agentAddress",
                  "nodeType": "VariableDeclaration",
                  "scope": 1474,
                  "src": "3678:20:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1443,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "3678:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "3584:115:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1446,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "3708:0:2"
            },
            "scope": 1534,
            "src": "3558:414:2",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "private"
          },
          {
            "body": {
              "id": 1486,
              "nodeType": "Block",
              "src": "4064:47:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "expression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 1481,
                        "name": "obligations",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1036,
                        "src": "4080:11:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                          "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                        }
                      },
                      "id": 1483,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 1482,
                        "name": "agent",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1476,
                        "src": "4092:5:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "4080:18:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                        "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                      }
                    },
                    "id": 1484,
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "memberName": "length",
                    "nodeType": "MemberAccess",
                    "referencedDeclaration": null,
                    "src": "4080:25:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 1480,
                  "id": 1485,
                  "nodeType": "Return",
                  "src": "4073:32:2"
                }
              ]
            },
            "documentation": null,
            "id": 1487,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getObligationsLength",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1477,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1476,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 1487,
                  "src": "4019:13:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1475,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "4019:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4018:15:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1480,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1479,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 1487,
                  "src": "4055:7:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1478,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "4055:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4054:9:2"
            },
            "scope": 1534,
            "src": "3989:122:2",
            "stateMutability": "view",
            "superFunction": 10994,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1498,
              "nodeType": "Block",
              "src": "4188:44:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "id": 1494,
                      "name": "totalObligation",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1040,
                      "src": "4204:15:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                        "typeString": "mapping(address => uint256)"
                      }
                    },
                    "id": 1496,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 1495,
                      "name": "agent",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1489,
                      "src": "4220:5:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "4204:22:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 1493,
                  "id": 1497,
                  "nodeType": "Return",
                  "src": "4197:29:2"
                }
              ]
            },
            "documentation": null,
            "id": 1499,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getTotalObligation",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1490,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1489,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 1499,
                  "src": "4143:13:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1488,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "4143:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4142:15:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1493,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1492,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 1499,
                  "src": "4179:7:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1491,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "4179:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4178:9:2"
            },
            "scope": 1534,
            "src": "4115:117:2",
            "stateMutability": "view",
            "superFunction": 11001,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1515,
              "nodeType": "Block",
              "src": "4330:59:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "expression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 1508,
                          "name": "obligations",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1036,
                          "src": "4346:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                            "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                          }
                        },
                        "id": 1510,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 1509,
                          "name": "agent",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1501,
                          "src": "4358:5:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "4346:18:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                          "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                        }
                      },
                      "id": 1512,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 1511,
                        "name": "position",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1503,
                        "src": "4365:8:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "4346:28:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                        "typeString": "struct AgentObligations.Obligation storage ref"
                      }
                    },
                    "id": 1513,
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "memberName": "receiver",
                    "nodeType": "MemberAccess",
                    "referencedDeclaration": 1028,
                    "src": "4346:37:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "functionReturnParameters": 1507,
                  "id": 1514,
                  "nodeType": "Return",
                  "src": "4339:44:2"
                }
              ]
            },
            "documentation": null,
            "id": 1516,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getObligationReceiver",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1504,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1501,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 1516,
                  "src": "4267:13:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1500,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "4267:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1503,
                  "name": "position",
                  "nodeType": "VariableDeclaration",
                  "scope": 1516,
                  "src": "4282:16:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1502,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "4282:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4266:33:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1507,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1506,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 1516,
                  "src": "4321:7:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1505,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "4321:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4320:9:2"
            },
            "scope": 1534,
            "src": "4236:153:2",
            "stateMutability": "view",
            "superFunction": 11010,
            "visibility": "public"
          },
          {
            "body": {
              "id": 1532,
              "nodeType": "Block",
              "src": "4486:69:2",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "expression": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "baseExpression": {
                          "argumentTypes": null,
                          "id": 1525,
                          "name": "obligations",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1036,
                          "src": "4502:11:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_mapping$_t_address_$_t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage_$",
                            "typeString": "mapping(address => struct AgentObligations.Obligation storage ref[] storage ref)"
                          }
                        },
                        "id": 1527,
                        "indexExpression": {
                          "argumentTypes": null,
                          "id": 1526,
                          "name": "agent",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1518,
                          "src": "4514:5:2",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "isConstant": false,
                        "isLValue": true,
                        "isPure": false,
                        "lValueRequested": false,
                        "nodeType": "IndexAccess",
                        "src": "4502:18:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_array$_t_struct$_Obligation_$1031_storage_$dyn_storage",
                          "typeString": "struct AgentObligations.Obligation storage ref[] storage ref"
                        }
                      },
                      "id": 1529,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 1528,
                        "name": "position",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 1520,
                        "src": "4521:8:2",
                        "typeDescriptions": {
                          "typeIdentifier": "t_uint256",
                          "typeString": "uint256"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": false,
                      "nodeType": "IndexAccess",
                      "src": "4502:28:2",
                      "typeDescriptions": {
                        "typeIdentifier": "t_struct$_Obligation_$1031_storage",
                        "typeString": "struct AgentObligations.Obligation storage ref"
                      }
                    },
                    "id": 1530,
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "memberName": "percentageEarnings",
                    "nodeType": "MemberAccess",
                    "referencedDeclaration": 1030,
                    "src": "4502:47:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 1524,
                  "id": 1531,
                  "nodeType": "Return",
                  "src": "4495:54:2"
                }
              ]
            },
            "documentation": null,
            "id": 1533,
            "implemented": true,
            "isConstructor": false,
            "isDeclaredConst": true,
            "modifiers": [],
            "name": "getObligationPayment",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 1521,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1518,
                  "name": "agent",
                  "nodeType": "VariableDeclaration",
                  "scope": 1533,
                  "src": "4423:13:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 1517,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "4423:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                },
                {
                  "constant": false,
                  "id": 1520,
                  "name": "position",
                  "nodeType": "VariableDeclaration",
                  "scope": 1533,
                  "src": "4438:16:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1519,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "4438:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4422:33:2"
            },
            "payable": false,
            "returnParameters": {
              "id": 1524,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 1523,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 1533,
                  "src": "4477:7:2",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 1522,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "4477:7:2",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "4476:9:2"
            },
            "scope": 1534,
            "src": "4393:162:2",
            "stateMutability": "view",
            "superFunction": 11019,
            "visibility": "public"
          }
        ],
        "scope": 1535,
        "src": "131:4427:2"
      }
    ],
    "src": "0:4559:2"
  },
  "compiler": {
    "name": "solc",
    "version": "0.4.25+commit.59dbf8f1.Emscripten.clang"
  },
  "networks": {
    "1547422729677": {
      "events": {},
      "links": {},
      "address": "0x6ec30c7f51358ed91f45ee084a53fef8b5de8081",
      "transactionHash": "0xb95c299ce1f5c188645ed45f951ed26d21a45c35cff8f9bac440faddca012db7"
    }
  },
  "schemaVersion": "2.0.2",
  "updatedAt": "2019-01-13T23:54:56.924Z"
}

module.exports = {

  agentObligations

}
