const PeerId = require('peer-id')
const PeerInfo = require('peer-info')
const Node = require('../libp2p-bundle')
var fs = require('fs')
const async = require('async')
var shell = require('shelljs')
var chain = require('../chainInteraction')
const BigNumber = require('bignumber.js')

var hostExecutor = require('./hostExecutor')

var nodeID
var ip
var portNr
var cliPath
var pbk

function nodeCreation(_nodeID, _ip, _portNr, _cliPath, _pbk, callback) {

  ip = _ip
  portNr = _portNr
  cliPath = _cliPath
  pbk = _pbk

  if (_nodeID == undefined) {

    nodeID = undefined

    createNode(function(err, result) {

      if (err) callback(undefined, undefined)

      if (result != undefined) callback(undefined, result)

    })

  } else {

    nodeID = _nodeID

    nodeFromID(nodeID, function(err, result) {

      if (err) callback(undefined, undefined)

      if (result != undefined) callback(undefined, result)

    })

  }

}

//CREATE NODE FROM ID COMING FROM THE CONSOLE

function nodeFromID(idPath, callback) {

  if(idPath == undefined) {console.log('You did not give any ID'); return}

  let node

  var id = fs.readFileSync('./ids/id' + idPath.toString()).toString()

  PeerId.createFromJSON(JSON.parse(id), (err, id) => {

    if (err) throw err;

    const peer = new PeerInfo(id);

    var composedAddr = '/ip4/' + ip + '/tcp/' + portNr.toString()

    peer.multiaddrs.add(composedAddr)

    node = new Node({

      peerInfo: peer

    })

    node.start((err) => {

      if (err) {
        throw err

      }

      process.stdin.setEncoding('utf8')

      process.openStdin().on('data', (chunk) => {

        var commandParts = chunk.split(' ')

        if (commandParts[0] == 'host') {

          var adjusted_command = chunk.replace('host ','');

          shell.exec(cliPath + adjusted_command.toString(), {silent:true}, function(code, stdout, stderr) {

          var action;

          try{

            action = JSON.parse(stdout)

          } catch(exception) {}

            if (stdout.charAt(0) == '{' && stdout.charAt(stdout.length - 2) == '}') {

              hostExecutor.executeAction(pbk, stdout)

            } else {

              console.log(stdout);

            }

         })

        }

      })

    })

    callback(null, node)

  })

}

//CREATE BRAND NEW NODE

function createNode(callback) {

  let node

  PeerId.create({bits:512}, (err, _id) => {

    if (err) throw err;

    const peer = new PeerInfo(_id)

    var composedAddr = '/ip4/' + ip + '/tcp/' + portNr.toString()

    peer.multiaddrs.add(composedAddr)

    node = new Node({

      peerInfo: peer

    })

    node.start((err) => {

      if (err) {
        throw err

      }

      process.stdin.setEncoding('utf8')

      process.openStdin().on('data', (chunk) => {

        var commandParts = chunk.split(' ')

        if (commandParts[0] == 'host') {

          var adjusted_command = chunk.replace('host ','');

          shell.exec(cliPath + adjusted_command.toString(), {silent:true}, function(code, stdout, stderr) {

          var action;

          try{

            action = JSON.parse(stdout)

          } catch(exception) {}

            if (stdout.charAt(0) == '{' && stdout.charAt(stdout.length - 2) == '}') {

              hostExecutor.executeAction(pbk, stdout)

            } else {

              console.log(stdout);

            }

         })

        }

      })

    })

    callback(null, node)

  })

}

module.exports = {

  nodeCreation

}
