'use strict'
/* eslint-disable no-console */

const PeerId = require('peer-id')
const PeerInfo = require('peer-info')
const Node = require('../libp2p-bundle')
const pull = require('pull-stream')
const Pushable = require('pull-pushable')
const p = Pushable()
var fs = require('fs')
const async = require('async')
var shell = require('shelljs')
var argv = require('optimist').argv
var waitjs = require('waitjs')

var hostChainCalls = require('../general/hostChainCalls')
var manager = require('./hostAgentManager')
var hostNode = require('./hostNode')
var hostSignalManager = require('./hostSignalManager')

var protocolName = '/cyberlife'
var peerList = []
var personalNode
var portNr
var ip
var cliPath
var privKey
var pbk
var nodeID

var agents_location
var parsed_action = ""

var migrationCheckTime = 10

//Get values from console

agents_location = argv.storage;
portNr = argv.port
ip = argv.ip
cliPath = argv.cli
privKey = argv.priv
pbk = argv.pbk
nodeID = argv.id

pbk = "0x" + pbk.toString()

//Check console values

if (cliPath == undefined) {console.log("Please input a valid CLI path"); return}

if (privKey == undefined)
  {console.log("Please input a valid private key"); return}

if (portNr == undefined || ip == undefined)
  {console.log("Please input a correct port number and IP"); return}

//Start node

async.parallel([
  (cb) => hostNode.nodeCreation(nodeID, ip, portNr, cliPath, pbk, cb)
], (err, nodes) => {
  if (err) { throw err }

  personalNode = nodes[0]

  setupCyberNode()

})

//Start CLI

shell.exec(cliPath, function(code, stdout, stderr) {})

async function setupCyberNode() {

  personalNode.on('peer:discovery', (peer) => saveNewPeer(peer))

  manager.update_hosted_agents(pbk, agents_location, personalNode)

  repeat(10*1000, function() {

    manager.migrateAgents(peerList, personalNode, agents_location)

  }, "migrateAgents");

  personalNode.handle(protocolName, (protocol, conn) => {
    pull(
      p,
      conn
    )

    pull(
      conn,
      pull.map((data) => {

        var commandParts = data.toString('utf8').split(' ')

        if (commandParts[0] == 'execute') {

          manager.execute(personalNode, agents_location, commandParts[1],
                          commandParts[2], commandParts[3], commandParts[4],
                          commandParts[5], commandParts[6], peerList)

        } else {

          var prepared_data = data.toString('utf8').replace('\n', '')

          var is_json = false

          try{

            parsed_action = JSON.parse(prepared_data)

            is_json = true

          } catch(error) {}

          if (is_json) {

            is_json = false

            hostSignalManager.receivedSignal
              (pbk, personalNode, agents_location, parsed_action)

          }

        }

    }),

    pull.drain()

  )

  })

}

function saveNewPeer(peer) {

  if (!isPeerSaved(peer.id.toB58String()))
    peerList.push(peer);

}

function isPeerSaved(peerID) {

  for (var i = 0; i < peerList.length; i++) {

    if (peerID == peerList[i].id.toB58String()) return true;

  }

  return false;

}
