var hostChainCalls = require('../general/hostChainCalls')
var manager = require('./hostAgentManager')

async function receivedSignal(pbk, personalNode, agents_location, signalData) {

  if (signalData.type == "install_agent") {

    manager.install_agent(pbk, personalNode, agents_location, signalData.name);

  } else if (signalData.type == "deployment_response") {

    hostChainCalls.respondToRequest(pbk, personalNode, signalData.name, signalData.position)

  } else if (signalData.type == "shard_modif") {

    hostChainCalls.voteModification(pbk, signalData.name)

  } else if (signalData.type == "uninstall_agent") {

    manager.uninstall_agent(pbk, agents_location, signalData.name)

  }

}

module.exports = {

  receivedSignal

}
