const AgentManagement = artifacts.require("contracts/agents/AgentManagement.sol")
const EphemeralPermissions = artifacts.require("contracts/agents/EphemeralPermissions.sol")
const HostManagement = artifacts.require("contracts/hosts/HostManagement.sol")
const Staking = artifacts.require("contracts/security/Staking.sol")
const WorkToken = artifacts.require("contracts/token/WorkToken.sol")

var sha3 = require('ethereumjs-util').sha3

//var Web3 = require('web3')

//var web3 = new Web3()

const timeManipulation = require("./helpers/timeManipulation");

contract('AgentManagement', function(accounts) {

    let management, permissions, hostManagement, staking, work

    let createAgentParamsHash = '0x51a1f39ed90e21926468211ed2f02e0d8281ee7067aa60673c2a3c28a8605e4b'

    let ID1 = '0000000000000000000000000000000000000000111111'

    let minimumExecutionTime = 30

    beforeEach(async () => {

        work = await WorkToken.new("WORK", "WOR", 18, {from: accounts[0]})

        staking = await Staking.new(work.address, 200000000000000000000, {from: accounts[0]})

        await work.mint(accounts[0], 1000000000000000000000000000, {from: accounts[0]})

        await work.mint(accounts[1], 1000000000000000000000000000, {from: accounts[0]})

        await work.approve(staking.address, 300000000000000000000, {from: accounts[1]})


        management = await AgentManagement.new(1, {from: accounts[0]})

        hostManagement = await HostManagement.new(staking.address, staking.address, {from: accounts[0]});

        permissions = await EphemeralPermissions.new(30, hostManagement.address, {from: accounts[0]})


        await management.setEphemeral(permissions.address, {from: accounts[0]})


        await staking.stake({from: accounts[1]})

        await hostManagement.setID(ID1, {from: accounts[1]})

        await hostManagement.changeRent(1, {from: accounts[1]})

        await timeManipulation.advanceTimeAndBlock(50000);

        await hostManagement.broadcastActivity({from: accounts[1]})


        await permissions.toggleAllowedContract(management.address, {from: accounts[0]})

    });

    it("Change minimum shard size", async () => {

        await management.changeMinimumShardSize(2, {from: accounts[0]});

        assert.equal(2, await management.getMinimumShardSize());

    });

    it("Create agent -- human creator", async () => {

        await management.newAgent(false, "", 0, "Simple Agent", "www.github.com/simple_agent", "codeHash", accounts[4], {from: accounts[0]});

        assert.equal(await management.getAgentName(accounts[0], 0), "Simple Agent");

    })

    it("Create agent -- agent creator", async () => {

        var codeHash = '0x' + sha3("newHash").toString('hex')

        //Commented because of JSON RPC bug with web3

      /*  var paramTypes = ['bool', 'string', 'string', 'string', 'bytes32', 'address']

        var params = ['true', 'Simple Agent', 'Complex Agent', 'www.github.com/simple_agent', codeHash, accounts[4]]

        var paramsHash =
          '0x' + sha3(web3.eth.abi.encodeParameters(paramTypes, params)).toString('hex')

        console.log(paramsHash) */

        await permissions.proposePermission('Simple Agent', createAgentParamsHash,
          "executionProof", management.address, "newAgent", "hostingProof", {from: accounts[1]})

        assert.equal(await permissions.getContractWhereUsed('Simple Agent', 0), management.address)

        assert.equal(await permissions.getHashOfParams('Simple Agent', 0), createAgentParamsHash)

        assert.equal(await permissions.getFunctionWhereUsed('Simple Agent', 0), "newAgent")

        assert.equal(await permissions.getHost('Simple Agent', 0), accounts[1])

        assert.equal(true, await permissions.canUsePermission('Simple Agent', 0,
          management.address, 'newAgent', accounts[1], createAgentParamsHash))

        await management.newAgent(true, 'Simple Agent', 0,
          'Complex Agent', 'www.github.com/simple_agent', codeHash, accounts[4], {from: accounts[1]})

    })

    it("Make agent autonomous -- human creator", async () => {

        await management.newAgent(false, "", 0, "Simple Agent", "www.github.com/simple_agent", "codeHash", accounts[4], {from: accounts[0]});

        await management.makeFullyAutonomous("Simple Agent", 0, {from: accounts[0]});

        assert.equal(true, await management.getAgentAutonomy(accounts[0], 0));

    })

    it("Update agent's code -- human creator", async () => {

        await management.newAgent(false, "", 0, "Simple Agent", "www.github.com/simple_agent", "codeHash", accounts[4], {from: accounts[0]});

        await management.updateCode("Simple Agent", 0, "newSource", '0x' + sha3("newHash").toString('hex'), {from: accounts[0]});

        assert.equal(1, await management.getAgentVersion(accounts[0], 0));

        assert.equal("newSource", await management.getAgentSourceLocation(accounts[0], 0));

        assert.equal('0x' + sha3("newHash").toString('hex'), await management.getAgentCodeHash(accounts[0], 0));

    })

});
