pragma solidity 0.4.25;

contract AgentKeys {

  //CONSTANTS

  uint256 standardKeyAmount = 10;

  address agentManagement;
  address treasury;

  mapping(string => address[]) keys;

  modifier onlyAgentManagement() {

    require(msg.sender == agentManagement);
    _;

  }

  modifier onlyTreasury() {

    require(msg.sender == treasury);
    _;

  }

  constructor(address _agentManagement) public {

    agentManagement = _agentManagement;

  }

  function initializeKeyArray(string agent, address[] _initialKeys) public onlyTreasury {

    require(_initialKeys.length == standardKeyAmount, "You did not specify the correct amount of keys");

    for (uint i = 0; i < _initialKeys.length; i++) {

      keys[agent].push(_initialKeys[i]);

    }



  }

  function deleteKeyArray(address creator, string agent, bool autonomous) public onlyAgentManagement {

    //if creator called and agent not autonomous, send all money to creator
    //if agent autonomous, can only delete if no money left in any key


  }

  function replaceKey(string agent, uint256 replacedKey, address newKey) public onlyTreasury {



  }

}
