pragma solidity 0.4.25;

import "contracts/interfaces/IAgentObligations.sol";
import "contracts/interfaces/IAgentManagement.sol";

contract AgentObligations is IAgentObligations {

  struct Obligation {

    address receiver;
    uint256 percentageEarnings;

  }

  mapping(address => Obligation[]) obligations;
  mapping(address => uint256) totalObligation;

  IAgentManagement management;

  modifier onlyManagement() {

    require(msg.sender == address(management));
    _;

  }

  constructor(address _management) public {

    management = IAgentManagement(_management);

  }

  function changeObligation(address receiver, string agentName, uint256 newEarnings) public {

    require(newEarnings > 0);
    require(newEarnings <= 100);

    address obligationTarget = msg.sender;

    if (receiver != address(0)) obligationTarget = receiver;

    address creator = management.getCreatorFromName(agentName);

    uint256 agentPosition = management.getPositionFromName(agentName);

    bool autonomy = management.getAgentAutonomy(creator, agentPosition);

    address agentAddress = management.getAgentAddress(creator, agentPosition);

    uint256 obligationPosition = 0;
    bool foundObligation = false;

    uint256 agentObligations = 0;

    for (uint i = 0; i < obligations[agentAddress].length; i++) {

      if (obligations[agentAddress][i].receiver == obligationTarget) {

        obligationPosition = i;
        foundObligation = true;

      }

      agentObligations = agentObligations + obligations[agentAddress][i].percentageEarnings;

    }

    if (obligationTarget == creator && autonomy) {

      require(foundObligation == false ||
          obligations[agentAddress][obligationPosition].percentageEarnings > newEarnings);

    }

    uint256 difference = 0;

    if (foundObligation == true) {

      if (obligations[agentAddress][obligationPosition].percentageEarnings > newEarnings) {

        difference = obligations[agentAddress][obligationPosition].percentageEarnings - newEarnings;

        totalObligation[agentAddress] = totalObligation[agentAddress] - difference;

      } else if (obligations[agentAddress][obligationPosition].percentageEarnings < newEarnings) {

        difference = newEarnings - obligations[agentAddress][obligationPosition].percentageEarnings;

        require(agentObligations + difference < 100);

        totalObligation[agentAddress] = totalObligation[agentAddress] + difference;

      }

    } else {

      require(agentObligations + newEarnings < 100);

      totalObligation[agentAddress] = totalObligation[agentAddress] + newEarnings;

    }

    applyModification(foundObligation, obligationPosition,
        newEarnings, obligationTarget, agentAddress);

  }

  function deleteAllObligations(address agent) public {

    totalObligation[agent] = 0;

    delete(obligations[agent]);

  }

  function deleteObligation(address agentAddress, uint256 obligationPosition) public {

    require(obligationPosition < getObligationsLength(agentAddress));
    require(obligations[agentAddress][i].receiver == msg.sender);

    totalObligation[agentAddress] = totalObligation[agentAddress] -
      obligations[agentAddress][i].percentageEarnings;

    for (uint i = obligationPosition; i < obligations[agentAddress].length - 1; i++) {

      obligations[agentAddress][i].receiver = obligations[agentAddress][i + 1].receiver;
      obligations[agentAddress][i].percentageEarnings = obligations[agentAddress][i + 1].percentageEarnings;

    }

    obligations[agentAddress].length--;

  }

  //PRIVATE

  function applyModification(bool foundObligation, uint256 obligationPosition,
      uint256 newEarnings, address caller, address agentAddress) private {

    if (foundObligation)
      obligations[agentAddress][obligationPosition].percentageEarnings = newEarnings;

    else {

      Obligation memory newObligation = Obligation(caller, newEarnings);
      obligations[agentAddress].push(newObligation);

    }

  }

  //GETTERS

  function getObligationsLength(address agent) public view returns (uint256) {

      return obligations[agent].length;

  }

  function getTotalObligation(address agent) public view returns (uint256) {

      return totalObligation[agent];

  }

  function getObligationReceiver(address agent, uint256 position) public view returns (address) {

      return obligations[agent][position].receiver;

  }

  function getObligationPayment(address agent, uint256 position) public view returns (uint256) {

      return obligations[agent][position].percentageEarnings;

  }

}
