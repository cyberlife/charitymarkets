pragma solidity 0.4.25;

import "contracts/interfaces/IAgentManagement.sol";
import "contracts/interfaces/IAgentsTreasury.sol";
import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/interfaces/IEphemeralPermissions.sol";

//TODO: let DAOs add obligations for agents to give them funds

contract AgentManagement is IAgentManagement, Ownable {

    //Constants

    string NEW_AGENT = "newAgent";
    string DELETE_AGENT = "deleteAgent";
    string FULLY_AUTONOMOUS = "makeFullyAutonomous";
    string UPDATE_CODE = "updateCode";

    struct AgentData {

        string agent_name;

        uint256 version;

        string open_source_location;

        bytes32 code_hash;

        bool fully_autonomous;

        bool creatorIsAgent;

        address agentAddress;

    }

    struct AgentPointer {

        address creator;
        uint256 position;

        address[] treasuryAddresses;

    }

    uint256 minimumShardSize;

    uint256 minimumExecutionTime;

    address treasury;

    IEphemeralPermissions ephemeral;

    mapping(address => AgentData[]) agents;
    mapping(string => AgentPointer) pointers;

    mapping(string => string) agentControllingAgent;

    modifier onlyCreator(string _name, uint256 position, string func, bytes32 paramsHash) {

        uint256 _position = getPositionFromName(_name);
        address creator = getCreatorFromName(_name);

        bool createdByAgent = agents[creator][_position].creatorIsAgent;

        if (createdByAgent) {

          string memory agentCreator = agentControllingAgent[_name];

          require(ephemeral.canUsePermission(agentCreator,
              position, this, func, msg.sender, paramsHash) == true, "Cannot use the permission");

          ephemeral.usePermission(agentCreator, position);

        } else {

          require(msg.sender == pointers[_name].creator, "The sender is not the creator of the agent");

        }

        _;

    }

    modifier autonomyRights(string _name, uint256 position, string func, bytes32 paramsHash) {

        uint256 _position = pointers[_name].position;
        address creator = pointers[_name].creator;

        if (agents[creator][_position].fully_autonomous) {

          string memory agentName = agents[creator][_position].agent_name;

          require(ephemeral.canUsePermission(agentName,
              position, this, func, msg.sender, paramsHash) == true, "Cannot use the permission for the agent itself");

          ephemeral.usePermission(agentName, position);

        } else {

          bool createdByAgent = agents[creator][_position].creatorIsAgent;

          if (createdByAgent) {

            string memory agentCreator = agentControllingAgent[_name];

            require(ephemeral.canUsePermission(agentCreator,
                position, this, func, msg.sender, paramsHash) == true, "Cannot use the permission for the creator agent itself");

            ephemeral.usePermission(agentCreator, position);

          } else {

            require(msg.sender == pointers[_name].creator, "The caller is not the agent creator");

          }

        }

        _;

    }

    modifier agentExists(uint256 position, address creator) {

        require(bytes(agents[creator][position].agent_name).length > 0, "The name for this position and creator does not exist");
        require(agents[creator][position].version > 0, "The version of the agent is zero");
        require(bytes(agents[creator][position].open_source_location).length > 0, "There is no source specified for the code");

        _;

    }

    constructor(uint256 shardSize) {

        minimumShardSize = shardSize;

    }

    function setEphemeral(address _ephemeral) public onlyOwner {

        require(_ephemeral != address(0), "The address must not be address(0)");

        ephemeral = IEphemeralPermissions(_ephemeral);

    }

    function setTreasury(address _treasury) public onlyOwner {

        require(_treasury != address(0), "The address must not be address(0)");

        treasury = _treasury;

    }

    function changeMinimumShardSize(uint256 newShardSize) public onlyOwner {

        minimumShardSize = newShardSize;

        emit ChangedShardSize(newShardSize);

    }

    function newAgent(bool agentIsCreator,
        string _agent,
        uint256 position,
        string _name,
        string open_source,
        bytes32 code_hash,
        address newBotAddress) public {

        require(keccak256(_name) != keccak256(''), "The name must be different than ''");

        require(keccak256(agentControllingAgent[_name]) == keccak256('') || pointers[_name].creator == address(0), "An agent with this name has already been created");

        require(keccak256(open_source) != keccak256(''), "An open source location for the code is not specified");

        require(code_hash != bytes32(''), "The hash of the code was not specified");

        AgentData memory newAgent;

        if (agentIsCreator == true) {

          bytes32 paramsHash = keccak256(abi.encode(agentIsCreator, _agent,
                                         _name, open_source, code_hash, newBotAddress));

          require(ephemeral.canUsePermission
                 (_agent, position, this, NEW_AGENT, msg.sender, paramsHash) == true, "There is no permission valid for this call");

          ephemeral.usePermission(_agent, position);

          agentControllingAgent[_name] = _agent;

        }

        newAgent = AgentData(_name, 0, open_source,
                code_hash, false, agentIsCreator, newBotAddress);

        agents[msg.sender].push(newAgent);

        pointers[_name].creator = msg.sender;

        pointers[_name].position = agents[msg.sender].length - 1;

        emit CreatedNewAgent(msg.sender, agents[msg.sender].length - 1);

    }

    function deleteAgent(string _name, uint256 position)
      public //autonomyRights(_name, position, DELETE_AGENT, keccak256(abi.encode(_name)))
      {

        address creator = getCreatorFromName(_name);

        uint256 _position = getPositionFromName(_name);

        address agentAddress = getAgentAddress(creator, _position);

        delete(agents[creator][_position]);

        delete(agentControllingAgent[_name]);

        IAgentsTreasury tr = IAgentsTreasury(treasury);

        tr.unregisterAgent(_name);

    }

    function makeFullyAutonomous(string _name, uint256 position)
      public //onlyCreator(_name, position, FULLY_AUTONOMOUS, keccak256(abi.encode(_name)))
      {

        address creator = getCreatorFromName(_name);

        uint256 _position = getPositionFromName(_name);

        require(agents[creator][_position].fully_autonomous == false, "The agent is already autonomous");
        require(agents[creator][_position].agentAddress != address(0), "The agent does not exist");

        agents[creator][_position].fully_autonomous = true;

        emit MadeAutonomous(creator, _position);

    }

    function updateCode(string _name, uint256 position, string codeSource, bytes32 codeHash)
      public //autonomyRights(_name, position, UPDATE_CODE, keccak256(abi.encode(_name, codeSource, codeHash)))
      {

        uint256 _position = getPositionFromName(_name);
        address creator = getCreatorFromName(_name);

        require(keccak256(codeSource) != keccak256(agents[creator][_position].open_source_location)
                && codeHash != agents[creator][_position].code_hash, "The code source and code hash specified are identical to the previous ones");

        if (codeHash != agents[creator][_position].code_hash) {

            agents[creator][position].version = agents[creator][_position].version + 1;

        }

        agents[creator][_position].open_source_location = codeSource;
        agents[creator][_position].code_hash = codeHash;

        emit UpdatedCode(msg.sender, creator, _position, codeSource, codeHash);

    }

    //GETTERS

    function getMinimumShardSize() public view returns (uint256) {

        return minimumShardSize;

    }

    function getAgentControllingAgent(string _controlledAgent) public view returns (string) {

        return agentControllingAgent[_controlledAgent];

    }

    function getCreatorIsAgent(address creator, uint256 position) public view returns (bool) {

        return agents[creator][position].creatorIsAgent;

    }

    function getCreatorFromName(string name) public view returns (address) {

        return pointers[name].creator;

    }

    function getPositionFromName(string name) public view returns (uint256) {

        return pointers[name].position;

    }

    function getAgentName(address creator, uint256 position) public view returns (string) {

        return agents[creator][position].agent_name;

    }

    function getAgentVersion(address creator, uint256 position) public view returns (uint256) {

        return agents[creator][position].version;

    }

    function getAgentSourceLocation(address creator, uint256 position) public view returns (string) {

        return agents[creator][position].open_source_location;

    }

    function getAgentCodeHash(address creator, uint256 position) public view returns (bytes32) {

        return agents[creator][position].code_hash;

    }

    function getAgentAutonomy(address creator, uint256 position) public view returns (bool) {

        return agents[creator][position].fully_autonomous;

    }

    function getAgentAddress(address creator, uint256 position) public view returns (address) {

        return agents[creator][position].agentAddress;

    }

    function getCurrentController(address creator, uint256 position) public view returns (address) {

        if (agents[creator][position].fully_autonomous) {

          return agents[creator][position].agentAddress;

        }

        return creator;

    }

    function isNameTaken(string _name) public view returns (bool) {

        return (keccak256(agentControllingAgent[_name]) != keccak256('') || pointers[_name].creator != address(0));

    }

}
