pragma solidity 0.4.25;

contract IEphemeralPermissions {

  event ToggledContract(address con, bool status);

  event ChangedAvailability(uint256 availability);

  event ChangedHostManagement(address host);

  event ProposedPermission(string agent, bytes32 hashOfParams, bytes trustedExecutionProof);

  event UsedPermission(string agent, uint256 position);


  function toggleAllowedContract(address con) public;

  function changeAvailability(uint256 _availability) public;

  function changeHostManagement(address _host) public;

  function proposePermission(string agent, bytes32 hashOfParams, bytes trustedExecutionProof,
                             address _forContract, string _func, bytes hostingProof) public;

  function usePermission(string agent, uint256 position) public;


  //PRIVATE

  function isContract(address _addr) private returns (bool);


  function canUsePermission(string agent, uint256 position, address con, string func, address caller, bytes32 paramsHash)
    public view returns (bool);

  function getContractWhereUsed(string agent, uint256 position) public view returns (address);

  function getLatestNonce(string agent) public view returns (uint256);

  function getHashOfParams(string agent, uint256 position) public view returns (bytes32);

  function getTrustedExecutionProof(string agent, uint256 position) public view returns (bytes);

  function getFunctionWhereUsed(string agent, uint256 position) public view returns (string);

  function getCreationTime(string agent, uint256 position) public view returns (uint256);

  function getUsed(string agent, uint256 position) public view returns (bool);

  function getHost(string agent, uint256 position) public view returns (address);

  function getAvailability() public view returns (uint256);

  function getContractAllowance(address con) public view returns (bool);

}
