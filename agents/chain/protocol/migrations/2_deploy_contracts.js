var WorkToken = artifacts.require('contracts/token/WorkToken.sol');
var Staking = artifacts.require('contracts/security/Staking.sol');
var HostManagement = artifacts.require('contracts/hosts/HostManagement.sol');
var AgentObligations = artifacts.require('contracts/agents/AgentObligations.sol');
var AgentManagement = artifacts.require('contracts/agents/AgentManagement.sol');
var AgentsTreasury = artifacts.require('contracts/agents/AgentsTreasury.sol');
var DeploymentRequests = artifacts.require('contracts/agents/DeploymentRequests.sol');
var ShardModifications = artifacts.require('contracts/agents/ShardModifications.sol');
var AgentVersioning = artifacts.require('contracts/hosts/AgentVersioning.sol');
var AssigningJobs = artifacts.require('contracts/hosts/AssigningJobs.sol');
var EphemeralPermissions = artifacts.require('contracts/agents/EphemeralPermissions.sol');

var fs = require('fs')

module.exports = async function(deployer, network, accounts) {

    let devAddress = '0x1900a41f2777ab70aad2074e3f4b9c5429c7f243'

    let STAKE = 200000000000000000000
    let POOL_WITHDRAWAL_TIME = 7
    let toMint = 1000000 * 10**18
    let toApprove = 1000 * 10**18
    let rent1 = 11
    let rent2 = 10
    let rent3 = 9
    let shardsBatchSize = 2
    let shardVoteThreshold = 1
    let paymentWindow = 60000
    let votingWindow = 60
    let timeBetweenPriceChanges = 64000
    let shutdownTime = 60
    let keyActivity = 30
    let minimumExecutionTime = 30

    const randomAddr = accounts[4]

    let backup_location = '/home/stefan/Work/cyberlife-js/agents/smartAgents/nodes/'

    if (network == "main") {


    } else if (network == "rinkeby" || network == "ropsten") {

        deployer.deploy(WorkToken, "WORK", "WOR", 18).then(function() {
          return deployer.deploy(Staking, WorkToken.address, STAKE).then(function() {
              return deployer.deploy(HostManagement, Staking.address, Staking.address).then(function() {
                  return deployer.deploy(AgentManagement, 10).then(function() {
                    return deployer.deploy(AgentObligations, AgentManagement.address, {from: accounts[0]}).then(function() {
                      return deployer.deploy(AgentsTreasury, paymentWindow).then(function() {
                          return deployer.deploy(ShardModifications, shardsBatchSize, shardVoteThreshold, votingWindow).then(function() {
                              return deployer.deploy(DeploymentRequests).then(function() {
                                  return deployer.deploy(AgentVersioning, AgentManagement.address, Staking.address).then(function() {
                                      return deployer.deploy(AssigningJobs, HostManagement.address, AgentManagement.address, AgentsTreasury.address).then(function() {
                                          return deployer.deploy(EphemeralPermissions, keyActivity, HostManagement.address).then(function() {

                })  })  })  }) }) }) }) }) }) }) })

    } else if (network == "development") {

        deployer.deploy(WorkToken, "WORK", "WOR", 18, {from: accounts[0]}).then(function() {
          return deployer.deploy(Staking, WorkToken.address, STAKE, {from: accounts[0]}).then(function() {
            return deployer.deploy(AgentManagement, 1, {from: accounts[0]}).then(function() {
              return deployer.deploy(AgentObligations, AgentManagement.address, {from: accounts[0]}).then(function() {
                return deployer.deploy(AgentsTreasury, paymentWindow, {from: accounts[0]}).then(function() {
                  return deployer.deploy(HostManagement, Staking.address, AgentsTreasury.address, {from: accounts[0]}).then(function() {
                    return deployer.deploy(ShardModifications, shardsBatchSize, shardVoteThreshold, votingWindow, {from: accounts[0]}).then(function() {
                      return deployer.deploy(DeploymentRequests, {from: accounts[0]}).then(function() {
                        return deployer.deploy(AgentVersioning, AgentManagement.address, Staking.address, {from: accounts[0]}).then(function() {
                          return deployer.deploy(AssigningJobs, HostManagement.address, AgentManagement.address, AgentsTreasury.address, {from: accounts[0]}).then(function() {
                            return deployer.deploy(EphemeralPermissions, keyActivity, HostManagement.address, {from: accounts[0]}).then(async function() {

                              var addresses = WorkToken.address
                              + "\n" + Staking.address
                              + "\n" + HostManagement.address
                              + "\n" + AgentManagement.address
                              + "\n" + AgentVersioning.address
                              + "\n" + AgentsTreasury.address
                              + "\n" + ShardModifications.address
                              + "\n" + DeploymentRequests.address
                              + "\n" + AssigningJobs.address
                              + "\n" + EphemeralPermissions.address
                              + "\n" + AgentObligations.address

                              backupAddresses(backup_location, 'localAddresses.txt', addresses.toString())

                              //Mint work coins

                              await WorkToken.at(WorkToken.address)
                                  .mint(accounts[0], toMint, {from: accounts[0]});

                              await WorkToken.at(WorkToken.address)
                                  .mint(accounts[1], toMint, {from: accounts[0]});

                              await WorkToken.at(WorkToken.address)
                                  .mint(accounts[2], toMint, {from: accounts[0]});

                              await WorkToken.at(WorkToken.address)
                                  .mint(accounts[3], toMint, {from: accounts[0]});

                              //Approve work coins spending

                              await WorkToken.at(WorkToken.address)
                                  .approve(Staking.address, toApprove, {from: accounts[1]});

                              await WorkToken.at(WorkToken.address)
                                  .approve(Staking.address, toApprove, {from: accounts[2]});

                              await WorkToken.at(WorkToken.address)
                                  .approve(Staking.address, toApprove, {from: accounts[3]});

                              //Hosts stake

                              await Staking.at(Staking.address)
                                  .stake({from: accounts[1]});

                              await Staking.at(Staking.address)
                                  .stake({from: accounts[2]});

                              await Staking.at(Staking.address)
                                  .stake({from: accounts[3]});

                              //Set treasury in HostManagement

                              await HostManagement.at(HostManagement.address)
                                  .setTreasury(AgentsTreasury.address, {from: accounts[0]});

                              //Set lower rent bound

                              await HostManagement.at(HostManagement.address)
                                  .setLowerBound(rent3, {from: accounts[0]});

                              //Set time between changes and shutdown time in HostManagement

                              await HostManagement.at(HostManagement.address)
                                  .changeTimeBetweenChanges(timeBetweenPriceChanges, {from: accounts[0]});

                              await HostManagement.at(HostManagement.address)
                                  .changeMinimumShutdownTime(shutdownTime, {from: accounts[0]});

                              //Set rent

                              await HostManagement.at(HostManagement.address)
                                  .changeRent(rent1, {from: accounts[1]});

                              await HostManagement.at(HostManagement.address)
                                  .changeRent(rent2, {from: accounts[2]});

                              await HostManagement.at(HostManagement.address)
                                  .changeRent(rent3, {from: accounts[3]});

                              //Set ID

                              await HostManagement.at(HostManagement.address)
                                  .setID("QmcThUufvADsvfUia6pgXcpC5xFyiWJQLXywJHw7SjKV2k", {from: accounts[1]});

                              await HostManagement.at(HostManagement.address)
                                  .setID("QmdKq27XbDW6LHPUbAx2zWQDXxdBa2Bb4txtopcaoDPLdj", {from: accounts[2]});

                              await HostManagement.at(HostManagement.address)
                                  .setID("QmVi64cSbLsAJZX1DGYe6ShaHSKWL3XVaVC9M7YfvZHH98", {from: accounts[3]});

                              //Broadcast activity from hosts

                          /*    await HostManagement.at(HostManagement.address)
                                  .broadcastActivity({from: accounts[1]});

                              await HostManagement.at(HostManagement.address)
                                  .broadcastActivity({from: accounts[2]});

                              await HostManagement.at(HostManagement.address)
                                  .broadcastActivity({from: accounts[3]}); */

                              //DeploymentRequests setup

                              await DeploymentRequests.at(DeploymentRequests.address)
                                  .setTreasuryDeployment(AgentsTreasury.address, {from: accounts[0]});

                              await DeploymentRequests.at(DeploymentRequests.address)
                                  .setShardModifications(ShardModifications.address, {from: accounts[0]});

                              await DeploymentRequests.at(DeploymentRequests.address)
                                  .changeAgentManagement(AgentManagement.address, {from: accounts[0]});

                              await DeploymentRequests.at(DeploymentRequests.address)
                                  .changeStakeAddress(Staking.address, {from: accounts[0]});


                              //ShardModifications setup

                              await ShardModifications.at(ShardModifications.address)
                                  .setTreasury(AgentsTreasury.address, {from: accounts[0]});

                              await ShardModifications.at(ShardModifications.address)
                                  .changeDeploymentRequestAddr(DeploymentRequests.address, {from: accounts[0]});

                              await ShardModifications.at(ShardModifications.address)
                                  .changeAgentManagement(AgentManagement.address, {from: accounts[0]});

                              //AgentsTreasury setup

                              await AgentsTreasury.at(AgentsTreasury.address)
                                  .setShardModifications(ShardModifications.address, {from: accounts[0]});

                              await AgentsTreasury.at(AgentsTreasury.address)
                                  .changeDeploymentRequestAddr(DeploymentRequests.address, {from: accounts[0]});

                              await AgentsTreasury.at(AgentsTreasury.address)
                                  .changeAgentManagement(AgentManagement.address, {from: accounts[0]});

                              await AgentsTreasury.at(AgentsTreasury.address)
                                  .setHostManagement(HostManagement.address, {from: accounts[0]});

                              //AgentManagement setup

                              await AgentManagement.at(AgentManagement.address)
                                  .setTreasury(AgentsTreasury.address, {from: accounts[0]});

                              await AgentManagement.at(AgentManagement.address)
                                  .setEphemeral(EphemeralPermissions.address, {from: accounts[0]});

                            })  })  })  })  })  })  }) }) }) }) })

  }

}

function backupAddresses(path, name, content) {

    var buffered = new Buffer(content);

    fs.open(path + name, 'w', function(err, fd) {
      if (err) {
          throw 'could not open file: ' + err;
      }

    fs.write(fd, buffered, 0, buffered, null, function(err) {
        if (err) throw 'error writing file: ' + err;
        fs.close(fd, function() {});
    });

    });
}
