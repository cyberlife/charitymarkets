var ArtBuying = artifacts.require('contracts/ArtBuying.sol');

var AgentManagement = artifacts.require('contracts/base/cyber/agents/AgentManagement.sol');
var AgentObligations = artifacts.require('contracts/base/cyber/agents/AgentObligations.sol');
var AgentsTreasury = artifacts.require('contracts/base/cyber/agents/AgentsTreasury.sol');
var WorkToken = artifacts.require('contracts/base/cyber/token/WorkToken.sol');
var Staking = artifacts.require('contracts/base/cyber/security/Staking.sol');
var HostManagement = artifacts.require('contracts/base/cyber/hosts/HostManagement.sol');
var DeploymentRequests = artifacts.require('contracts/base/cyber/agents/DeploymentRequests.sol');
var ShardModifications = artifacts.require('contracts/base/cyber/agents/ShardModifications.sol');
var AgentVersioning = artifacts.require('contracts/base/cyber/hosts/AgentVersioning.sol');
var AssigningJobs = artifacts.require('contracts/base/cyber/hosts/AssigningJobs.sol');
var EphemeralPermissions = artifacts.require('contracts/base/cyber/agents/EphemeralPermissions.sol');

var CharityMarkets = artifacts.require('contracts/base/charity/CharityMarkets.sol');
var Token = artifacts.require("contracts/base/charity/token/Token.sol");
var BondingCurveVault = artifacts.require("contracts/base/charity/BondingCurveVault.sol");
var FractionalExponents = artifacts.require("contracts/base/charity/FractionalExponents.sol");

module.exports = async function(deployer, network, accounts) {

  //Agent

  var agentCashback = 5;
  var marketShare = 40;
  var agentName = "Artonomous";

  //Charity

  var toSend = 5 * 10**18;

  //Cyberlife

  let STAKE = 200000000000000000000
  let POOL_WITHDRAWAL_TIME = 7
  let toMint = 1000000 * 10**18
  let toApprove = 1000 * 10**18
  let rent1 = 11
  let rent2 = 10
  let rent3 = 9
  let shardsBatchSize = 2
  let shardVoteThreshold = 1
  let paymentWindow = 60000
  let votingWindow = 60
  let timeBetweenPriceChanges = 64000
  let shutdownTime = 60
  let keyActivity = 30
  let minimumExecutionTime = 30

  const randomAddr = accounts[4]

  if (network == "main") {


  } else if (network == "rinkeby" || network == "ropsten") {

    deployer.deploy(ArtBuying, agentCashback, marketShare, {from: accounts[0]}).then(function() {

      var management = AgentManagement.deployed();

      var obligations = AgentObligations.deployed();

      var charity = CharityMarkets.deployed();

      var treasury = AgentsTreasury.deployed();

    })

  } else if (network == "development") {

    deployer.deploy(ArtBuying, agentCashback, marketShare, {from: accounts[0]}).then(async function() {

      var management = await AgentManagement.deployed();

      var obligations = await AgentObligations.deployed();

      var charity = await CharityMarkets.deployed();

      var treasury = await AgentsTreasury.deployed();

      await ArtBuying.at(ArtBuying.address)
        .setAgentManagement(agentName, management.address, {from: accounts[0]});

      await ArtBuying.at(ArtBuying.address)
        .setTreasury(treasury.address, {from: accounts[0]});

      await ArtBuying.at(ArtBuying.address)
        .setObligations(obligations.address, {from: accounts[0]});

      await ArtBuying.at(ArtBuying.address)
        .setCharityMarkets(charity.address, {from: accounts[0]});

    })

  } else if (network == "test") {

    deployer.deploy(ArtBuying, agentCashback, marketShare, {from: accounts[0]}).then(function() {

      return deployer.deploy(CharityMarkets, {from: accounts[0]}).then(function() {
        return deployer.deploy(Token, CharityMarkets.address, {from: accounts[0]}).then(function() {
          return deployer.deploy(BondingCurveVault, CharityMarkets.address, {from: accounts[0]}).then(function() {
            return deployer.deploy(FractionalExponents, {from: accounts[0]}).then(async function() {

              return deployer.deploy(WorkToken, "WORK", "WOR", 18).then(function() {
                return deployer.deploy(Staking, WorkToken.address, STAKE).then(function() {
                  return deployer.deploy(HostManagement, Staking.address, Staking.address).then(function() {
                    return deployer.deploy(AgentManagement, 1).then(function() {
                      return deployer.deploy(AgentsTreasury, paymentWindow).then(function() {
                        return deployer.deploy(ShardModifications, shardsBatchSize, shardVoteThreshold, votingWindow).then(function() {
                          return deployer.deploy(DeploymentRequests).then(function() {
                            return deployer.deploy(AgentVersioning, AgentManagement.address, Staking.address).then(function() {
                              return deployer.deploy(AssigningJobs, HostManagement.address, AgentManagement.address, AgentsTreasury.address).then(function() {
                                return deployer.deploy(EphemeralPermissions, keyActivity, HostManagement.address).then(async function() {
                                  return deployer.deploy(AgentObligations, AgentManagement.address, {from: accounts[0]}).then(async function() {

      // CharityMarkets

      let charityInstance = await CharityMarkets.deployed()

      await charityInstance.setCharityAddress("allCharities", accounts[5], Token.address, BondingCurveVault.address)
      await charityInstance.setExponentContract(FractionalExponents.address)

      await web3.eth.sendTransaction({to: BondingCurveVault.address, from: accounts[1], value: toSend})

      //Cyberlife

      await WorkToken.at(WorkToken.address)
        .mint(accounts[0], toMint, {from: accounts[0]});

      await WorkToken.at(WorkToken.address)
        .mint(accounts[1], toMint, {from: accounts[0]});

      await WorkToken.at(WorkToken.address)
        .mint(accounts[2], toMint, {from: accounts[0]});

      await WorkToken.at(WorkToken.address)
        .mint(accounts[3], toMint, {from: accounts[0]});

      //Approve work coins spending

      await WorkToken.at(WorkToken.address)
        .approve(Staking.address, toApprove, {from: accounts[1]});

      await WorkToken.at(WorkToken.address)
        .approve(Staking.address, toApprove, {from: accounts[2]});

      await WorkToken.at(WorkToken.address)
        .approve(Staking.address, toApprove, {from: accounts[3]});

      //Hosts stake

      await Staking.at(Staking.address)
        .stake({from: accounts[1]});

      await Staking.at(Staking.address)
        .stake({from: accounts[2]});

      await Staking.at(Staking.address)
        .stake({from: accounts[3]});


      await HostManagement.at(HostManagement.address)
        .setTreasury(AgentsTreasury.address, {from: accounts[0]});


      await HostManagement.at(HostManagement.address)
        .setLowerBound(rent3, {from: accounts[0]});


      await HostManagement.at(HostManagement.address)
        .changeTimeBetweenChanges(timeBetweenPriceChanges, {from: accounts[0]});

      await HostManagement.at(HostManagement.address)
        .changeMinimumShutdownTime(shutdownTime, {from: accounts[0]});


      await HostManagement.at(HostManagement.address)
        .changeRent(rent1, {from: accounts[1]});

      await HostManagement.at(HostManagement.address)
        .changeRent(rent2, {from: accounts[2]});

      await HostManagement.at(HostManagement.address)
        .changeRent(rent3, {from: accounts[3]});


      await HostManagement.at(HostManagement.address)
        .setID("QmcThUufvADsvfUia6pgXcpC5xFyiWJQLXywJHw7SjKV2k", {from: accounts[1]});

      await HostManagement.at(HostManagement.address)
        .setID("QmdKq27XbDW6LHPUbAx2zWQDXxdBa2Bb4txtopcaoDPLdj", {from: accounts[2]});

      await HostManagement.at(HostManagement.address)
        .setID("QmVi64cSbLsAJZX1DGYe6ShaHSKWL3XVaVC9M7YfvZHH98", {from: accounts[3]});


      await HostManagement.at(HostManagement.address)
        .broadcastActivity({from: accounts[1]});

      await HostManagement.at(HostManagement.address)
        .broadcastActivity({from: accounts[2]});

      await HostManagement.at(HostManagement.address)
        .broadcastActivity({from: accounts[3]});

      //DeploymentRequests setup

      await DeploymentRequests.at(DeploymentRequests.address)
        .setTreasuryDeployment(AgentsTreasury.address, {from: accounts[0]});

      await DeploymentRequests.at(DeploymentRequests.address)
        .setShardModifications(ShardModifications.address, {from: accounts[0]});

      await DeploymentRequests.at(DeploymentRequests.address)
        .changeAgentManagement(AgentManagement.address, {from: accounts[0]});

      await DeploymentRequests.at(DeploymentRequests.address)
        .changeStakeAddress(Staking.address, {from: accounts[0]});


      await ShardModifications.at(ShardModifications.address)
        .setTreasury(AgentsTreasury.address, {from: accounts[0]});

      await ShardModifications.at(ShardModifications.address)
        .changeDeploymentRequestAddr(DeploymentRequests.address, {from: accounts[0]});

      await ShardModifications.at(ShardModifications.address)
        .changeAgentManagement(AgentManagement.address, {from: accounts[0]});


      await AgentsTreasury.at(AgentsTreasury.address)
        .setShardModifications(ShardModifications.address, {from: accounts[0]});

      await AgentsTreasury.at(AgentsTreasury.address)
        .changeDeploymentRequestAddr(DeploymentRequests.address, {from: accounts[0]});

      await AgentsTreasury.at(AgentsTreasury.address)
        .changeAgentManagement(AgentManagement.address, {from: accounts[0]});

      await AgentsTreasury.at(AgentsTreasury.address)
        .setHostManagement(HostManagement.address, {from: accounts[0]});

      await AgentManagement.at(AgentManagement.address)
        .setTreasury(AgentsTreasury.address, {from: accounts[0]});

      await AgentManagement.at(AgentManagement.address)
        .setEphemeral(EphemeralPermissions.address, {from: accounts[0]});

      //Register Artonomous

      await AgentManagement.at(AgentManagement.address)
        .newAgent(false, "", 0, agentName, "source",
        '0x' + new Buffer("hash").toString('hex'), accounts[4], {from: accounts[0]})

      await DeploymentRequests.at(DeploymentRequests.address)
        .deployAgent(agentName,
        '0x' + new Buffer("root").toString('hex'),
        "IPFS",
        '0x' + new Buffer("root").toString('hex'),
        "proof",
        1,
        5000, {from: accounts[0]})

      await DeploymentRequests.at(DeploymentRequests.address)
        .respond(agentName,
        0,
        '0x' + new Buffer("proof").toString('hex'),
        {from: accounts[1]})

      await AgentsTreasury.at(AgentsTreasury.address)
        .registerAgent(agentName,
        '0x' + new Buffer("root").toString('hex'),
        "IPFS",
        1,
        {from: accounts[0], value: 5000})

      await AgentObligations.at(AgentObligations.address)
        .changeObligation(accounts[5], agentName, 10, {from: accounts[0]})

      //Artonomous setup

      var management = await AgentManagement.deployed();

      var obligations = await AgentObligations.deployed();

      var charity = await CharityMarkets.deployed();

      var treasury = await AgentsTreasury.deployed();

      await ArtBuying.at(ArtBuying.address)
        .setAgentManagement(agentName, management.address, {from: accounts[0]});

      await ArtBuying.at(ArtBuying.address)
        .setTreasury(treasury.address, {from: accounts[0]});

      await ArtBuying.at(ArtBuying.address)
        .setObligations(obligations.address, {from: accounts[0]});

      await ArtBuying.at(ArtBuying.address)
        .setCharityMarkets(charity.address, {from: accounts[0]});

    }) }) }) }) }) }) }) }) }) }) }) }) }) }) }) })

  }

}
