pragma solidity 0.4.25;

import "contracts/base/charity/Logic.sol";
import "contracts/base/charity/interfaces/ICharityMarkets.sol";

/**
 * @title Proof of Concept contract for Donation customization of Bonding Curve Logic
 */

contract CharityMarkets is ICharityMarkets, Logic {

    mapping(address => bool) availableCharities;

    string charitiesListIPFS;

    constructor() Logic() public {}

    /**
    * @dev The sender can split the money between a donation and cashback
    */
    function donate(address targetCharity, uint256 charityPercentage,
                    address[] targets, uint256[] paymentShares)
                    public payable {

        require(availableCharities[targetCharity] == true, "This charity is not available");
        require(charityPercentage <= 100 && charityPercentage >= 1, "The charity percentage is not between 1 and 100 percent");
        require(targets.length == paymentShares.length, "The targets and payments arrays do not have the same length");
        require(targetCharity != address(0), "Charity address is not set correctly");
        require(msg.value > 0, "Must include some ETH to donate");

        uint256 totalShares = charityPercentage;
        uint256 cashbackShare = 0;
        uint i;

        for (i = 0; i < paymentShares.length; i++) {

          totalShares = totalShares.add(paymentShares[i]);
          cashbackShare = cashbackShare.add(paymentShares[i]);

        }

        require(totalShares == 100, "Shares do not add up to 100%");

        // Make ETH distributions
        uint256 multiplier = 100;
        uint256 charityAllocation = (msg.value).mul(charityPercentage);
        uint256 bondingAllocation = (msg.value.mul(multiplier)).sub(charityAllocation).div(multiplier);

        sendToCharity(targetCharity, charityAllocation.div(multiplier));

        if (cashbackShare > 0) {

          this.sponsor.value(bondingAllocation)(targetCharity);

          // Mint the tokens - 10:1 ratio
          //TODO: probably it should be made dynamic

          for (i = 0; i < paymentShares.length; i++) {

            super.award(targetCharity, targets[i],
              ( msg.value.mul(paymentShares[i]).div(multiplier) ).mul(multiplier));

          }

        }

        emit Donated(targetCharity, charityPercentage);

    }

    function sendToCharity(address charity, uint256 _amount) internal {

        //TODO: convert ETH to DAI

        require(availableCharities[charity] == true, "This charity is not available");

        charity.transfer(_amount);

        emit SendToCharity(charity, _amount);

    }

    /**
    * @dev Toggle the status of a '_charityAddress'
    */
    function setCharityAddress(string ipfsCharities, address _charityAddress,
                               address token, address vault) public onlyOwner {

        require(_charityAddress != address(0), "The charity address must not be 0x0");

        if (availableCharities[_charityAddress] == true) {

          uint256 tokenSupply = super.getTokenSupply(_charityAddress);

          require(tokenSupply == 0, "If you want to delete the charity, the token linked to it must have supply 0");

          super.deleteCharityData(_charityAddress);

        } else {

          availableCharities[_charityAddress] = !availableCharities[_charityAddress];

          super.setTokenAndBondingVault(_charityAddress, token, vault);

        }

        charitiesListIPFS = ipfsCharities;

        emit ToggleCharity(ipfsCharities, _charityAddress,
                           token, vault);

    }

}
