pragma solidity 0.4.25;

import "contracts/zeppelin/ERC20/MintableToken.sol";

contract Token is MintableToken {

    string public name = "CharityToken";
    string public symbol = "CHARITY";
    uint8 public decimals = 18;

    // Where business logic resides
    address public logicContract;

    event LogLogicContractChanged
    (
        address byWhom,
        address oldContract,
        address newContract
    );

    event LogBurn
    (
        address byWhom,
        uint256 amount
    );

    event LogMint
    (
        address forWhom,
        uint256 amount
    );

    // Enforce logicContract only calls
    modifier onlyLogicContract() {
        require(msg.sender == logicContract, "Only logicContract can call this");
        _;
    }

    constructor(address _logicContract) public {
        setLogicContract(_logicContract);
    }

    function mintToken(address _who, uint256 _amount) public onlyLogicContract returns (bool) {

        require(_who != address(0), "Invalid address");
        require(_amount > 0, "Invalid amount to mint");

        mint(_who, _amount);

        return true;

    }

    function burn(address _who, uint256 _value) public onlyLogicContract {

        require(_value <= super.balanceOf(_who), "Burn amount needs to be <= to balance");

        totalSupply_ = totalSupply_.sub(_value);
        balances[_who] = balances[_who].sub(_value);

        emit LogBurn(_who, _value);

    }

    /**
    * @dev Set the 'logicContract' to a different contract address
    */
    function setLogicContract(address _logicContract) public onlyOwner {
        address oldContract = logicContract;
        logicContract = _logicContract;
        emit LogLogicContractChanged(msg.sender, oldContract, _logicContract);
    }

}
