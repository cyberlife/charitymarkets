pragma solidity 0.4.25;

contract IAssigningJobs {

  function setHostManagement(address _management) public;

  function setAgentManagement(address _management) public;

  function setTreasury(address _treasury) public;

  function changingAgentTree(string agent) public;

  function finishedChangingTree(string agent) public;


  function getChangingTree(string agent) public view returns (bool);

  function getIsChangingTree(address host) public view returns (bool);

  function getHostChangingTree(string agent) public view returns (address);

}
