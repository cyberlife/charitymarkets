pragma solidity 0.4.25;

contract IShardModifications {

    event ChangedVotingWindow(uint256 window);

    event ChangedBatchSize(uint256 batch);

    event ChangedAgentManagement(address management);

    event ChangedDeploymentRequests(address deployments);

    event ChangedVotingThreshold(uint256 threshold);

    event ProposedModification(string name, string ipfs);

    event VotedModification(string name, bool vote);


    function changeVotingWindow(uint256 _window) public;

    function changeBatchSize(uint256 _batch) public;

    function changeVoteThreshold(uint256 _threshold) public;

    function proposeModification(string _name, address[] hosts, uint256[] timeToHost,
        uint256[] currentPrices, bytes root, string ipfs, uint256[] packagedUint) public;

    function applyModification(string _name) public payable;

    function voteModification(string _name, bool yesno) public;

    function getVoteThreshold() public view returns (uint256);

    function getBatchSize() public view returns (uint256);

    function getModificationTree(string name) public view returns (bytes, string);

    function getModificationArrays(string name) public view returns (address[], uint256[], uint256[]);

    function getVotingWindow() public view returns (uint256);

    function getModificationIntData(string _name) public view returns (uint256, uint256, uint256, uint256, uint256, uint256);

    function agentVoted(address agent, uint256 _nonce) public view returns (bool);

    function uintToStr(uint i) public pure returns (string);

}
