pragma solidity 0.4.25;

contract IDeploymentRequests {

    event ChangedStakingAddress(address staking);

    event ChangedMerkleUtils(address utils);

    event DeployedAgent(string name, string ipfs, string billing, uint256 responsesNr);

    event Responded(uint256 totalResponses, bool readyToApply);

    event ChangedAgentManagement(address management);

    event WithdrewAgent(string _name, bytes _root, string _ipfs, uint256 totalHostsNr);


    function setTreasuryDeployment(address _treasury) public;

    function setShardModifications(address _shardModifications) public;

    function changeStakeAddress(address _staking) public;

    function changeAgentManagement(address _newManagement) public;

    function deployAgent(string _name, bytes _hostsRoot, string _ipfs,
        bytes billingRoot, string billingProof, uint256 responsesNumber, uint256 moneyToDeposit) public;

    function withdrawAgent(string _name, bytes _root, string _ipfs, uint256 totalHostsNr) public;

    function respond(string _name, uint256 position, bytes _proof) public;

    function hasEnoughResponses(string _name, uint256 position) public view returns (bool);

    function getDeploymentsLength(string _name) public view returns (uint256);

    function getDeploymentIPFSProof(string _name, uint256 position) public view returns (string);

    function getDeploymentHostsRoot(string _name, uint256 position) public view returns (bytes);

    function getDeploymentBillingRoot(string _name, uint256 position) public view returns (bytes);

    function getDeploymentBillingProof(string _name, uint256 position) public view returns (string);

    function getWithdrawalsLength(string _name) public view returns (uint256);

    function getWithdrawalRoot(string _name, uint256 position) public view returns (bytes);

    function getWithdrawalIPFSProof(string _name, uint256 position) public view returns (string);

    function getDeploymentIntData(string _name, uint256 position) public view returns (uint256[]);

    function getDeploymentHostsTree(string _name, uint256 position) public view returns (bytes, string);

    function getWithdrawalData(string _name, uint256 position) public view returns (bytes, string, uint256);

}
