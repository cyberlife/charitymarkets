pragma solidity 0.4.25;

contract IAgentVersioning {

  event ChangedAgentVersion(address host, string agent);

  event ClearedAgent(address host, string agent);

  function setManagement(address _management) public;

  function setStaking(address _staking) public;

  function updateAgentVersion(string _agent) public;

  function clearAgent(string _agent) public;

  function getHostAgentVersion(address host, string agent) public view returns (uint256);

}
