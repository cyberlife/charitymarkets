pragma solidity 0.4.25;

contract IArtBuying {

  event SetAgentManagement(string _agentName, address _management);
  event SetTreasury(address _treasury);
  event SetObligations(address _obligations);
  event SetCharityMarkets(address _charityMarkets);
  event ChangedAgentCashback(uint256 cashback);
  event ChangedMarketCut(uint256 cut);
  event BoughtPainting(address buyer, uint256 price, string ipfsLocation, address charity);

  function setAgentManagement(string _agentName, address _management) public;

  function setTreasury(address _treasury) public;

  function setObligations(address _obligations) public;

  function setCharityMarkets(address _charityMarkets) public;

  function changeAgentCashback(uint256 cashback) public;

  function changeMarketCut(uint256 cut) public;

  function showPainting(address potentialBuyer, string ipfsLocation, uint256 price)
    public;

  function buyPainting(address charity) public payable;

  function processLatestObligation() public;

  function processLatestOrder() public;


  //PRIVATE

  function isContract(address _addr) private returns (bool isContract);

  //GETTERS

  function getObligation(uint256 position) public view returns (uint256);

  function getOrderCharityAddress(uint256 position) public view returns (address);

  function getOrderCharityCut(uint256 position) public view returns (uint256);

  function getOrderMoney(uint256 position) public view returns (uint256);

  function getAgentName() public view returns (string);

  function getMarketCut() public view returns (uint256);

  function getAgentCreator() public view returns (address);

  function getAgentPositionInCreatorArray() public view returns (uint256);

  function getAgentCashback() public view returns (uint256);

  function getCurrentOrder() public view returns (uint256);

  function getCurrentObligation() public view returns (uint256);

  function getLatestPainting(address who) public view returns (string, uint256);

  function paintingIsBought(string painting) public view returns (bool);

  function getOwnedPainting(address who, uint256 position) public view returns (string, uint256);

  function getOwnedPaintingsNumber(address who) public view returns (uint256);

}
