var ArtBuying = artifacts.require('contracts/ArtBuying.sol');
var Token = artifacts.require("contracts/base/charity/token/Token.sol");

contract('ArtBuying', function(accounts) {

    let art, charityToken;

    beforeEach(async () => {

      art = await ArtBuying.deployed()

      charityToken = await Token.deployed()

    })

    it("should change agent cashback", async () => {

      assert.equal(await art.getAgentCashback(), 5);

      await art.changeAgentCashback(10, {from: accounts[0]});

      assert.equal(await art.getAgentCashback(), 10);

    })

    it("should change the market cut", async () => {

      assert.equal(await art.getMarketCut(), 40);

      await art.changeMarketCut(50, {from: accounts[0]});

      assert.equal(await art.getMarketCut(), 50);

    })

    it("should show painting", async () => {

      await art.showPainting(accounts[1], "IPFS_LOC", 10**10, {from: accounts[0]})

      var paintingData = await art.getLatestPainting(accounts[1])

      assert.equal(paintingData[0], "IPFS_LOC")

      assert.equal(paintingData[1], 10**10)

    })

    it("should buy painting", async () => {

      await art.showPainting(accounts[1], "IPFS_LOC", 10**10, {from: accounts[0]})

      await art.buyPainting(accounts[5], {from: accounts[1], value: 10**10})

      assert.equal(1, await art.getOwnedPaintingsNumber(accounts[1]))

      assert.equal(true, await art.paintingIsBought("IPFS_LOC"))

      var paintingData = await art.getOwnedPainting(accounts[1], 0)

      assert(paintingData[0] == "IPFS_LOC" && paintingData[1] == 10**10)

      var obligationData = await art.getObligation(0)

      var marketMoney = await art.getOrderMoney(0)

      assert.equal(parseInt(obligationData) + parseInt(marketMoney), web3.eth.getBalance(art.address))

      assert.equal(accounts[5], await art.getOrderCharityAddress(0))

      assert.equal(1, await art.getOrderCharityCut(0))

    })

    it("should process obligations", async () => {

      await art.showPainting(accounts[1], "IPFS_LOC_2", 10**10, {from: accounts[0]})

      await art.buyPainting(accounts[5], {from: accounts[1], value: 10**10})

      var beforeProcessing = web3.eth.getBalance(accounts[5])

      await art.processLatestObligation({from: accounts[1]})

      var afterProcessing = web3.eth.getBalance(accounts[5])

      assert.equal(afterProcessing.minus(beforeProcessing).toString(), "500000000")

    })

    it("should process market orders", async () => {

      await art.showPainting(accounts[1], "IPFS_LOC_3", 10**10, {from: accounts[0]})

      await art.buyPainting(accounts[5], {from: accounts[1], value: 10**10})

      var charityBeforeProcessing = web3.eth.getBalance(accounts[5])

      var beforeBuyerTokenBalance = await charityToken.balanceOf(accounts[1])

      var beforeAgentTokenBalance = await charityToken.balanceOf(accounts[4])

      await art.processLatestOrder()

      var charityAfter = web3.eth.getBalance(accounts[5])

      assert.equal(charityAfter.minus(charityBeforeProcessing).toString(), "50000000")

      var afterBuyerTokenBalance = await charityToken.balanceOf(accounts[1])

      var afterAgentTokenBalance = await charityToken.balanceOf(accounts[4])

      assert.equal(afterBuyerTokenBalance.minus(beforeBuyerTokenBalance).toString(), "445000000000")
      assert.equal(afterAgentTokenBalance.minus(beforeAgentTokenBalance).toString(), "50000000000")

    })

})
