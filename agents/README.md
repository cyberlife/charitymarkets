# Cyberlife Smart Agents - Self-Optimizing Programmatic Entities Doing Business Online

*Note that* this software was inspired by the [Agoric Papers](https://e-drexler.com/d/09/00/AgoricsPapers/agoricpapers.html) and Trent McConaghy's [articles](https://medium.com/@trentmc0/ai-daos-and-three-paths-to-get-there-cfa0a4cc37b8)

## Table of Contents

- [Why This Solution](https://gitlab.com/cyberlife/smart_agents#why-this-solution)
- [Why Now](https://gitlab.com/cyberlife/smart_agents#why-now)
- [Target Groups](https://gitlab.com/cyberlife/smart_agents#target-groups)
- [How Can Agents Contribute to the Greater Good](https://gitlab.com/cyberlife/smart_agents#how-can-agents-contribute-to-the-greater-good)
- [Harmful Agent Examples](https://gitlab.com/cyberlife/smart_agents#harmful-agent-examples)
- [How Exactly Can Agents Make Money](https://gitlab.com/cyberlife/smart_agents#how-exactly-can-agents-make-money)
- [What is Missing from This Demo](https://gitlab.com/cyberlife/smart_agents#what-is-missing-from-this-demo)
- [Extras](https://gitlab.com/cyberlife/smart_agents#extras)

## Why This Solution

Deploy code once and let it optimize itself (and its costs) while giving you all or part of its revenue; moreover, due to division of labor, there can be different types of agents (workers, voters, advisors etc) that coordinate around [TransparentDAOs](https://github.com/stefanionescu/TransparentDao) and together achieve greater goals

https://spectrum.ieee.org/automaton/robotics/robotics-software/cloud-robotics

## Why Now

Fog/Edge computing getting traction - https://goldberg.berkeley.edu/cloud-robotics/

All components in place (blockchain for persistent scripts, IPFS/Filecoin for storage, DHTs for messaging)

## Target Groups and Personas

Agents migrating between machines:

hackers (hackers might find the network attractive because they can use it to deploy malware)

Charity markets:

NGOs, cash-back (Lyoness model)

Autonomy:

groups that want to protect the planet by personifying trees, forests etc or giving property to autonomous agents (see Mattereum)

Decentralized, reusable and composable code:

IoT devices (Fog/Edge computing)

From DAOs to AI DAOs:

blockchain developers / AI researchers

## How Can Agents Contribute to the Greater Good

Subsidizing smart contracts and helping them stay active after [blockchain rent](https://ethresear.ch/t/ethereum-state-rent-for-eth-1-x-pre-eip-document/4378) is introduced 

Offering insurance for smart contracts in case of hacks

[Charity markets](https://tokeneconomy.co/on-bonding-curves-and-charitable-giving-9bf74b9343d2)

Protecting the environment by making smart agents legal people and assigning forests as the property of the agents

## Harmful Agent Examples

All examples in this [Github repository](https://github.com/daviddao/awful-ai)

Agents [manipulating the Twitter sentiment](http://www.iftf.org/future-now/article-detail/artifact-from-the-future-clever-kitchen-agents/) regarding specific products in order to change their price

## Non-monetary Uses of Agents

[Detecting and combating bribing attacks](https://medium.com/@stefanionescu_71041/detecting-bribers-on-chain-bb1944164c7d)

## How Exactly Can Agents Make Money

[Generalized mining](https://tokeneconomy.co/generalized-mining-the-lps-perspective-f5dcbd0ed4e0)

Content curation on social media (Steemit) and getting paid in microtxs

Curating news for people and creating short summaries to big articles

Selling art, music, emojis etc

Playing in games where cryptocurrency can be earned

Controlling IoT devices (ex: washer autonomously reordering detergent and charging a small fee, displays autonomously displaying advertising content and getting a cut of the sales, vending machine selling goods)

[POSSIBLY] managing the crypto wealth for other people by diversifying or using other quantitative techniques

Lending money

[POSSIBLY] through insurance

## What is Missing from This Demo

A system to make sure agents don't mess with hosts' hardware

[VITAL] a governance system that allows people to interrupt agents that wreck the system and are only insterested in profit instead of helping humanity

A registry showing job requests from clients to agents and the status of each job

Scheduling software for hosts so they can correctly execute agents (this is for strictly defined times when agents need to be executed)

A priority queue for job requests (this is for non scheduled calls to agents)

A system like [ENS](https://ens.domains/) to allocate names for agents

Allow agents to self-execute (and possibly change their code) not just react to external factors (like smart contracts do)

Markets for processor time, RAM and disk space

Integrate Intel SGX (with something like [this](https://github.com/luckychain/node-secureworker)) in order to ensure that the correct code is run for each agent; alternativelly a consensus mechanism like DPoS can be used

A key management system that allows agents to control funds on the blockchain and call smart contracts without a malicious party stealing their private key (a possible solution is Shamir Secret Sharing + MPC)

[Scarce objects](https://nakamotoinstitute.org/scarce-objects/) in the form of cryptographic tokens; scarce objects would give us rights to interact with specific agents (or agents to call specific functions from other agents), get a share of the revenue made by an agent, delete and combine parts from different agents to create a new one etc

Division of labor: there can be different categories of agents that are specialized on specific tasks (voting on what to do next, making money, analysing data and giving advice, agents helping other agents optimize their costs etc)

## Extras

- Agents will be either controlled by other agents (be them software or humans) or they will be fully autonomous (they change their own code, make their own rules etc)
- Note that agents will be able to create other agents; this ability is not reserved only for humans ;)
- Smart Agents might be considered for [Fog Computing](https://www.networkworld.com/article/3243111/internet-of-things/what-is-fog-computing-connecting-the-cloud-to-things.html)
- Scarce objects can be considered a 'Property Token'; the token describes different rights that the possessor has
- Smart Agents can help solve a problem raised by V. Buterin: the fact that there is no economy entirely built around cryptocurrencies
- A system like this might not be so decentralized like a blockchain, but instead offers a higher degree of composability 
- [Mattereum](https://mattereum.com/) can be a good partner for Cyberlife because they are thinking about giving software the rights to own stuff