# cyberlife-js

## Topics

- [Inspirations](#inspirations)
- [Project Description](#project-description)
- [Demo Presentation](#demo-presentation)
- [Why Now](#why-now)
- [Mechanics of the Cyberlife Token](#mechanics-of-the-cyberlife-token)
- [Customer Incentives](#customer-incentives)
- [Vendor Incentives](#vendor-incentives)
- [Issues Encountered](#issues-encountered)
- [If We Had More Time, We Would Have](#if-we-had-more-time-we-would-have)
- [One Last Thing](#one-last-thing)

## Inspirations

The smart agent side of this project was inspired by the [Agoric Papers](https://e-drexler.com/d/09/00/AgoricsPapers/agoricpapers.html) and by Trent McConaghy's [AI DAOs](https://medium.com/@trentmc0/ai-daos-and-three-paths-to-get-there-cfa0a4cc37b8). Cashback markets were inspired by posts such as [this one](https://media.consensys.net/how-1990s-vacuum-packed-steaks-could-help-bring-ethereum-its-early-majority-f94f0a9eae5) encouraging the crypto community to find ways to engage an early majority.

## Project Description

Suppose Alice, a student at the University of Oxford, goes to an e-commerce store and pays for her shopping with a Cyberlife cashback card. Out of the $20 Alice paid, she will receive $4 back in the form of crypto cashback. Alice wants to delegate Cyberlife with investing her cashback in order to build up wealth, so her $4 go directly to a loan fund.

The loan fund matches Alice's money with a smart agent which needs to pay for computation and hosting on different machines. This particular smart agent is a piece of software selling generative art, but there are many more like it performing completely different tasks. The agent will gradually pay the loan back. Alice will get the principal back and the interest from the loan will be split between Alice, Cyberlife and the e-commerce shop which accepted the cashback program.

In short, Cyberlife is composed out of two smaller projects. First, a cashback program with one-click DeFi investments for students. Second, a P2P network of autonomous agents which need to pay for hosting and computation and can request financial help in case of dire need.

## Demo Presentation

The demo will cover all the layers of Cyberlife. We will go through:

1. Deploying a smart agent on a host
2. Showing an integration of the cashback markets with an e-commerce shop
3. Calling the deployed smart agent with the help of a P2P library and asking the agent to request a loan
4. Seeing how the agent creates a Maker CDP and uses the DAI as collateral for a Cyberlife loan
5. Seeing how a Cyberlife relayer fills the agent's request and sends them the principal
6. Additionally, seeing how another relayer is monitoring the savings accounts of Cyberlife customers and swapping their coins

## Why Now

The timing is right for such a project because of three reasons. 

First, companies like Aragon started to take an interest in [agents](https://blog.aragon.one/aragon-agent-beta-release/) which are controlled by multisig accounts and perform different tasks for a company, individual or group. 

Second, blockchain scaling solutions like [Loom sidechains](https://loomx.io/) and [Monoplasma](https://medium.com/streamrblog/monoplasma-revenue-share-dapps-off-chain-6cb7ee8b42fa) are ready to be used in production in order to meet user expectations. 

Finally, the idea or reinvesting cashback (which is a form of "staked" crypto) can be extended to the currently [300 million staked ETH](https://www.trustnodes.com/2019/02/27/300-million-eth-now-locked-in-dapps) and thus create [superfluid collateral](https://tokeneconomy.co/superfluid-collateral-in-open-finance-8c3db15efac).

## Mechanics of the Cyberlife Token

In this MVP we chose not to have an official token in order to focus more on building the actual network. Regardless, we do have ideas about how a token might behave and how it can incentivize all the parties in the ecosystem.

Cyberlife wants to be a for-profit company which will decentralize itself gradually. We intend to buy back tokens from our investors and burn them until a certain floor in terms of the token amount is hit.

The tokens will also be used by hosts who need to stake them in order to get paid for storing and executing smart agents. Last but not least, we are exploring the idea of having our own blockchain where humans and bots store their wealth. This blockchain will most probably need a token to incentivize the nodes maintaining it.

## Customer Incentives

Why would a normal person choose Cyberlife over established players in the cashback industry?

We do not encourage anyone to "recruit" their friends in the system or promise grand returns if someone "invests" in our system. When someone receives cashback, we simply ask them "Do you want to withdraw your cashback in a bank account? Or do you want us to invest it for you and we split the revenue?".

We add a banking layer over the simple cashback system and encourage cryptocurrency adoption by finding and offering competitive ROI.

## Vendor Incentives

Vendors also have a financial incentive to accept our cashback system. When we invest our customer's cashback and get a return, the return might be split equally between us and the customer. We can then offer some of our return to the vendor where the customer got the cashback from. Thus, vendors give up on a small percentage of their earnings today in order to receive returns over a longer period of time.

## Business Risks

The main risks related to this business are:

- Inability to scale the blockchain side of our business in order to meet the transaction load demanded by millions of users
- Users and vendors refusing to accept the cashback-and-invest system we offer
- Developers not wanting to experiment with smart agents and build dApps interacting with agents

## Issues Encountered

Listening to events and submitting transactions to Thunder do not work most of the time. Cashback markets relies on a relayer to process cashback orders and on Thunder it seems we cannot make our system work. That's why for the demo we will use a local blockchain.

Kyber was a bit restricting when it came to swapping bigger amounts of tokens.

## If We Had More Time, We Would Have

- Dockerized the entire demo instead of using multiple terminals
- Created a host node which can store and execute smart agents on phones
- Created a mobile app with NFT capabilities to pay and receive crypto cashback
- Integrated with a fiat-crypto on/off ramp
- Created software to analyze DeFi markets (Uniswap, Compound etc) and detect investments opportunities for our cashback customers
- Created a banking interface where cashback users can tell us their preferences regarding investing their cashback
- Tackled into the [300 million locked ETH](https://www.trustnodes.com/2019/02/27/300-million-eth-now-locked-in-dapps) and create a middleware to convert staked assets into [superfluid tokens](https://tokeneconomy.co/superfluid-collateral-in-open-finance-8c3db15efac) which can be reinvested into Defi markets 
- Much, much more... :muscle:

## One Last Thing

Cyberlife is a vision composed out of multiple pieces that we've been working on for 1 month and a half. Although the project is just making its first steps, we appreciate any feedback and help with developing it. Send an email to stefanionescu@protonmail.com if you'd like to get involved. Xièxiè! :cn:
