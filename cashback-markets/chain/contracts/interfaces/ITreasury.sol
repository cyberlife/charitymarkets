pragma solidity 0.5.3;

contract ITreasury {

  event FundedTarget(address target, uint256 money);

  event SetMarkets(address markets);


  function fundTarget(address target) public payable;

  function setMarkets(address _markets) public;

  function getTreasuryBalance(address target) public view returns (uint256);

}
