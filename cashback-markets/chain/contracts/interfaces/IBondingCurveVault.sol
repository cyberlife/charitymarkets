pragma solidity 0.5.3;

contract IBondingCurveVault {

  event LogLogicContractChanged
  (
      address byWhom,
      address indexed oldContract,
      address newContract
  );

  event LogEthSent(
      uint256 amount,
      address indexed account
  );

  event LogEthReceived(
      uint256 amount,
      address indexed account
  );

  function sendEth(uint256 _amount, address payable _account) public;

  function setLogicContract(address _logicContract) public;

  function contributeEth(address who) public payable;

  function () external payable;


  function getContributions(address who) public view returns (uint256);

}
