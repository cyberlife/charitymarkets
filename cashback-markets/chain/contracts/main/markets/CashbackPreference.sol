pragma solidity 0.5.3;

import "contracts/zeppelin/ownership/Ownable.sol";

import "contracts/interfaces/ICashbackPreference.sol";

contract CashbackPreference is Ownable, ICashbackPreference {

  mapping(address => uint256) bondedPreferences;

  constructor() public {}

  function changePreference(uint256 bonded) public {

    require(bonded >= 0 && bonded <= 100, "bonded needs to be between bounds");

    bondedPreferences[msg.sender] = bonded;

    emit ChangedPreference(msg.sender, msg.sender, bonded);

  }

  function setPreferenceFor(address target, uint256 bonded) public onlyOwner {

    require(bonded >= 0 && bonded <= 100, "bonded needs to be between bounds");

    bondedPreferences[target] = bonded;

    emit ChangedPreference(msg.sender, target, bonded);

  }

  //GETTERS

  function getBondedPreference(address target) public view returns (uint256) {

    return bondedPreferences[target];

  }

}
