pragma solidity 0.5.3;

// https://github.com/OpenZeppelin/

import "contracts/zeppelin/ERC20/CyberBondingERC20.sol";
import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/zeppelin/SafeMath.sol";
import "contracts/zeppelin/ContractDetector.sol";

import "contracts/main/bonding/IBondingCurve.sol";
import "contracts/main/bonding/BancorFormula.sol";
import "contracts/main/bonding/MaxGasPrice.sol";

contract CyberBondedToken is IBondingCurve, BancorFormula, Ownable, MaxGasPrice, CyberBondingERC20, ContractDetector {
    using SafeMath for uint256;

    // Use the same decimal places as ether.
    uint256 public scale; //= 10**18;
    uint256 public poolBalance; //= 1*scale;
    uint256 public reserveRatio;

    constructor(
        uint256 _scale,
        uint256 _poolBalance,
        uint256 _reserveRatio
    ) public {

        require(_scale > 0, "The scale needs to be positive");
        require(_poolBalance > 0, "The pool balance needs to be positive");
        require(_reserveRatio > 0, "The reserve ratio needs to be bigger than zero");

        scale = _scale;
        poolBalance = _poolBalance;
        reserveRatio = _reserveRatio;
        _mint(msg.sender, poolBalance);

    }

    function calculateCurvedMintReturn(uint256 _amount)
        public view returns (uint256 mintAmount)
    {
        return calculatePurchaseReturn(totalSupply(), poolBalance, uint32(reserveRatio), _amount);
    }

    function calculateCurvedBurnReturn(uint256 _amount)
        public view returns (uint256 burnAmount)
    {
        return calculateSaleReturn(totalSupply(), poolBalance, uint32(reserveRatio), _amount);
    }

    modifier validMint(uint256 _amount) {
        require(_amount > 0, "Amount must be non-zero!");
        _;
    }

    modifier validBurn(address payable target, uint256 _amount) {
        require(_amount > 0, "Amount must be non-zero!");
        require(balanceOf(target) >= _amount, "Target does not have enough tokens to burn.");
        _;
    }

    function _curvedMint(address target, uint256 _deposit)
        validGasPrice
        validMint(_deposit)
        internal returns (uint256)
    {
        uint256 amount = calculateCurvedMintReturn(_deposit);
        _mint(target, amount);
        poolBalance = poolBalance.add(_deposit);
        emit CurvedMint(target, amount, _deposit);
        return amount;
    }

    function _curvedBurn(address payable target, uint256 _amount)
        validGasPrice
        validBurn(target, _amount)
        internal returns (uint256)
    {
        uint256 reimbursement = calculateCurvedBurnReturn(_amount);
        poolBalance = poolBalance.sub(reimbursement);
        _burn(target, _amount);
        emit CurvedBurn(target, _amount, reimbursement);
        return reimbursement;
    }
}
