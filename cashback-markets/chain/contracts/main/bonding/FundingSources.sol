pragma solidity 0.5.3;

import "contracts/zeppelin/ownership/Ownable.sol";

import "contracts/interfaces/IFundingSources.sol";

import "contracts/zeppelin/ContractDetector.sol";

contract FundingSources is Ownable, IFundingSources, ContractDetector {

  mapping(address => mapping(address => bool)) sources;

  constructor() public {}

  function toggleSource(address token, address source) public onlyOwner {

    require(isContract(token) == true, "The token parram is not a contract address");

    sources[token][source] = !sources[token][source];

    emit ToggledSource(token, source);

  }

  //GETTERS

  function isSourceAvailable(address _tkn, address _source) public view returns (bool) {

    return sources[_tkn][_source];

  }

}
