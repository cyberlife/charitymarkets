pragma solidity 0.5.3;

import "contracts/interfaces/ICashbackIntegration.sol";
import "contracts/interfaces/ICashbackUpgrade.sol";

contract CashbackUpgrade is ICashbackUpgrade {

  ICashbackIntegration cashback;

  bool manualCharityFill = false;

  address upgradeOwner;

  modifier onlyUpgradeOwner {

    require(msg.sender == upgradeOwner);

    _;

  }

  constructor() public {

    upgradeOwner = msg.sender;

  }

  function changeUpgradeOwner(address newOwner) public onlyUpgradeOwner {

    require(newOwner != address(0), "The new owner must not be address(0)");

    upgradeOwner = newOwner;

    emit ChangedUpgradeOwner(msg.sender, newOwner);

  }

  function setCashbackIntegration(address _cashback) public onlyUpgradeOwner {

    cashback = ICashbackIntegration(_cashback);

    emit SetCashbackIntegration(_cashback);

  }

  function setManualCharityFill(bool manualFill) public onlyUpgradeOwner {

    manualCharityFill = manualFill;

    emit SetManualCharityFill(manualFill);

  }

  function moveFunds(uint256 amount, address payable target) public onlyUpgradeOwner {

    require(address(this).balance >= amount, "You do not have so many funds in the contract");
    require(target != address(0), "Cannot burn coins");

    target.transfer(amount);

    emit MovedFunds(amount, target);

  }

  function createCashbackOrder(address sender) public payable {

    require(address(cashback) != address(0), "The cashback integration must be initialized");
    require(msg.value > 0, "msg.value must be greater than zero");

    bytes memory data;
    uint256 marketMoney;

    (data, marketMoney) = cashback.getTwoPartyOrder(sender, msg.value);

    cashback.submitOrder.value(marketMoney)
                              (data, manualCharityFill);

  }

}
