var treasury = {
  "contractName": "Treasury",
  "abi": [
    {
      "constant": false,
      "inputs": [
        {
          "name": "_addr",
          "type": "address"
        }
      ],
      "name": "isContract",
      "outputs": [
        {
          "name": "isContract",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [],
      "name": "renounceOwnership",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "owner",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_newOwner",
          "type": "address"
        }
      ],
      "name": "transferOwnership",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "payable": true,
      "stateMutability": "payable",
      "type": "fallback"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "target",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "money",
          "type": "uint256"
        }
      ],
      "name": "FundedTarget",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "markets",
          "type": "address"
        }
      ],
      "name": "SetMarkets",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "name": "previousOwner",
          "type": "address"
        }
      ],
      "name": "OwnershipRenounced",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "name": "previousOwner",
          "type": "address"
        },
        {
          "indexed": true,
          "name": "newOwner",
          "type": "address"
        }
      ],
      "name": "OwnershipTransferred",
      "type": "event"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_markets",
          "type": "address"
        }
      ],
      "name": "setMarkets",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "target",
          "type": "address"
        }
      ],
      "name": "fundTarget",
      "outputs": [],
      "payable": true,
      "stateMutability": "payable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "target",
          "type": "address"
        }
      ],
      "name": "getTreasuryBalance",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    }
  ],
  "bytecode": "0x608060405234801561001057600080fd5b5060008054600160a060020a0319163317905561054b806100326000396000f3fe60806040526004361061008d576000357c010000000000000000000000000000000000000000000000000000000090048063715018a61161006b578063715018a6146101465780638da5cb5b1461015b578063cc83f7fd1461018c578063f2fde38b146101bf5761008d565b8063162790551461009257806322cd7b93146100d957806357baeac814610101575b600080fd5b34801561009e57600080fd5b506100c5600480360360208110156100b557600080fd5b5035600160a060020a03166101f2565b604080519115158252519081900360200190f35b6100ff600480360360208110156100ef57600080fd5b5035600160a060020a0316610200565b005b34801561010d57600080fd5b506101346004803603602081101561012457600080fd5b5035600160a060020a03166102a8565b60408051918252519081900360200190f35b34801561015257600080fd5b506100ff6102c3565b34801561016757600080fd5b5061017061032f565b60408051600160a060020a039092168252519081900360200190f35b34801561019857600080fd5b506100ff600480360360208110156101af57600080fd5b5035600160a060020a031661033e565b3480156101cb57600080fd5b506100ff600480360360208110156101e257600080fd5b5035600160a060020a0316610419565b6000903b63ffffffff161190565b600154600160a060020a03163314610263576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260268152602001806104fa6026913960400191505060405180910390fd5b600160a060020a03811660009081526002602052604090205461028c903463ffffffff61043c16565b600160a060020a03909116600090815260026020526040902055565b600160a060020a031660009081526002602052604090205490565b600054600160a060020a031633146102da57600080fd5b60008054604051600160a060020a03909116917ff8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c6482091a26000805473ffffffffffffffffffffffffffffffffffffffff19169055565b600054600160a060020a031681565b600054600160a060020a0316331461035557600080fd5b61035e816101f2565b15156001146103b8576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252602d8152602001806104cd602d913960400191505060405180910390fd5b60018054600160a060020a03831673ffffffffffffffffffffffffffffffffffffffff19909116811790915560408051918252517f2a2a28b2201ea796aa56ad60d5cf10b325423b3dc60cf39b3f70ba64a27cf2b49181900360200190a150565b600054600160a060020a0316331461043057600080fd5b6104398161044f565b50565b8181018281101561044957fe5b92915050565b600160a060020a038116151561046457600080fd5b60008054604051600160a060020a03808516939216917f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e091a36000805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a039290921691909117905556fe54686520706172616d20737065636966696564206973206e6f74206120636f6e747261637420616464726573735468652073656e646572206973206e6f7420746865206d61726b65747320636f6e7472616374a165627a7a723058209e965ad94f286a0332494401647eb35fe822dc1febaa518219597d0caef290170029",
  "deployedBytecode": "0x60806040526004361061008d576000357c010000000000000000000000000000000000000000000000000000000090048063715018a61161006b578063715018a6146101465780638da5cb5b1461015b578063cc83f7fd1461018c578063f2fde38b146101bf5761008d565b8063162790551461009257806322cd7b93146100d957806357baeac814610101575b600080fd5b34801561009e57600080fd5b506100c5600480360360208110156100b557600080fd5b5035600160a060020a03166101f2565b604080519115158252519081900360200190f35b6100ff600480360360208110156100ef57600080fd5b5035600160a060020a0316610200565b005b34801561010d57600080fd5b506101346004803603602081101561012457600080fd5b5035600160a060020a03166102a8565b60408051918252519081900360200190f35b34801561015257600080fd5b506100ff6102c3565b34801561016757600080fd5b5061017061032f565b60408051600160a060020a039092168252519081900360200190f35b34801561019857600080fd5b506100ff600480360360208110156101af57600080fd5b5035600160a060020a031661033e565b3480156101cb57600080fd5b506100ff600480360360208110156101e257600080fd5b5035600160a060020a0316610419565b6000903b63ffffffff161190565b600154600160a060020a03163314610263576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260268152602001806104fa6026913960400191505060405180910390fd5b600160a060020a03811660009081526002602052604090205461028c903463ffffffff61043c16565b600160a060020a03909116600090815260026020526040902055565b600160a060020a031660009081526002602052604090205490565b600054600160a060020a031633146102da57600080fd5b60008054604051600160a060020a03909116917ff8df31144d9c2f0f6b59d69b8b98abd5459d07f2742c4df920b25aae33c6482091a26000805473ffffffffffffffffffffffffffffffffffffffff19169055565b600054600160a060020a031681565b600054600160a060020a0316331461035557600080fd5b61035e816101f2565b15156001146103b8576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252602d8152602001806104cd602d913960400191505060405180910390fd5b60018054600160a060020a03831673ffffffffffffffffffffffffffffffffffffffff19909116811790915560408051918252517f2a2a28b2201ea796aa56ad60d5cf10b325423b3dc60cf39b3f70ba64a27cf2b49181900360200190a150565b600054600160a060020a0316331461043057600080fd5b6104398161044f565b50565b8181018281101561044957fe5b92915050565b600160a060020a038116151561046457600080fd5b60008054604051600160a060020a03808516939216917f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e091a36000805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a039290921691909117905556fe54686520706172616d20737065636966696564206973206e6f74206120636f6e747261637420616464726573735468652073656e646572206973206e6f7420746865206d61726b65747320636f6e7472616374a165627a7a723058209e965ad94f286a0332494401647eb35fe822dc1febaa518219597d0caef290170029",
  "sourceMap": "214:862:28:-;;;507:23;8:9:-1;5:2;;;30:1;27;20:12;5:2;-1:-1;565:5:78;:18;;-1:-1:-1;;;;;;565:18:78;573:10;565:18;;;214:862:28;;;;;;",
  "deployedSourceMap": "214:862:28:-;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;569:8;;;82:171:68;;8:9:-1;5:2;;;30:1;27;20:12;5:2;82:171:68;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;82:171:68;-1:-1:-1;;;;;82:171:68;;:::i;:::-;;;;;;;;;;;;;;;;;;809:134:28;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;809:134:28;-1:-1:-1;;;;;809:134:28;;:::i;:::-;;960:113;;8:9:-1;5:2;;;30:1;27;20:12;5:2;960:113:28;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;960:113:28;-1:-1:-1;;;;;960:113:28;;:::i;:::-;;;;;;;;;;;;;;;;999:111:78;;8:9:-1;5:2;;;30:1;27;20:12;5:2;999:111:78;;;:::i;236:20::-;;8:9:-1;5:2;;;30:1;27;20:12;5:2;236:20:78;;;:::i;:::-;;;;-1:-1:-1;;;;;236:20:78;;;;;;;;;;;;;;587:218:28;;8:9:-1;5:2;;;30:1;27;20:12;5:2;587:218:28;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;587:218:28;-1:-1:-1;;;;;587:218:28;;:::i;1272:103:78:-;;8:9:-1;5:2;;;30:1;27;20:12;5:2;1272:103:78;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;-1:-1;1272:103:78;-1:-1:-1;;;;;1272:103:78;;:::i;82:171:68:-;133:15;200:18;;238:8;;;;82:171::o;809:134:28:-;432:15;;-1:-1:-1;;;;;432:15:28;418:10;:29;410:80;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;;;;;903:19:28;;;;;;:11;:19;;;;;;:34;;927:9;903:34;:23;:34;:::i;:::-;-1:-1:-1;;;;;881:19:28;;;;;;;:11;:19;;;;;:56;809:134::o;960:113::-;-1:-1:-1;;;;;1048:19:28;1025:7;1048:19;;;:11;:19;;;;;;;960:113::o;999:111:78:-;717:5;;-1:-1:-1;;;;;717:5:78;703:10;:19;695:28;;;;;;1075:5;;;1056:25;;-1:-1:-1;;;;;1075:5:78;;;;1056:25;;;1103:1;1087:18;;-1:-1:-1;;1087:18:78;;;999:111::o;236:20::-;;;-1:-1:-1;;;;;236:20:78;;:::o;587:218:28:-;717:5:78;;-1:-1:-1;;;;;717:5:78;703:10;:19;695:28;;;;;;657:20:28;668:8;657:10;:20::i;:::-;:28;;681:4;657:28;649:86;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;741:15;:26;;-1:-1:-1;;;;;741:26:28;;-1:-1:-1;;741:26:28;;;;;;;;779:20;;;;;;;;;;;;;;;;587:218;:::o;1272:103:78:-;717:5;;-1:-1:-1;;;;;717:5:78;703:10;:19;695:28;;;;;;1341:29;1360:9;1341:18;:29::i;:::-;1272:103;:::o;1236:128:77:-;1317:7;;;1337;;;;1330:15;;;;1236:128;;;;:::o;1510:171:78:-;-1:-1:-1;;;;;1580:23:78;;;;1572:32;;;;;;1636:5;;;1615:38;;-1:-1:-1;;;;;1615:38:78;;;;1636:5;;;1615:38;;;1659:5;:17;;-1:-1:-1;;1659:17:78;-1:-1:-1;;;;;1659:17:78;;;;;;;;;;1510:171::o",
  "source": "pragma solidity 0.4.18;\n\nimport \"contracts/zeppelin/ownership/Ownable.sol\";\nimport \"contracts/zeppelin/SafeMath.sol\";\nimport \"contracts/zeppelin/ContractDetector.sol\";\n\nimport \"contracts/interfaces/ITreasury.sol\";\n\ncontract Treasury is Ownable, ITreasury, ContractDetector {\n\n  using SafeMath for uint256;\n\n  address cashbackMarkets;\n\n  mapping(address => uint256) ethBalances;\n\n  modifier onlyMarkets() {\n\n    require(msg.sender == cashbackMarkets, \"The sender is not the markets contract\");\n    _;\n\n  }\n\n  constructor() public {}\n\n  function() external payable {\n\n    revert();\n\n  }\n\n  function setMarkets(address _markets) public onlyOwner {\n\n    require(isContract(_markets) == true, \"The param specified is not a contract address\");\n    cashbackMarkets = _markets;\n\n    emit SetMarkets(_markets);\n\n  }\n\n  function fundTarget(address target) public payable onlyMarkets() {\n\n    ethBalances[target] = ethBalances[target].add(msg.value);\n\n  }\n\n  //GETTERS\n\n  function getTreasuryBalance(address target) public view returns (uint256) {\n\n    return ethBalances[target];\n\n  }\n\n}\n",
  "sourcePath": "/home/stefan/Work/cyberlife-js/cashback-markets/chain/contracts/main/markets/Treasury.sol",
  "ast": {
    "absolutePath": "/home/stefan/Work/cyberlife-js/cashback-markets/chain/contracts/main/markets/Treasury.sol",
    "exportedSymbols": {
      "Treasury": [
        7277
      ]
    },
    "id": 7278,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 7177,
        "literals": [
          "solidity",
          "0.5",
          ".3"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:22:28"
      },
      {
        "absolutePath": "contracts/zeppelin/ownership/Ownable.sol",
        "file": "contracts/zeppelin/ownership/Ownable.sol",
        "id": 7178,
        "nodeType": "ImportDirective",
        "scope": 7278,
        "sourceUnit": 16391,
        "src": "24:50:28",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/zeppelin/SafeMath.sol",
        "file": "contracts/zeppelin/SafeMath.sol",
        "id": 7179,
        "nodeType": "ImportDirective",
        "scope": 7278,
        "sourceUnit": 16305,
        "src": "75:41:28",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/zeppelin/ContractDetector.sol",
        "file": "contracts/zeppelin/ContractDetector.sol",
        "id": 7180,
        "nodeType": "ImportDirective",
        "scope": 7278,
        "sourceUnit": 14697,
        "src": "117:49:28",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/ITreasury.sol",
        "file": "contracts/interfaces/ITreasury.sol",
        "id": 7181,
        "nodeType": "ImportDirective",
        "scope": 7278,
        "sourceUnit": 10550,
        "src": "168:44:28",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 7182,
              "name": "Ownable",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 16390,
              "src": "235:7:28",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_Ownable_$16390",
                "typeString": "contract Ownable"
              }
            },
            "id": 7183,
            "nodeType": "InheritanceSpecifier",
            "src": "235:7:28"
          },
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 7184,
              "name": "ITreasury",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10549,
              "src": "244:9:28",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_ITreasury_$10549",
                "typeString": "contract ITreasury"
              }
            },
            "id": 7185,
            "nodeType": "InheritanceSpecifier",
            "src": "244:9:28"
          },
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 7186,
              "name": "ContractDetector",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 14696,
              "src": "255:16:28",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_ContractDetector_$14696",
                "typeString": "contract ContractDetector"
              }
            },
            "id": 7187,
            "nodeType": "InheritanceSpecifier",
            "src": "255:16:28"
          }
        ],
        "contractDependencies": [
          10549,
          14696,
          16390
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 7277,
        "linearizedBaseContracts": [
          7277,
          14696,
          10549,
          16390
        ],
        "name": "Treasury",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "id": 7190,
            "libraryName": {
              "contractScope": null,
              "id": 7188,
              "name": "SafeMath",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 16304,
              "src": "283:8:28",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_SafeMath_$16304",
                "typeString": "library SafeMath"
              }
            },
            "nodeType": "UsingForDirective",
            "src": "277:27:28",
            "typeName": {
              "id": 7189,
              "name": "uint256",
              "nodeType": "ElementaryTypeName",
              "src": "296:7:28",
              "typeDescriptions": {
                "typeIdentifier": "t_uint256",
                "typeString": "uint256"
              }
            }
          },
          {
            "constant": false,
            "id": 7192,
            "name": "cashbackMarkets",
            "nodeType": "VariableDeclaration",
            "scope": 7277,
            "src": "308:23:28",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_address",
              "typeString": "address"
            },
            "typeName": {
              "id": 7191,
              "name": "address",
              "nodeType": "ElementaryTypeName",
              "src": "308:7:28",
              "stateMutability": "nonpayable",
              "typeDescriptions": {
                "typeIdentifier": "t_address",
                "typeString": "address"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 7196,
            "name": "ethBalances",
            "nodeType": "VariableDeclaration",
            "scope": 7277,
            "src": "336:39:28",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
              "typeString": "mapping(address => uint256)"
            },
            "typeName": {
              "id": 7195,
              "keyType": {
                "id": 7193,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "344:7:28",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "336:27:28",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                "typeString": "mapping(address => uint256)"
              },
              "valueType": {
                "id": 7194,
                "name": "uint256",
                "nodeType": "ElementaryTypeName",
                "src": "355:7:28",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 7207,
              "nodeType": "Block",
              "src": "403:100:28",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 7202,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 7199,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 16405,
                            "src": "418:3:28",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 7200,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "418:10:28",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address_payable",
                            "typeString": "address payable"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "id": 7201,
                          "name": "cashbackMarkets",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 7192,
                          "src": "432:15:28",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "418:29:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "5468652073656e646572206973206e6f7420746865206d61726b65747320636f6e7472616374",
                        "id": 7203,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "449:40:28",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_fffc888cbb57d30e349d5a46932cccc69ab884a8632d36e890e438c4a3e98272",
                          "typeString": "literal_string \"The sender is not the markets contract\""
                        },
                        "value": "The sender is not the markets contract"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_fffc888cbb57d30e349d5a46932cccc69ab884a8632d36e890e438c4a3e98272",
                          "typeString": "literal_string \"The sender is not the markets contract\""
                        }
                      ],
                      "id": 7198,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        16408,
                        16409
                      ],
                      "referencedDeclaration": 16409,
                      "src": "410:7:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 7204,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "410:80:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 7205,
                  "nodeType": "ExpressionStatement",
                  "src": "410:80:28"
                },
                {
                  "id": 7206,
                  "nodeType": "PlaceholderStatement",
                  "src": "496:1:28"
                }
              ]
            },
            "documentation": null,
            "id": 7208,
            "name": "onlyMarkets",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 7197,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "400:2:28"
            },
            "src": "380:123:28",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 7211,
              "nodeType": "Block",
              "src": "528:2:28",
              "statements": []
            },
            "documentation": null,
            "id": 7212,
            "implemented": true,
            "kind": "constructor",
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 7209,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "518:2:28"
            },
            "returnParameters": {
              "id": 7210,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "528:0:28"
            },
            "scope": 7277,
            "src": "507:23:28",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 7218,
              "nodeType": "Block",
              "src": "562:21:28",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [],
                    "expression": {
                      "argumentTypes": [],
                      "id": 7215,
                      "name": "revert",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        16410,
                        16411
                      ],
                      "referencedDeclaration": 16410,
                      "src": "569:6:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_revert_pure$__$returns$__$",
                        "typeString": "function () pure"
                      }
                    },
                    "id": 7216,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "569:8:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 7217,
                  "nodeType": "ExpressionStatement",
                  "src": "569:8:28"
                }
              ]
            },
            "documentation": null,
            "id": 7219,
            "implemented": true,
            "kind": "fallback",
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 7213,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "542:2:28"
            },
            "returnParameters": {
              "id": 7214,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "562:0:28"
            },
            "scope": 7277,
            "src": "534:49:28",
            "stateMutability": "payable",
            "superFunction": null,
            "visibility": "external"
          },
          {
            "body": {
              "id": 7243,
              "nodeType": "Block",
              "src": "642:163:28",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        "id": 7231,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 7228,
                              "name": "_markets",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 7221,
                              "src": "668:8:28",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            ],
                            "id": 7227,
                            "name": "isContract",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 14695,
                            "src": "657:10:28",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_internal_nonpayable$_t_address_$returns$_t_bool_$",
                              "typeString": "function (address) returns (bool)"
                            }
                          },
                          "id": 7229,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "657:20:28",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "74727565",
                          "id": 7230,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "bool",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "681:4:28",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          },
                          "value": "true"
                        },
                        "src": "657:28:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "54686520706172616d20737065636966696564206973206e6f74206120636f6e74726163742061646472657373",
                        "id": 7232,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "687:47:28",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_ea7f55fcb379b4eff8365eb1dc433bf73ecc6d3549fcfb942b712381c17d4308",
                          "typeString": "literal_string \"The param specified is not a contract address\""
                        },
                        "value": "The param specified is not a contract address"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_ea7f55fcb379b4eff8365eb1dc433bf73ecc6d3549fcfb942b712381c17d4308",
                          "typeString": "literal_string \"The param specified is not a contract address\""
                        }
                      ],
                      "id": 7226,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        16408,
                        16409
                      ],
                      "referencedDeclaration": 16409,
                      "src": "649:7:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 7233,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "649:86:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 7234,
                  "nodeType": "ExpressionStatement",
                  "src": "649:86:28"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 7237,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 7235,
                      "name": "cashbackMarkets",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 7192,
                      "src": "741:15:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 7236,
                      "name": "_markets",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 7221,
                      "src": "759:8:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "741:26:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 7238,
                  "nodeType": "ExpressionStatement",
                  "src": "741:26:28"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 7240,
                        "name": "_markets",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 7221,
                        "src": "790:8:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      ],
                      "id": 7239,
                      "name": "SetMarkets",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 10531,
                      "src": "779:10:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$returns$__$",
                        "typeString": "function (address)"
                      }
                    },
                    "id": 7241,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "779:20:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 7242,
                  "nodeType": "EmitStatement",
                  "src": "774:25:28"
                }
              ]
            },
            "documentation": null,
            "id": 7244,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": null,
                "id": 7224,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 7223,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 16338,
                  "src": "632:9:28",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "632:9:28"
              }
            ],
            "name": "setMarkets",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 7222,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 7221,
                  "name": "_markets",
                  "nodeType": "VariableDeclaration",
                  "scope": 7244,
                  "src": "607:16:28",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 7220,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "607:7:28",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "606:18:28"
            },
            "returnParameters": {
              "id": 7225,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "642:0:28"
            },
            "scope": 7277,
            "src": "587:218:28",
            "stateMutability": "nonpayable",
            "superFunction": 10541,
            "visibility": "public"
          },
          {
            "body": {
              "id": 7263,
              "nodeType": "Block",
              "src": "874:69:28",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 7261,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 7251,
                        "name": "ethBalances",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 7196,
                        "src": "881:11:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 7253,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 7252,
                        "name": "target",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 7246,
                        "src": "893:6:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "881:19:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 7258,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 16405,
                            "src": "927:3:28",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 7259,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "value",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "927:9:28",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        ],
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 7254,
                            "name": "ethBalances",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 7196,
                            "src": "903:11:28",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                              "typeString": "mapping(address => uint256)"
                            }
                          },
                          "id": 7256,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 7255,
                            "name": "target",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 7246,
                            "src": "915:6:28",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "903:19:28",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 7257,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "add",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 16303,
                        "src": "903:23:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_function_internal_pure$_t_uint256_$_t_uint256_$returns$_t_uint256_$bound_to$_t_uint256_$",
                          "typeString": "function (uint256,uint256) pure returns (uint256)"
                        }
                      },
                      "id": 7260,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "functionCall",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "903:34:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "881:56:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 7262,
                  "nodeType": "ExpressionStatement",
                  "src": "881:56:28"
                }
              ]
            },
            "documentation": null,
            "id": 7264,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": [],
                "id": 7249,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 7248,
                  "name": "onlyMarkets",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 7208,
                  "src": "860:11:28",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "860:13:28"
              }
            ],
            "name": "fundTarget",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 7247,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 7246,
                  "name": "target",
                  "nodeType": "VariableDeclaration",
                  "scope": 7264,
                  "src": "829:14:28",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 7245,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "829:7:28",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "828:16:28"
            },
            "returnParameters": {
              "id": 7250,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "874:0:28"
            },
            "scope": 7277,
            "src": "809:134:28",
            "stateMutability": "payable",
            "superFunction": 10536,
            "visibility": "public"
          },
          {
            "body": {
              "id": 7275,
              "nodeType": "Block",
              "src": "1034:39:28",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "id": 7271,
                      "name": "ethBalances",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 7196,
                      "src": "1048:11:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                        "typeString": "mapping(address => uint256)"
                      }
                    },
                    "id": 7273,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 7272,
                      "name": "target",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 7266,
                      "src": "1060:6:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "1048:19:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 7270,
                  "id": 7274,
                  "nodeType": "Return",
                  "src": "1041:26:28"
                }
              ]
            },
            "documentation": null,
            "id": 7276,
            "implemented": true,
            "kind": "function",
            "modifiers": [],
            "name": "getTreasuryBalance",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 7267,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 7266,
                  "name": "target",
                  "nodeType": "VariableDeclaration",
                  "scope": 7276,
                  "src": "988:14:28",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 7265,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "988:7:28",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "987:16:28"
            },
            "returnParameters": {
              "id": 7270,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 7269,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 7276,
                  "src": "1025:7:28",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 7268,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "1025:7:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1024:9:28"
            },
            "scope": 7277,
            "src": "960:113:28",
            "stateMutability": "view",
            "superFunction": 10548,
            "visibility": "public"
          }
        ],
        "scope": 7278,
        "src": "214:862:28"
      }
    ],
    "src": "0:1077:28"
  },
  "legacyAST": {
    "absolutePath": "/home/stefan/Work/cyberlife-js/cashback-markets/chain/contracts/main/markets/Treasury.sol",
    "exportedSymbols": {
      "Treasury": [
        7277
      ]
    },
    "id": 7278,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 7177,
        "literals": [
          "solidity",
          "0.5",
          ".3"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:22:28"
      },
      {
        "absolutePath": "contracts/zeppelin/ownership/Ownable.sol",
        "file": "contracts/zeppelin/ownership/Ownable.sol",
        "id": 7178,
        "nodeType": "ImportDirective",
        "scope": 7278,
        "sourceUnit": 16391,
        "src": "24:50:28",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/zeppelin/SafeMath.sol",
        "file": "contracts/zeppelin/SafeMath.sol",
        "id": 7179,
        "nodeType": "ImportDirective",
        "scope": 7278,
        "sourceUnit": 16305,
        "src": "75:41:28",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/zeppelin/ContractDetector.sol",
        "file": "contracts/zeppelin/ContractDetector.sol",
        "id": 7180,
        "nodeType": "ImportDirective",
        "scope": 7278,
        "sourceUnit": 14697,
        "src": "117:49:28",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "absolutePath": "contracts/interfaces/ITreasury.sol",
        "file": "contracts/interfaces/ITreasury.sol",
        "id": 7181,
        "nodeType": "ImportDirective",
        "scope": 7278,
        "sourceUnit": 10550,
        "src": "168:44:28",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 7182,
              "name": "Ownable",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 16390,
              "src": "235:7:28",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_Ownable_$16390",
                "typeString": "contract Ownable"
              }
            },
            "id": 7183,
            "nodeType": "InheritanceSpecifier",
            "src": "235:7:28"
          },
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 7184,
              "name": "ITreasury",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 10549,
              "src": "244:9:28",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_ITreasury_$10549",
                "typeString": "contract ITreasury"
              }
            },
            "id": 7185,
            "nodeType": "InheritanceSpecifier",
            "src": "244:9:28"
          },
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 7186,
              "name": "ContractDetector",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 14696,
              "src": "255:16:28",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_ContractDetector_$14696",
                "typeString": "contract ContractDetector"
              }
            },
            "id": 7187,
            "nodeType": "InheritanceSpecifier",
            "src": "255:16:28"
          }
        ],
        "contractDependencies": [
          10549,
          14696,
          16390
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 7277,
        "linearizedBaseContracts": [
          7277,
          14696,
          10549,
          16390
        ],
        "name": "Treasury",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "id": 7190,
            "libraryName": {
              "contractScope": null,
              "id": 7188,
              "name": "SafeMath",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 16304,
              "src": "283:8:28",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_SafeMath_$16304",
                "typeString": "library SafeMath"
              }
            },
            "nodeType": "UsingForDirective",
            "src": "277:27:28",
            "typeName": {
              "id": 7189,
              "name": "uint256",
              "nodeType": "ElementaryTypeName",
              "src": "296:7:28",
              "typeDescriptions": {
                "typeIdentifier": "t_uint256",
                "typeString": "uint256"
              }
            }
          },
          {
            "constant": false,
            "id": 7192,
            "name": "cashbackMarkets",
            "nodeType": "VariableDeclaration",
            "scope": 7277,
            "src": "308:23:28",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_address",
              "typeString": "address"
            },
            "typeName": {
              "id": 7191,
              "name": "address",
              "nodeType": "ElementaryTypeName",
              "src": "308:7:28",
              "stateMutability": "nonpayable",
              "typeDescriptions": {
                "typeIdentifier": "t_address",
                "typeString": "address"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 7196,
            "name": "ethBalances",
            "nodeType": "VariableDeclaration",
            "scope": 7277,
            "src": "336:39:28",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
              "typeString": "mapping(address => uint256)"
            },
            "typeName": {
              "id": 7195,
              "keyType": {
                "id": 7193,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "344:7:28",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "336:27:28",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                "typeString": "mapping(address => uint256)"
              },
              "valueType": {
                "id": 7194,
                "name": "uint256",
                "nodeType": "ElementaryTypeName",
                "src": "355:7:28",
                "typeDescriptions": {
                  "typeIdentifier": "t_uint256",
                  "typeString": "uint256"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 7207,
              "nodeType": "Block",
              "src": "403:100:28",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        },
                        "id": 7202,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 7199,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 16405,
                            "src": "418:3:28",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 7200,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "sender",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "418:10:28",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address_payable",
                            "typeString": "address payable"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "id": 7201,
                          "name": "cashbackMarkets",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 7192,
                          "src": "432:15:28",
                          "typeDescriptions": {
                            "typeIdentifier": "t_address",
                            "typeString": "address"
                          }
                        },
                        "src": "418:29:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "5468652073656e646572206973206e6f7420746865206d61726b65747320636f6e7472616374",
                        "id": 7203,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "449:40:28",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_fffc888cbb57d30e349d5a46932cccc69ab884a8632d36e890e438c4a3e98272",
                          "typeString": "literal_string \"The sender is not the markets contract\""
                        },
                        "value": "The sender is not the markets contract"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_fffc888cbb57d30e349d5a46932cccc69ab884a8632d36e890e438c4a3e98272",
                          "typeString": "literal_string \"The sender is not the markets contract\""
                        }
                      ],
                      "id": 7198,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        16408,
                        16409
                      ],
                      "referencedDeclaration": 16409,
                      "src": "410:7:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 7204,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "410:80:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 7205,
                  "nodeType": "ExpressionStatement",
                  "src": "410:80:28"
                },
                {
                  "id": 7206,
                  "nodeType": "PlaceholderStatement",
                  "src": "496:1:28"
                }
              ]
            },
            "documentation": null,
            "id": 7208,
            "name": "onlyMarkets",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 7197,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "400:2:28"
            },
            "src": "380:123:28",
            "visibility": "internal"
          },
          {
            "body": {
              "id": 7211,
              "nodeType": "Block",
              "src": "528:2:28",
              "statements": []
            },
            "documentation": null,
            "id": 7212,
            "implemented": true,
            "kind": "constructor",
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 7209,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "518:2:28"
            },
            "returnParameters": {
              "id": 7210,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "528:0:28"
            },
            "scope": 7277,
            "src": "507:23:28",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 7218,
              "nodeType": "Block",
              "src": "562:21:28",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [],
                    "expression": {
                      "argumentTypes": [],
                      "id": 7215,
                      "name": "revert",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        16410,
                        16411
                      ],
                      "referencedDeclaration": 16410,
                      "src": "569:6:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_revert_pure$__$returns$__$",
                        "typeString": "function () pure"
                      }
                    },
                    "id": 7216,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "569:8:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 7217,
                  "nodeType": "ExpressionStatement",
                  "src": "569:8:28"
                }
              ]
            },
            "documentation": null,
            "id": 7219,
            "implemented": true,
            "kind": "fallback",
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 7213,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "542:2:28"
            },
            "returnParameters": {
              "id": 7214,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "562:0:28"
            },
            "scope": 7277,
            "src": "534:49:28",
            "stateMutability": "payable",
            "superFunction": null,
            "visibility": "external"
          },
          {
            "body": {
              "id": 7243,
              "nodeType": "Block",
              "src": "642:163:28",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        "id": 7231,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "arguments": [
                            {
                              "argumentTypes": null,
                              "id": 7228,
                              "name": "_markets",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 7221,
                              "src": "668:8:28",
                              "typeDescriptions": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            }
                          ],
                          "expression": {
                            "argumentTypes": [
                              {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              }
                            ],
                            "id": 7227,
                            "name": "isContract",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 14695,
                            "src": "657:10:28",
                            "typeDescriptions": {
                              "typeIdentifier": "t_function_internal_nonpayable$_t_address_$returns$_t_bool_$",
                              "typeString": "function (address) returns (bool)"
                            }
                          },
                          "id": 7229,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "kind": "functionCall",
                          "lValueRequested": false,
                          "names": [],
                          "nodeType": "FunctionCall",
                          "src": "657:20:28",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "74727565",
                          "id": 7230,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "bool",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "681:4:28",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          },
                          "value": "true"
                        },
                        "src": "657:28:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      },
                      {
                        "argumentTypes": null,
                        "hexValue": "54686520706172616d20737065636966696564206973206e6f74206120636f6e74726163742061646472657373",
                        "id": 7232,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": true,
                        "kind": "string",
                        "lValueRequested": false,
                        "nodeType": "Literal",
                        "src": "687:47:28",
                        "subdenomination": null,
                        "typeDescriptions": {
                          "typeIdentifier": "t_stringliteral_ea7f55fcb379b4eff8365eb1dc433bf73ecc6d3549fcfb942b712381c17d4308",
                          "typeString": "literal_string \"The param specified is not a contract address\""
                        },
                        "value": "The param specified is not a contract address"
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        {
                          "typeIdentifier": "t_stringliteral_ea7f55fcb379b4eff8365eb1dc433bf73ecc6d3549fcfb942b712381c17d4308",
                          "typeString": "literal_string \"The param specified is not a contract address\""
                        }
                      ],
                      "id": 7226,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        16408,
                        16409
                      ],
                      "referencedDeclaration": 16409,
                      "src": "649:7:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$_t_string_memory_ptr_$returns$__$",
                        "typeString": "function (bool,string memory) pure"
                      }
                    },
                    "id": 7233,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "649:86:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 7234,
                  "nodeType": "ExpressionStatement",
                  "src": "649:86:28"
                },
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 7237,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 7235,
                      "name": "cashbackMarkets",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 7192,
                      "src": "741:15:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 7236,
                      "name": "_markets",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 7221,
                      "src": "759:8:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "src": "741:26:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "id": 7238,
                  "nodeType": "ExpressionStatement",
                  "src": "741:26:28"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 7240,
                        "name": "_markets",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 7221,
                        "src": "790:8:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      ],
                      "id": 7239,
                      "name": "SetMarkets",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 10531,
                      "src": "779:10:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$returns$__$",
                        "typeString": "function (address)"
                      }
                    },
                    "id": 7241,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "779:20:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 7242,
                  "nodeType": "EmitStatement",
                  "src": "774:25:28"
                }
              ]
            },
            "documentation": null,
            "id": 7244,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": null,
                "id": 7224,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 7223,
                  "name": "onlyOwner",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 16338,
                  "src": "632:9:28",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "632:9:28"
              }
            ],
            "name": "setMarkets",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 7222,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 7221,
                  "name": "_markets",
                  "nodeType": "VariableDeclaration",
                  "scope": 7244,
                  "src": "607:16:28",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 7220,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "607:7:28",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "606:18:28"
            },
            "returnParameters": {
              "id": 7225,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "642:0:28"
            },
            "scope": 7277,
            "src": "587:218:28",
            "stateMutability": "nonpayable",
            "superFunction": 10541,
            "visibility": "public"
          },
          {
            "body": {
              "id": 7263,
              "nodeType": "Block",
              "src": "874:69:28",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 7261,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 7251,
                        "name": "ethBalances",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 7196,
                        "src": "881:11:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                          "typeString": "mapping(address => uint256)"
                        }
                      },
                      "id": 7253,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 7252,
                        "name": "target",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 7246,
                        "src": "893:6:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "881:19:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "arguments": [
                        {
                          "argumentTypes": null,
                          "expression": {
                            "argumentTypes": null,
                            "id": 7258,
                            "name": "msg",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 16405,
                            "src": "927:3:28",
                            "typeDescriptions": {
                              "typeIdentifier": "t_magic_message",
                              "typeString": "msg"
                            }
                          },
                          "id": 7259,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "memberName": "value",
                          "nodeType": "MemberAccess",
                          "referencedDeclaration": null,
                          "src": "927:9:28",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        }
                      ],
                      "expression": {
                        "argumentTypes": [
                          {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        ],
                        "expression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 7254,
                            "name": "ethBalances",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 7196,
                            "src": "903:11:28",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                              "typeString": "mapping(address => uint256)"
                            }
                          },
                          "id": 7256,
                          "indexExpression": {
                            "argumentTypes": null,
                            "id": 7255,
                            "name": "target",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 7246,
                            "src": "915:6:28",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address",
                              "typeString": "address"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "903:19:28",
                          "typeDescriptions": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          }
                        },
                        "id": 7257,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "add",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": 16303,
                        "src": "903:23:28",
                        "typeDescriptions": {
                          "typeIdentifier": "t_function_internal_pure$_t_uint256_$_t_uint256_$returns$_t_uint256_$bound_to$_t_uint256_$",
                          "typeString": "function (uint256,uint256) pure returns (uint256)"
                        }
                      },
                      "id": 7260,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": false,
                      "kind": "functionCall",
                      "lValueRequested": false,
                      "names": [],
                      "nodeType": "FunctionCall",
                      "src": "903:34:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_uint256",
                        "typeString": "uint256"
                      }
                    },
                    "src": "881:56:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "id": 7262,
                  "nodeType": "ExpressionStatement",
                  "src": "881:56:28"
                }
              ]
            },
            "documentation": null,
            "id": 7264,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": [],
                "id": 7249,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 7248,
                  "name": "onlyMarkets",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 7208,
                  "src": "860:11:28",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "860:13:28"
              }
            ],
            "name": "fundTarget",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 7247,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 7246,
                  "name": "target",
                  "nodeType": "VariableDeclaration",
                  "scope": 7264,
                  "src": "829:14:28",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 7245,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "829:7:28",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "828:16:28"
            },
            "returnParameters": {
              "id": 7250,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "874:0:28"
            },
            "scope": 7277,
            "src": "809:134:28",
            "stateMutability": "payable",
            "superFunction": 10536,
            "visibility": "public"
          },
          {
            "body": {
              "id": 7275,
              "nodeType": "Block",
              "src": "1034:39:28",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "id": 7271,
                      "name": "ethBalances",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 7196,
                      "src": "1048:11:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_address_$_t_uint256_$",
                        "typeString": "mapping(address => uint256)"
                      }
                    },
                    "id": 7273,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 7272,
                      "name": "target",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 7266,
                      "src": "1060:6:28",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "1048:19:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "functionReturnParameters": 7270,
                  "id": 7274,
                  "nodeType": "Return",
                  "src": "1041:26:28"
                }
              ]
            },
            "documentation": null,
            "id": 7276,
            "implemented": true,
            "kind": "function",
            "modifiers": [],
            "name": "getTreasuryBalance",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 7267,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 7266,
                  "name": "target",
                  "nodeType": "VariableDeclaration",
                  "scope": 7276,
                  "src": "988:14:28",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 7265,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "988:7:28",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "987:16:28"
            },
            "returnParameters": {
              "id": 7270,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 7269,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 7276,
                  "src": "1025:7:28",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_uint256",
                    "typeString": "uint256"
                  },
                  "typeName": {
                    "id": 7268,
                    "name": "uint256",
                    "nodeType": "ElementaryTypeName",
                    "src": "1025:7:28",
                    "typeDescriptions": {
                      "typeIdentifier": "t_uint256",
                      "typeString": "uint256"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "1024:9:28"
            },
            "scope": 7277,
            "src": "960:113:28",
            "stateMutability": "view",
            "superFunction": 10548,
            "visibility": "public"
          }
        ],
        "scope": 7278,
        "src": "214:862:28"
      }
    ],
    "src": "0:1077:28"
  },
  "compiler": {
    "name": "solc",
    "version": "0.5.3+commit.10d17f24.Emscripten.clang"
  },
  "networks": {},
  "schemaVersion": "3.0.1",
  "updatedAt": "2019-02-17T22:40:21.839Z",
  "devdoc": {
    "methods": {
      "renounceOwnership()": {
        "details": "Allows the current owner to relinquish control of the contract."
      },
      "transferOwnership(address)": {
        "details": "Allows the current owner to transfer control of the contract to a newOwner.",
        "params": {
          "_newOwner": "The address to transfer ownership to."
        }
      }
    }
  },
  "userdoc": {
    "methods": {
      "renounceOwnership()": {
        "notice": "Renouncing to ownership will leave the contract without an owner. It will not be possible to call the functions with the `onlyOwner` modifier anymore."
      }
    }
  }
}

module.exports = {

  treasury

}
