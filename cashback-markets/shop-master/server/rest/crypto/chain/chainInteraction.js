var Web3 = require('web3');
const axios = require('axios')
const EthereumTx = require('ethereumjs-tx')

//30 Gwei
var DEFAULT_GAS_PRICE = 30000000000

var DEFAULT_GAS_LIMIT = 1000000

async function getWeb3(network) {

  if (network == "local")
    return new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

  else return new Web3(new Web3.providers.HttpProvider("https://testnet-rpc.thundercore.com:8544"));

}

//SHOP

async function buySomething(network, instance, pubKey, privateKey, customer,
                            order, moneySent, v, r, s, callback) {

  if (network == "local") {

    instance.methods.buySomething(customer, order, v, r, s).send({

      from: pubKey,
      value: moneySent,
      gas: 800000

    }, function(err, result) {

      if (err) {callback(err, undefined);}

      callback(undefined, result);

    })

  } else {

    var web3 = await getWeb3(network);

    web3.eth.getTransactionCount(pubKey).then( async function(nonce) {

      //creating raw transaction
      var rawTransaction = {
        "from": pubKey,
        "gasPrice": DEFAULT_GAS_PRICE,
        "gasLimit": DEFAULT_GAS_LIMIT,
        "to": instance.address,
        "value": moneySent.toString(),
        "data": instance.methods.buySomething(customer, order, v, r, s).encodeABI(),
        "nonce": web3.utils.toHex(nonce)
      }

      var transaction = new EthereumTx(rawTransaction);

      const privateKeyBuffer = Buffer.from(privateKey, 'hex');

      //signing transaction with private key
      await transaction.sign(privateKeyBuffer);

      //sending transaction via web3js
      web3.eth.sendSignedTransaction('0x' + transaction.serialize().toString('hex'), function(rawErr, rawResult) {

        if (rawErr) {callback(rawErr, undefined);}

        callback(undefined, rawResult);

      })

    })

  }

}

//HOUSE

async function getOrdersNumber(instance, pubKey, company, callback) {

  await instance.methods.getOrdersNumber(company)
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

//CHARITY MARKETS

async function getTotalReturn(instance, pubKey, target, callback) {

  await instance.methods.getTotalReturn(target)
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

//TREASURY

async function getTreasuryBalance(instance, pubKey, target, callback) {

  await instance.methods.getTreasuryBalance(target)
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

//DETAILED CBT

async function balanceOf(instance, pubKey, target, callback) {

  await instance.methods.balanceOf(target)
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

async function calculateCurvedMintReturn(instance, pubKey, amount, callback) {

  await instance.methods.calculateCurvedMintReturn(amount)
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

async function calculateCurvedBurnReturn(instance, pubKey, amount, callback) {

  await instance.methods.calculateCurvedBurnReturn(amount)
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

async function getTotalSupply(instance, pubKey, callback) {

  await instance.methods.totalSupply()
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

async function calculateSaleReturn(instance, pubKey, supply,
    connectorBalance, connectorWeight, sellAmount, callback) {

  await instance.methods.calculateSaleReturn(supply, connectorBalance,
                                             connectorWeight, sellAmount)
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

async function getPoolBalance(instance, pubKey, callback) {

  await instance.methods.getPoolBalance()
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

module.exports = {

  getWeb3,

  buySomething,

  getTotalReturn,

  getOrdersNumber,

  getTreasuryBalance,

  balanceOf,
  calculateCurvedMintReturn,
  calculateCurvedBurnReturn,
  getTotalSupply,
  calculateSaleReturn,
  getPoolBalance

}
