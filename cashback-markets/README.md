# Cashback Markets
_Cashback and donations markets to incentivise cryptocurrency adoption_

Code taken and modified from [David Truong's bonded donations](https://github.com/mrdavey/BondedDonations)

# Example Scenario

Alice wants to buy a [Gen 0 Cryptokitty](https://github.com/cryptocopycats/awesome-cryptokitties) worth $100. After she presses _Buy_ a couple of things happen:

- __X%__ (80%, or $80 in this example) go to Axiom Zen (AZ); AZ can split this money between revenue, charitable donation and investment in a bonding curve

- The rest of __(100 - X)%__ ($20 in our example) will be available for Alice; Alice can choose to donate all the money, get it back as cashback in the form of tokens minted by a bonding curve or a combination of the two; it's important to note that a minimum of money (1%) will go toward donations no matter what

# Why Cashback Markets?

_Cashback_ is a well known word, although most companies in this industry have shady practices and an overall bad reputation (Lyoness, suddenly rebranded to [Cashback World](https://www.cashbackworld.com)). Namely, so called "cashback" companies rely on endless recruitment and "investments" which are promised to grow exponentially in a short period of time.

Rather than imposing a MLM-like structure, cashback markets offer customers and businesses (especially those in the blockchain world) a number of advantages:

- Transparency. All transactions are being recorded on the blockchain ready for anyone to inspect them.

- Economic design over deceiving the masses. Cashback markets rely on math and [dry code](https://unenumerated.blogspot.com/2006/11/wet-code-and-dry.html) and not on false promises, endless growth and closed-door deals

- Blockchain companies have been struggling to grow their user-base. Charity markets are a way to encourage user growth by offering customers the option to direct part of their spent money on donations, __bonded cashback__ or both

- Donations (especially those which can be tracked) are an excellent way to present the blockchain as tech for the betterment of humanity.

- An alternative to ICOs. Each bonding curve can be tied to a new type of DAO, [a continuous organization](https://medium.com/@thibauld/continuous-organizations-v1-0-45d42b3082bb). In our sample scenario, we said 1% will always go to a charity. What if this one percent goes to an investment pool and we use [Liberal Radicalism](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3243656) to allocate money to blockchain companies?

# What's Missing

Most people won't use cryptocurrencies to buy items, be them digital or physical. They will use fiat to buy an emoji, fuel for their car or an intergalactic super kitty. In order to bring people in the crypto world, cashback markets need a fiat-crypto on-ramp.

# Quick Architecture Description

__TODO__

# Set up

Clone this repo and then in the root directory do:

```
npm install

chmod +x startGanache.sh
./startGanache.sh

```

Then open another CLI in the root dir and type:

```
truffle compile
truffle migrate --reset
truffle test
```
