var utils = require("./crypto/utils.js");
var contractsAddresses = require("./crypto/contractsAddresses.js")
var contractCaller = require("./crypto/contractCaller.js");

var lendingCustomers = [

  "0x6259ac218eed8caf47e26246d7e13c1df70165f2"

];

var relayer = "0xcecc7c956a1707b22b81179ae67018f7f8ff57f7"

var net = "local"

async function watchLoans() {

  var loanManagerInstance =
    await utils.getContractInstance(net, "websocket", "lendingManager")

  console.log("LENDING-RELAYER: Started to watch for loan requests...")

  loanManagerInstance.events.allEvents({
    fromBlock: 'latest'
  }, async (error, event) => {

    if (error) {

      console.log("Could not catch an event from LendingManager")

    } else {

      if (event.event == 'RequestedLoan') {

        console.log("LENDING-RELAYER: Found a new loan request with id "
                     + event.returnValues.id
                     + ". Filling it...")

        var loanTreasuryInstance =
          await utils.getContractInstance(net, "http", "lendingTreasury");

        await contractCaller
        .fillLoan(net, loanTreasuryInstance, relayer,
                  lendingCustomers[0], event.returnValues.id.toString())

      } else if (event.event == 'FilledLoan') {

        console.log("LENDING-RELAYER: Filled the loan with id " + event.returnValues.id)

      }

    }

  })

}

module.exports = {

  watchLoans

}
