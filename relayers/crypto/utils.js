var houseABI = require('../abi/ClearingHouse.js');
var rebalancerABI = require("../abi/SavingsRebalancer.js");
var rebalancerDataABI = require("../abi/SavingsRebalancerData.js");

var lendingManagerABI = require("../abi/LendingManager.js");
var lendingTreasuryABI = require("../abi/LendingTreasury.js");
var mockDAIABI = require("../abi/MockDAI.js");

var contractsAddresses = require('./contractsAddresses.js');
var accounts = require('./accounts.js');
var contractCaller = require('./contractCaller.js');

async function getContractInstance(network, providerType, contractName) {

  var web3, houseInstance, rebalancerInstance, rebalancerDataInstance,
      lendingTreasuryInstance, lendingManagerInstance, mockDaiInstance;

  if (network == "local") {

    web3 = await contractCaller.getWeb3('local', providerType);

    if (contractName == "house") {

      try {

          houseInstance = await new web3.eth.Contract(

            houseABI.house.abi,
            contractsAddresses.localContracts[0],

            {from: accounts.local[0]}

        );

        return houseInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "rebalancer") {

      try {

        rebalancerInstance = await new web3.eth.Contract(

          rebalancerABI.rebalancer.abi,
          contractsAddresses.localContracts[1],

          {from: accounts.local[0]}

        );

        return rebalancerInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "rebalancerData") {

      try {

        rebalancerDataInstance = await new web3.eth.Contract(

          rebalancerDataABI.rebalancerData.abi,
          contractsAddresses.localContracts[2],

          {from: accounts.local[0]}

        );

        return rebalancerDataInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "lendingTreasury") {

      try {

        lendingTreasuryInstance = await new web3.eth.Contract(

          lendingTreasuryABI.lendingTreasury.abi,
          contractsAddresses.localContracts[8],

          {from: accounts.local[0]}

        );

        return lendingTreasuryInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "lendingManager") {

      try {

        lendingManagerInstance = await new web3.eth.Contract(

          lendingManagerABI.lendingManager.abi,
          contractsAddresses.localContracts[9],

          {from: accounts.local[0]}

        );

        return lendingManagerInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "mockDai") {

      try {

        mockDaiInstance = await new web3.eth.Contract(

          mockDAIABI.mockDai.abi,
          contractsAddresses.localContracts[6],

          {from: accounts.local[0]}

        );

        return mockDaiInstance;

      } catch(err) {

        return undefined;

      }

    }

    return undefined;

  }

}

module.exports = {

  getContractInstance

}
