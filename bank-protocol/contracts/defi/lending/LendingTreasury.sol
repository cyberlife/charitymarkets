pragma solidity 0.5.3;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/zeppelin/SafeMath.sol";

import "contracts/interfaces/ILendingManager.sol";
import "contracts/interfaces/ILendingTreasury.sol";

contract LendingTreasury is ILendingTreasury, Ownable {

  using SafeMath for uint256;

  mapping(address => uint256) lendingBalances;

  mapping(address => bool) validRelayers;

  mapping(address => mapping(uint => bool)) customerLoanTie;

  mapping(uint => address) loanCustomerTie;

  mapping(address => uint256) totalLentPerCustomer;

  ILendingManager manager;

  modifier onlyValidRelayer() {

    require(validRelayers[msg.sender] == true, "This sender is not a valid relayer");

    _;

  }

  modifier onlyLendingManager() {

    require(msg.sender == address(manager), "The caller is not the lending manager");

    _;

  }

  constructor() public {}

  function() external payable {

    require(msg.value > 0, "You need to send more than 0 wei");

    lendingBalances[msg.sender] = lendingBalances[msg.sender].add(msg.value);

    emit DepositedETH(msg.sender, msg.value);

  }

  //OWNER

  function toggleRelayer(address relayer) public onlyOwner {

    validRelayers[relayer] = !validRelayers[relayer];

    emit ToggledRelayer(relayer, validRelayers[relayer]);

  }

  function setLoanManager(address _manager) public onlyOwner {

    manager = ILendingManager(_manager);

    emit SetLoanManager(_manager);

  }

  function depositLendingETH() public payable {

    require(msg.value > 0, "You need to send a positive amount of money");

    lendingBalances[msg.sender] = lendingBalances[msg.sender].add(msg.value);

    emit DepositedETH(msg.sender, msg.value);

  }

  function withdrawLendingETH(uint256 amount) public {

    require(amount <= lendingBalances[msg.sender] - totalLentPerCustomer[msg.sender],
      "You cannot withdraw more than your total balance minus what is currently being lent");

    lendingBalances[msg.sender] = lendingBalances[msg.sender] - amount;

    msg.sender.transfer(amount);

    emit WithdrewETH(msg.sender, amount);

  }

  function fillLoan(address customer, uint loanId) public onlyValidRelayer() {

    uint amountToLend = manager.getAmountLent(loanId);

    require(totalLentPerCustomer[customer]
        + amountToLend <= lendingBalances[customer],
        "Cannot lend more than the customer approved for lending");

    totalLentPerCustomer[customer] = totalLentPerCustomer[customer] + amountToLend;

    manager.fillLoan.value(amountToLend)(loanId);

    customerLoanTie[customer][loanId] = true;

    loanCustomerTie[loanId] = customer;

  }

  function enableKyberSwap(uint id, address destToken) public onlyValidRelayer() {

    manager.enableSwappedCollateral(id, destToken);

  }

  function seizeCollateral(uint id, address customer) public onlyValidRelayer() {

    address collateralReceived;
    uint collateralType;
    bytes memory collateralDetails;

    address ethAddress = manager.getETHAddress();

    (collateralReceived, collateralType, collateralDetails)
      = manager.declareDefault(id);

    //For the demo we have multiple token types, for production we swap with Kyber

    /*require(ethAddress == collateralReceived,
      "The treasury only accepts ETH as collateral");

    uint ethAmount = abi.decode(collateralDetails, (uint256));

    totalLentPerCustomer[customer] =
      totalLentPerCustomer[customer] - ethAmount;*/

    customerLoanTie[customer][id] = false;

  }

  function registerLoanIncome(uint id) public payable onlyLendingManager {

    address customer = loanCustomerTie[id];

    if (msg.value > totalLentPerCustomer[customer]) {

      uint256 extraIncome = msg.value - totalLentPerCustomer[customer];

      totalLentPerCustomer[customer] = 0;

      lendingBalances[customer] = lendingBalances[customer] + extraIncome;

    } else {

      totalLentPerCustomer[customer] = totalLentPerCustomer[customer] - msg.value;

    }

    emit RegisteredLoanIncome(customer, msg.value);

  }

}
