pragma solidity 0.5.3;

import "contracts/zeppelin/ERC20/DetailedMintableToken.sol";

contract MockZILL is DetailedMintableToken {

  constructor(string memory _name, string memory _symbol, uint8 decimals) public
  DetailedMintableToken(_name, _symbol, decimals)
  {}

}
