pragma solidity 0.5.3;

/// @title Kyber Network interface
contract KyberNetworkProxyInterface {

  function recordTokenMinting(address tkn, uint256 amount) public;

  function swapTokenToToken(
      address src,
      uint srcAmount,
      address dest,
      uint destAmount
  )
      public;

  function provideETHLiquidity() public payable;

  function swapEtherToToken(address token, uint destAmount)
    public payable;

  function swapTokenToEther(address token, uint srcAmount, uint destAmount)
    public;

  function tradeWithHint(
      address src,
      uint srcAmount,
      address dest,
      address payable destAddress,
      uint maxDestAmount,
      uint destAmount,
      address walletId,
      bytes memory hint
  )
      public
      payable;

  function getCoinAmount(address coin) public view returns (uint256);

  function getEthAddress() public view returns (address);

}
