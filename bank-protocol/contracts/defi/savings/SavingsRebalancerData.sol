pragma solidity 0.5.3;

import "contracts/zeppelin/ownership/Ownable.sol";

import "contracts/zeppelin/ContractDetector.sol";

import "contracts/zeppelin/SafeMath.sol";

import "contracts/interfaces/ISavingsRebalancerData.sol";

import "contracts/interfaces/ISavingsRebalancer.sol";

contract SavingsRebalancerData is ISavingsRebalancerData, ContractDetector, Ownable {

  using SafeMath for uint256;

  struct Customer {

    bool completedBaseData;

    string coinsList;

    uint256 diversificationLevel;

    uint256 currentCoins;

    uint256 remainingFeeMoney;

    uint256 checkpointedPaymentRequest;

    uint256 serviceStartTimestamp;

    bool stoppedService;

    bool blockedPaymentRequests;

  }

  mapping(address => Customer) customersData;

  mapping(uint256 => uint256) diversificationMultiplier;

  mapping(address => bool) approvedRelayers;

  //Need to optimize for off-chain storage

  address[] customers;

  uint256 baseFeePerSecond;
  uint256 minServiceDuration;
  uint256 minRebalancingPause;

  address payable cyberFund;
  ISavingsRebalancer rebalancer;

  modifier customerState(bool desired) {

    require(desired == isCustomerInitialized(msg.sender));
    _;

  }

  modifier hasFundsDeposited(address customer) {

    require(rebalancer.getTotalBalance(customer) >= 0);

    _;

  }

  modifier serviceState(bool desired, address customer) {

    require(desired == customersData[customer].stoppedService);
    _;

  }

  modifier ownerDidSetup() {

    require(

      cyberFund != address(0) &&
      address(rebalancer) != address(0) &&
      minServiceDuration > 0 &&
      minRebalancingPause > 0

    );

    _;

  }

  modifier onlyValidRelayer() {

    require(approvedRelayers[msg.sender] == true);

    _;

  }

  constructor() public {

    //Approve the owner as a relayer if they are not a contract

    if (isContract(msg.sender) == false) {

      approvedRelayers[msg.sender] = true;

    }

  }

  //OWNER

  function changeRebalancerAddress(address _reb) public onlyOwner {

    require(isContract(_reb) == true);

    rebalancer = ISavingsRebalancer(_reb);

    emit ChangedRebalancerAddress(_reb);

  }

  function changeMinRebalancingPause(uint256 _pause) public onlyOwner {

    require(_pause > 0);

    minRebalancingPause = _pause;

    emit ChangedMinRebalancingPause(_pause);

  }

  function changeCyberFund(address payable _fund) public onlyOwner {

    require(isContract(_fund) == true);

    cyberFund = _fund;

    emit ChangedCyberFund(_fund);

  }

  function changeBaseFee(uint256 fee) public onlyOwner {

    baseFeePerSecond = fee;

    emit ChangedBaseFee(fee);

  }

  function changeDiversificationMultiplier(uint256 diverseLevel, uint256 multiplier)
    public onlyOwner {

    require(multiplier >= 100);

    diversificationMultiplier[diverseLevel] = multiplier;

    emit ChangedDiversificationMultiplier(diverseLevel, multiplier);

  }

  function changeMinServiceDuration(uint256 _minServiceDuration) public onlyOwner {

    require(_minServiceDuration > 60);

    minServiceDuration = _minServiceDuration * 1 seconds;

    emit ChangedMinServiceDuration(_minServiceDuration);

  }

  function toggleRelayer(address _relayer) public onlyOwner {

    require(isContract(_relayer) == false);

    approvedRelayers[_relayer] = !approvedRelayers[_relayer];

    emit ToggledRelayer(_relayer);

  }

  //USER

  function initializeCustomer(uint256 diversification)
    public customerState(false) ownerDidSetup {

    require(diversificationMultiplier[diversification] > 0);

    Customer memory newCustomer = Customer(true, "", diversification,
                                    0, 0, 0, 0,
                                    true, true);

    customersData[msg.sender] = newCustomer;

    customers.push(msg.sender);

    emit CustomerInitialized(msg.sender, diversification);

  }

  function changeSetup(uint256 diversification)
    public serviceState(true, msg.sender) {

    require(diversificationMultiplier[diversification] > 0);

    customersData[msg.sender].diversificationLevel = diversification;

    emit ChangedSetup(msg.sender, diversification);

  }

  function depositFees() public payable customerState(true) {

    require(msg.value > 0);

    customersData[msg.sender].remainingFeeMoney =
      customersData[msg.sender].remainingFeeMoney.add(msg.value);

    emit DepositedFees(msg.sender, msg.value);

  }

  function withdrawFees(uint256 money)
    public customerState(true) serviceState(true, msg.sender) {

    require(customersData[msg.sender].remainingFeeMoney >= money);

    customersData[msg.sender].remainingFeeMoney =
      customersData[msg.sender].remainingFeeMoney.sub(money);

    msg.sender.transfer(money);

    emit WithdrewFees(msg.sender, money);

  }

  function startService()
    public customerState(true) serviceState(true, msg.sender)
    hasFundsDeposited(msg.sender) ownerDidSetup() {

    require(enoughMoneyForMinimumService(msg.sender) == true);

    //Save timestamp

    customersData[msg.sender].serviceStartTimestamp = now;

    //Unblock payment requests

    customersData[msg.sender].blockedPaymentRequests = false;

    //Show that service is ongoing

    customersData[msg.sender].stoppedService = false;

    //Put the latest checkpoint now

    customersData[msg.sender].checkpointedPaymentRequest = now;

    uint256 serviceTime = serviceTimeForCurrentFunds(msg.sender);

    emit StartedService(msg.sender, now + serviceTime);

  }

  function stopService()
    public customerState(true) serviceState(false, msg.sender) {

    //Check if enough time passed since start

    require(now - customersData[msg.sender].serviceStartTimestamp >= minServiceDuration);

    require(customersData[msg.sender].blockedPaymentRequests == false);

    customersData[msg.sender].blockedPaymentRequests = true;

    uint256 serviceTime = serviceTimeForCurrentFunds(msg.sender);

    uint256 pricePerSecond = customerPricePerSecond(msg.sender);

    uint256 secondsSinceCheckpoint = now - customersData[msg.sender].checkpointedPaymentRequest;

    customersData[msg.sender].checkpointedPaymentRequest = now;

    //Decide how many seconds are billed

    uint256 secondsToBill = secondsSinceCheckpoint;

    if (serviceTime <= secondsToBill) secondsToBill = serviceTime;

    //Reduce amount of fee money the customer has

    uint256 charge = secondsToBill * pricePerSecond;

    customersData[msg.sender].remainingFeeMoney =
      customersData[msg.sender].remainingFeeMoney.sub(charge);

    //Send money to cyber address

    cyberFund.transfer(charge);

    //Stop the service

    customersData[msg.sender].stoppedService = true;

    //event

    emit StoppedService(msg.sender, customersData[msg.sender].remainingFeeMoney);

  }

  //RELAYER

  function updateCoinAmount(address customer, uint256 coins, string memory distributionList)
    public onlyValidRelayer customerState(true) {

    customersData[customer].coinsList = distributionList;

    customersData[customer].currentCoins = coins;

    emit UpdatedCoinsAmount(msg.sender, customer, distributionList, coins);

  }

  function billCustomer(address customer)
    public serviceState(false, customer) onlyValidRelayer {

    require(customersData[customer].blockedPaymentRequests == false);

    customersData[customer].blockedPaymentRequests = true;

    //Get data relevant to billing

    uint256 pricePerSecond = customerPricePerSecond(customer);

    uint256 totalServiceTime = serviceTimeForCurrentFunds(customer);

    uint256 secondsSinceCheckpoint = now - customersData[customer].checkpointedPaymentRequest;

    if (secondsSinceCheckpoint > totalServiceTime)
      secondsSinceCheckpoint = totalServiceTime;

    //Put the checkpoint now

    customersData[customer].checkpointedPaymentRequest = now;

    //Decide how much to bill

    uint256 charge = secondsSinceCheckpoint * pricePerSecond;

    //Send payment to Cyber fund

    cyberFund.transfer(charge);

    //Unblock payment requests

    customersData[customer].blockedPaymentRequests = false;

    //event

    emit BilledCustomer(customer, msg.sender, charge);

  }

  //GETTERS

  function isCustomerInitialized(address customer) public view returns (bool) {

    return customersData[customer].completedBaseData;

  }

  function customerCoinList(address customer) public view returns (string memory) {

    if (isCustomerInitialized(customer) == true) {

      return customersData[customer].coinsList;

    } else return "";

  }

  function customerDiversificationLevel(address customer) public view returns (uint256) {

    if (isCustomerInitialized(customer) == true) {

      return customersData[customer].diversificationLevel;

    } else return 0;

  }

  function customerCurrentCoinNumber(address customer) public view returns (uint256) {

    if (isCustomerInitialized(customer) == true) {

      return customersData[customer].currentCoins;

    } else return 0;

  }

  function customerRemainingFeeMoney(address customer) public view returns (uint256) {

    if (isCustomerInitialized(customer) == true) {

      return customersData[customer].remainingFeeMoney;

    } else return 0;

  }

  function customerCheckpointedPayment(address customer) public view returns (uint256) {

    if (isCustomerInitialized(customer) == true) {

      return customersData[customer].checkpointedPaymentRequest;

    } else return 0;

  }

  function customerServiceStart(address customer) public view returns (uint256) {

    if (isCustomerInitialized(customer) == true) {

      return customersData[customer].serviceStartTimestamp;

    } else return 0;

  }

  function customerStoppedService(address customer) public view returns (bool) {

    if (isCustomerInitialized(customer) == true) {

      return customersData[customer].stoppedService;

    } else return true;

  }

  function customerBlockedRequests(address customer) public view returns (bool) {

    if (isCustomerInitialized(customer) == true) {

      return customersData[customer].blockedPaymentRequests;

    } else return true;

  }

  function timeUntilStop(address customer) public view returns (int256) {

    if (isCustomerInitialized(customer) == true) {

      if (customersData[customer].serviceStartTimestamp == 0) return 0;

      return int256(customersData[customer].serviceStartTimestamp + minServiceDuration) - int256(now);

    } else return 0;

  }

  function serviceTimeForCurrentFunds(address customer) public view returns (uint256) {

    uint256 perSecondFee = customerPricePerSecond(customer);

    return customersData[customer].remainingFeeMoney / perSecondFee;

  }

  function setupPricePerSecond(uint256 diverse, uint256 coinsNumber)
    public view returns (uint256) {

    return baseFeePerSecond *
           diversificationMultiplier[diverse] /
           100 +
           150 *
           coinsNumber /
           100;

  }

  function customerPricePerSecond(address customer) public view returns (uint256) {

    return baseFeePerSecond *
           diversificationMultiplier[customersData[customer].diversificationLevel] /
           100;

  }

  function enoughMoneyForMinimumService(address customer) public view returns (bool) {

    uint256 perSecond = serviceTimeForCurrentFunds(customer);

    return (perSecond >= minServiceDuration);

  }

  function isRelayerApproved(address relayer) public view returns (bool) {

    return approvedRelayers[relayer];

  }

  function getRebalancingPause() public view returns (uint256) {

    return minRebalancingPause;

  }

}
