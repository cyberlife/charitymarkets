pragma solidity 0.5.3;

import "contracts/zeppelin/ownership/Ownable.sol";

contract DAO is Ownable {

  constructor() public {}

  function() external payable {}

  function transferFunds(uint256 amount, address payable target) public onlyOwner {

    target.transfer(amount);

  }

}
