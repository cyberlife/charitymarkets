pragma solidity 0.5.3;

contract ILendingManager {

  event RequestedLoan(uint id, bytes loanData);

  event CancelledLoan(uint id);

  event EnabledSwapping(uint id, address destToken);

  event FilledLoan(address filler, uint id);

  event PaidLoan(uint id, address lender, address principal, uint amount);

  event SuccessfulLoan(uint id);

  event DefaultedLoan(uint id);

  event SetLendingTreasury(address treasury);


  function setLendingTreasury(address _treasury) public;

  function requestLoan(

    bytes memory loanData

  ) public payable;

  function cancelLoan(uint id) public;

  function fillLoan(uint id) public payable;

  function payLoan(uint id, uint amount) public payable;

  function declareDefault(uint id) public returns (address, uint, bytes memory);

  function enableSwappedCollateral(uint id, address destToken) public;

  function transferCollateral(uint id, address payable target) internal returns (uint);

  function getAmountToPay(uint id) public view returns (uint256);

  function canDeclareDefault(uint id) public view returns (bool);

  function getAmountLent(uint id) public view returns (uint);

  function getLoanState(uint id) public view returns (uint);

  function getLender(uint id) public view returns (address);

  function getBorrower(uint id) public view returns (address);

  function getETHAddress() public view returns (address);

}
